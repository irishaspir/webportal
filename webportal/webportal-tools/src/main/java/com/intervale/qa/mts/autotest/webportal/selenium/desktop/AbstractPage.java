package com.intervale.qa.mts.autotest.webportal.selenium.desktop;

import com.intervale.qa.mts.autotest.webportal.selenium.SeleniumPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by dchahovsky.
 */
public abstract class AbstractPage extends SeleniumPageObject {

    public AbstractPage(WebDriver driver) {
        super(driver);
    }

    public LeftMenu getLeftMenu(){
        return new LeftMenu(getDriver(), getDriver().findElement(By.className("leftmenu")));
    }
}
