package com.intervale.qa.mts.autotest.webportal.selenium.mobile;

import com.intervale.qa.mts.autotest.webportal.selenium.SeleniumPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ashaporev
 */
public class MobilePaymentPage extends MobileWhiteboardPage {

    @FindBy(className = "pay-row")
    private WebElement payRow;

    @FindBy(id = "payment-form")
    private WebElement paymentForm;

    @FindBy(id = "cardform")
    private WebElement cardForm;

    public MobilePaymentPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getInputParamFieldByName(String name) {
        return paymentForm.findElement(By.name(name));
    }

    public void submit() {
        getDriver().findElement(By.id("btnOK")).click();
    }

    public List<WebElement> getRegisteredCardRadioButtons() {
        return paymentForm.findElements(By.xpath("//*[@class='from-registered-card']/input[@type='radio']"));
    }

    public List<String> listRegisteredCardNames() {
        List<WebElement> labelList = paymentForm.findElements(By.xpath("//*[@class='from-registered-card']/label"));
        List<String> cardTitleList = new ArrayList<>();
        for (WebElement label : labelList) {
            String text = label.getText().trim();
            if (text.matches("Банковской картой .*")) {
                cardTitleList.add(text.substring(18));
            } else {
                throw new IllegalStateException("Card title should start with text 'Банковской картой ', but it is: " + text);
            }
        }
        return cardTitleList;
    }

    public WebElement getCardByName(String name) {
        for (WebElement e : getRegisteredCardRadioButtons()) {
            String text = e.findElement(By.xpath("../label")).getText();
            if (text != null && text.contains(name)) {
                return e;
            }
        }
        return null;
    }

    public WebElement getAccountRadioButton() {
        return getDriver().findElement(By.xpath("//div[@class='from-account']/input[@type='radio']"));
    }

    public WebElement getUnregisteredCardRadioButton() {
        return getDriver().findElement(By.xpath("//div[@class='from-unregistered-card']/input[@type='radio']"));
    }

    public WebElement getCvvInputField() {
        return cardForm.findElement(By.id("card-cvv2"));
    }

    public WebElement getPanInputField() {
        return cardForm.findElement(By.id("card-pan"));
    }

    public WebElement getCardHolderNameInputField() {
        return cardForm.findElement(By.id("card-cardHolderName"));
    }

    public void selectSomeExpYear() {
        final Select selectBox = new Select(getDriver().findElement(By.id("card-expyear")));
        selectBox.selectByValue("17");
    }

    public WebElement getOfferLink() {
        return getDriver().findElement(By.id("offerLink"));
    }

    public String getPaymentSourceName() {
        return payRow.findElement(By.xpath("//input[@type='radio' and  @checked='checked']")).getAttribute("value");
    }

    public void searchForPayment(String searchQuery) {
        getSearchInputField().click();
        getSearchInputField().clear();
        getSearchInputField().sendKeys(searchQuery);
    }

    public WebElement getSearchInputField(){
        return getDriver().findElement(By.xpath("//input[@name='search-text']"));
    }

    public void openRootCategoryByTitle(String rootCategoryTitle) {
        getDriver().findElement(By.xpath("//li/a[contains(text(),'"+rootCategoryTitle+"')]")).click();
    }

    public boolean isInputParamFieldHighlighted(String name){
        return getInputParamFieldByName(name).getAttribute("class").contains("error");
    }

    public boolean isErrorMessagePresent(String message){
        String errorText = getDriver().findElement(By.xpath("//*[@id='input-error-msg']")).getText().trim();
        return message.equals(errorText);
    }
}
