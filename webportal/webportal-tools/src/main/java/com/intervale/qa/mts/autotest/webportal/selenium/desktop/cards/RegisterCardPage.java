package com.intervale.qa.mts.autotest.webportal.selenium.desktop.cards;

import com.intervale.qa.mts.autotest.webportal.selenium.desktop.WhiteboardPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by ashaporev
 */
public class RegisterCardPage extends WhiteboardPage {

    @FindBy(id = "regamountrub")
    private WebElement regAmountRub;

    @FindBy(id = "regamountkop")
    private WebElement regAmountKop;

    @FindBy(id = "regcard")
    private WebElement okButton;

    public RegisterCardPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getRegAmountRub(){
        return regAmountRub;
    }

    public WebElement getRegAmountKop(){
        return regAmountKop;
    }

    public WebElement getOkButton(){
        return okButton;
    }

    public void fillRubValue(String value) {
        regAmountRub.click();
        regAmountRub.sendKeys(value);
    }

    public void fillKopValue(String value) {
        regAmountKop.clear();
        regAmountKop.sendKeys(value);
    }

    public void submitReservedAmount() {
        okButton.click();
    }

    public boolean hasErrorMessage() {
        return getDriver().findElement(By.id("amount-validation-error")).isDisplayed();
    }

    public String getErrorMessage() {
        return getDriver().findElement(By.id("amount-validation-error")).getText().trim();
    }

    public String getAttemptsText(){
        return getDriver().findElement(By.className("attempts-text")).getText();
    }

    public String getFailedText(){
        return getDriver().findElement(By.className("failed-text")).getText();
    }

    public WebElement getFailedDeleteLink(){
        return getDriver().findElement(By.xpath("//strong[@class='failed-text']/a[text()='Удалить']"));
    }
}
