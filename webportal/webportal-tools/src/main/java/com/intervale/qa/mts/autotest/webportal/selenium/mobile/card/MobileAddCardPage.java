package com.intervale.qa.mts.autotest.webportal.selenium.mobile.card;

import com.intervale.qa.mts.autotest.webportal.selenium.SeleniumPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by dchahovsky.
 */
public class MobileAddCardPage extends SeleniumPageObject {

    @FindBy(id = "pan")
    private WebElement panInputElement;

    @FindBy(id = "cvv2")
    private WebElement cvcInputElement;

    @FindBy(id = "alias")
    private WebElement cardAliasElement;

    @FindBy(id = "expyear")
    private WebElement expYearSelectElement;

    @FindBy(id = "btnaccept")
    private WebElement addCardButtonElement;

    @FindBy(tagName = "h1")
    private WebElement title;

    @FindBy(id = "msgprocessing")
    private WebElement msgprocessingDiv;

    public MobileAddCardPage(WebDriver driver) {
        super(driver);
    }

    public String getTitle() {
        return title.findElement(By.xpath("//body/h1")).getText();
    }

    public WebElement getCardNameInputField() {
        return cardAliasElement;
    }

    public void fillPan(String pan) {
        panInputElement.click();
        panInputElement.sendKeys(pan);
        //  throw new RuntimeException("todo");
    }

    public void selectYear(String year) {
        expYearSelectElement.click();
        expYearSelectElement.click();
        Select mySelect = new Select(expYearSelectElement);
        mySelect.selectByValue(year.substring(2));
    }

    public void fillCvc(String cvc) {
        cvcInputElement.click();
        cvcInputElement.sendKeys(cvc);
    }

    public void fillCardName(String cardName) {
        cardAliasElement.click();
        cardAliasElement.clear();
        cardAliasElement.sendKeys(cardName);
    }

    public void submitAddCard() {
        addCardButtonElement.click();
    }

    public void clickCVV() {
        cvcInputElement.click();
    }


    /**
     * Проверяет выболняется ли в данный момент обработка добавления карты
     *
     * @return true, если элемент виден
     */
    public boolean isProcessing() {
        return msgprocessingDiv.isDisplayed();
    }

    public void panClick() {
        panInputElement.click();
    }

    public String getNameCard() {
        return cardAliasElement.getAttribute("value");
    }

}

