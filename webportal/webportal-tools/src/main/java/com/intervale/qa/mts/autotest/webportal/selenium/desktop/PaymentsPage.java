package com.intervale.qa.mts.autotest.webportal.selenium.desktop;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dchahovsky.
 */
public class PaymentsPage extends WhiteboardPage {

    @FindBy(xpath = "//h2[@class='gradient']/a/text()")
    private List<String> categories;

    @FindBy(className = "menu-title-wrap")
    private List<WebElement> menuItems;

    public PaymentsPage(WebDriver driver) {
        super(driver);
    }

    /**
     * Opens payment or directory located on main page
     * @param menuItemTitle
     */
    public void openMenuItem(String menuItemTitle) {
        for (WebElement we : menuItems) {
            if (we.getText().trim().equals(menuItemTitle)) {
                we.click();
                return;
            }
        }
        throw new RuntimeException("Menu item '" + menuItemTitle + "' was not found on page");
    }

    public boolean containsMenuItem(String menuItemTitle) {
        for (WebElement we : menuItems) {
            if (we.getText().trim().equals(menuItemTitle)) {
                return true;
            }
        }
        return false;
    }

    public void openRootCategoryByTitle(String rootCategoryTitle) {
        getDriver().findElement(By.xpath("//h2[@class='gradient']/a[contains(text(),'"+rootCategoryTitle+"')]")).click();
    }

    public List<String> listRootCategories() {
        List<String> categoriesList = new ArrayList<>();
        for(WebElement e : getDriver().findElements(By.xpath("//h2[@class='gradient']/a"))){
            categoriesList.add(e.getText().trim());
        }
        return categoriesList;
    }

    public void searchForPayment(String searchQuery) {
        getSearchInputField().click();
        getSearchInputField().clear();
        getSearchInputField().sendKeys(searchQuery);
    }

    public List<String> getDropDownSearchHints(){
        List<String> searchHints = new ArrayList<>();
        for(WebElement e : getDriver().findElements(By.xpath("//ul[@id='ui-id-1' and contains(@style, 'display: block')]/li/a"))){
            searchHints.add(e.getText());
        }
        return searchHints;
    }

    public void clickSearchHint(String hintText){
        getDriver().findElement(By.xpath("//ul[@id='ui-id-1' and contains(@style, 'display: block')]/li/a[text()='" + hintText + "']")).click();
    }

    public WebElement getSearchInputField(){
        return getDriver().findElement(By.xpath("//input[@name='search-text']"));
    }

    public void changeRegion(String newRegionTitle) {
        getDriver().findElement(By.className("region")).findElement(By.xpath("//a[@class='jqTransformSelectOpen']")).click();
        getDriver().findElement(By.xpath("//body/div[@class='jqTransformSelectWrapper']/ul/li/a[text()='"+newRegionTitle+"']")).click();
    }

    public String getSelectedRegionTitle() {
        return getDriver().findElement(By.className("region")).findElement(By.xpath("//div[contains(@style, 'display: block')]//span")).getText();
    }

    public List<String> listAvailableRegions() {
        List<String> regionsNames = new ArrayList<>();
        getDriver().findElement(By.className("region")).findElement(By.xpath("//a[@class='jqTransformSelectOpen']")).click();
        List<WebElement> regions = getDriver().findElements(By.xpath("//body/div[@class='jqTransformSelectWrapper']/ul/li/a"));
        for (WebElement region: regions){
            regionsNames.add(region.getText());
        }
        return regionsNames;
    }

    public void openAllPaymentsTree() {
        getDriver().findElement(By.xpath("//span[@class='full-services']/a")).click();
    }

    public String getNewsHeader() {
        WebElement newsBlock = getDriver().findElement(By.className("h-lnews-wrap"));
        WebElement newsHeader = newsBlock.findElement(By.tagName("h2"));
        return newsHeader.getText();
    }

    public List<WebElement> getNews() {
        WebElement newsBlock = getDriver().findElement(By.className("h-lnews-wrap"));
        return newsBlock.findElements(By.className("b-lnews"));
    }

    public List<String> listPaymentTitlesInRootCategory(String rootCategoryTitle) {
        WebElement grid = getDriver().findElement(By.className("grid3"));
        //  Мобильный телефон
        WebElement rowWithCategoryTitle = grid.findElement(By.xpath("./tbody/tr[normalize-space(td/h2/a/text())='" + rootCategoryTitle + "']"));
        //WebElement rowWithCategoryTitle = grid.findElement(By.xpath("./tbody/tr[td/h2/text() = '" + rootCategoryTitle + "']"));
        int position = Integer.valueOf(rowWithCategoryTitle
                .findElement(By.xpath("count(./td[normalize-space(h2/a/text())='" + rootCategoryTitle + "']/preceding-sibling::*)+1")).getText());
        WebElement rowWithPayments = rowWithCategoryTitle.findElement(By.xpath("./following-sibling::tr[2]"));
        WebElement currentCategoryPaymentsTable = rowWithPayments.findElement(By.xpath(".//table[@class='grid-in-table' and position()=" + position + "]"));
        List<String> paymentsInCategory = new ArrayList<>();
        for (WebElement e : currentCategoryPaymentsTable.findElements(By.xpath(".//span[@class='menu-title-wrap']"))) {
            paymentsInCategory.add(e.getText().trim());
        }
        return paymentsInCategory;
    }

    public String getPageHeader() {
        return getDriver().findElement(By.tagName("h1")).getText();
    }

    public WebElement getAllPaymentsLink() {
        return getDriver().findElement(By.className("full-services")).findElement(By.tagName("a"));
    }

    public WebElement getLinkToMobileSiteVersion() {
        return getDriver().findElement(By.className("mobile"));
    }

}
