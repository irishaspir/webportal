package com.intervale.qa.mts.autotest.webportal.selenium.mobile.card;

import com.intervale.qa.mts.autotest.webportal.selenium.ElementFinder;
import com.intervale.qa.mts.autotest.webportal.selenium.SeleniumPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by iosdeveloper on 15/04/16.
 */
public class MobileCardLine extends SeleniumPageObject {

    @FindBy(className = "card-type")
    private WebElement cardTypeImage;

    @FindBy(className = "card-name")
    private WebElement nameElement;

    @FindBy(className = "card-pan")
    private WebElement panElement;

    @FindBy(className = "card-status")
    private WebElement stateElement;

    @FindBy(className = "card-action")
    private WebElement actionElement;

    public MobileCardLine(WebDriver driver,SearchContext searchContext) {
        super(driver,searchContext);}

    public String getCardName(){
        return nameElement.getText().trim();
    }

    public String getPan(){
        return panElement.getText().trim();
    }

    public String getStateText(){
        String state = stateElement.getText().trim();
        if (!state.equals("")){
            return state;
        } else {
            WebElement stateLink = ElementFinder.searchElement(stateElement, By.xpath(".//a"));
            return stateLink.getText().trim();
        }
    }

    public  WebElement getStateElement(){
        return stateElement.findElement(By.tagName("a"));
    }

    public void clickOnState() {
        stateElement.findElement(By.tagName("a")).click();
    }

    public WebElement getDeleteLink(){
        return actionElement.findElement(By.xpath(".//a[text()='Удалить']"));
    }

    public WebElement getEditLink(){
        return actionElement.findElement(By.xpath(".//a[text()='Переименовать']"));
    }

    public WebElement getCardTypeImage(){
        return cardTypeImage;
    }
}
