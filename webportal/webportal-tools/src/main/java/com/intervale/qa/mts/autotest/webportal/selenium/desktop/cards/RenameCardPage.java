package com.intervale.qa.mts.autotest.webportal.selenium.desktop.cards;

import com.intervale.qa.mts.autotest.webportal.selenium.desktop.WhiteboardPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by dchahovsky.
 */
public class RenameCardPage extends WhiteboardPage {

    @FindBy(id = "new_alias")
    private WebElement newCardAliasElement;

    @FindBy(xpath = "//div[@class='btns']/button[2]")
    private WebElement saveButtonElement;

    public RenameCardPage(WebDriver driver) {
        super(driver);
    }

    public String getCardAlias() {
        return newCardAliasElement.getAttribute("value");
    }

    public void fillNewAlias(String alias) {
        newCardAliasElement.click();
        newCardAliasElement.clear();
        newCardAliasElement.sendKeys(alias);
    }

    public CardsListPage save() {
        saveButtonElement.click();
        return new CardsListPage(getDriver());
    }

}
