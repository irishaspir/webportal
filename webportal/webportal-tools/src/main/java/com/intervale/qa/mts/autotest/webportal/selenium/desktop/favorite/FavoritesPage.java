package com.intervale.qa.mts.autotest.webportal.selenium.desktop.favorite;

import com.intervale.qa.mts.autotest.webportal.selenium.ElementFinder;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.WhiteboardPage;
import org.openqa.selenium.*;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static ru.yandex.qatools.matchers.webdriver.DisplayedMatcher.displayed;
import static ru.yandex.qatools.matchers.webdriver.ExistsMatcher.exists;

/**
 * Created by dchahovsky.
 */
public class FavoritesPage extends WhiteboardPage {

    private List<FavoritePaymentLine> favoritesList = new ArrayList<>();

    public FavoritesPage(WebDriver driver) {
        super(driver);
        for (WebElement e : getDriver().findElements(By.xpath("//table[@class='pay-table']/tbody/tr[position()>1]"))) {
            favoritesList.add(new FavoritePaymentLine(getDriver(), e));
        }
    }

    public List<String> listAvailablePages() {
        List<WebElement> elList = getDriver().findElements(By.xpath("//div[@class='pager']/ul/li/a/text()"));
        List<String> sList = new ArrayList<>();
        for (WebElement e : elList) {
            sList.add(e.getText());
        }
        return sList;
    }

    public List<String> listFavoritePayments() {
        List<WebElement> favElmList = getDriver().findElement(By.id("mainTable")).findElements(By.xpath("./tbody/tr[position()>1]/td[1]/a"));
        List<String> favTitleList = new ArrayList<>();
        for (WebElement e : favElmList) {
            favTitleList.add(e.getText().trim());
        }
        return favTitleList;
    }

    public List<FavoritePaymentLine> getFavoritesList(){
        return favoritesList;
    }

    public boolean containsPayment(String paymentTitle){
        for (FavoritePaymentLine line : favoritesList){
            if (paymentTitle.equals(line.getTitle())){
                return true;
            }
        }
        return false;
    }
    public FavoritePaymentLine getPaymentLine(String paymentTitle){
        for (FavoritePaymentLine line : favoritesList){
            if (paymentTitle.equals(line.getTitle())){
                return line;
            }
        }
        return null;
    }

    public WebElement getAddToFavoriteLink(){
        return getDriver().findElement(By.id("f_hrefAddToFavorites"));
    }

    public void confirmDelete() {
        // Waiting for devs to fix bug with multiple ids:
        // https://jiraf.intervale.ru/browse/WEBMTS-220
        //getDriver().findElement(By.id("deleteFavPayment")).findElements(By.xpath(".//button[contains(concat(' ', @class, ' '), ' actnbtn ')]"));
        assertThat("Delete form should exists on page", ElementFinder.searchElement(getDriver(), By.id("popup_form")), exists());
        assertThat("Delete form should be displayed", ElementFinder.searchElement(getDriver(), By.id("popup_form")), displayed());
        getDriver().findElement(By.id("popup_form")).findElement(By.xpath(".//button[contains(concat(' ', @class, ' '), ' actnbtn ')]")).click();
    }
}
