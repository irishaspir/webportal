package com.intervale.qa.mts.autotest.webportal.selenium.mobile.favorite;

import org.openqa.selenium.WebElement;

/**
 * Created by ashaporev
 */
public class MobileFavoritePaymentLine{

    private String name;
    private WebElement paymentLink;
    private WebElement editLink;
    private WebElement deleteLink;
    private WebElement description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public WebElement getPaymentLink() {
        return paymentLink;
    }

    public void setPaymentLink(WebElement paymentLink) {
        this.paymentLink = paymentLink;
    }

    public WebElement getEditLink() {
        return editLink;
    }

    public void setEditLink(WebElement editLink) {
        this.editLink = editLink;
    }

    public WebElement getDeleteLink() {
        return deleteLink;
    }

    public void setDeleteLink(WebElement deleteLink) {
        this.deleteLink = deleteLink;
    }

    public WebElement getDescription() {
        return description;
    }

}
