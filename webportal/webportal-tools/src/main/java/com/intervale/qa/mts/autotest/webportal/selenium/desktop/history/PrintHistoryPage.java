package com.intervale.qa.mts.autotest.webportal.selenium.desktop.history;

import com.intervale.qa.mts.autotest.webportal.selenium.SeleniumPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ashaporev
 */
public class PrintHistoryPage extends SeleniumPageObject {

    public PrintHistoryPage(WebDriver driver) {
        super(driver);
    }

    public String getSupportPhone(){
        return getDriver().findElement(By.xpath("//*[@class='hp'][1]")).getText();
    }

    public List<PaymentHistoryLine> getHistoryList() {
        List<PaymentHistoryLine> list = new ArrayList<>();
        for (WebElement e : getDriver().findElements(By.xpath("//table[@class='pay-table']/tbody/tr[position()>1]"))) {
            PaymentHistoryLine paymentHistoryLine = new PaymentHistoryLine(getDriver(), e);
            paymentHistoryLine.setPaymentPcid(e.getAttribute("id"));
            list.add(paymentHistoryLine);
        }
        return list;
    }

}
