package com.intervale.qa.mts.autotest.webportal.selenium.desktop.cards;

import com.intervale.qa.mts.autotest.webportal.selenium.ElementFinder;
import com.intervale.qa.mts.autotest.webportal.selenium.SeleniumPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by ashaporev
 */
public class CardLine extends SeleniumPageObject {

    @FindBy(xpath = "./td[1]/img")
    private WebElement cardTypeImage;

    @FindBy(xpath = "./td[2]")
    private WebElement nameElement;

    @FindBy(xpath = "./td[3]")
    private WebElement panElement;

    @FindBy(xpath = "./td[4]")
    private WebElement stateElement;

    @FindBy(xpath = "./td[5]")
    private WebElement actionElement;

    public CardLine(WebDriver driver, SearchContext searchContext) {
        super(driver, searchContext);
    }

    public String getCardName(){
        return nameElement.getText().trim();
    }

    public String getPan(){
        return panElement.getText().trim();
    }

    public String getStateText(){
        String state = stateElement.getText().trim();
        if (!state.equals("")){
            return state;
        } else {
            WebElement stateLink = ElementFinder.searchElement(stateElement, By.xpath(".//a"));
            return stateLink.getText().trim();
        }
    }

    public  WebElement getStateElement(){
        return stateElement.findElement(By.tagName("a"));
    }

    public void clickOnState() {
        stateElement.findElement(By.tagName("a")).click();
    }

    public WebElement getDeleteLink(){
        return actionElement.findElement(By.xpath(".//a[text()='Удалить']"));
    }

    public WebElement getEditLink(){
        return actionElement.findElement(By.xpath(".//a[text()='Переименовать']"));
    }

    public WebElement getCardTypeImage(){
        return cardTypeImage;
    }
}
