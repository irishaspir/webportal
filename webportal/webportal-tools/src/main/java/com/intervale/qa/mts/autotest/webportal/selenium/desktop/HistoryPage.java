package com.intervale.qa.mts.autotest.webportal.selenium.desktop;

import com.intervale.qa.mts.autotest.webportal.selenium.SeleniumPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ispiridonova on 14.10.2015.
 */
public class HistoryPage extends WhiteboardPage {

    public HistoryPage(WebDriver driver) {
        super(driver);
    }

    public void openPage(){
        getDriver().findElement(By.xpath("//a[contains(@href,'/webportal/help')]")).click();
    }

    public List<String> listAllLinks() {
        List<String> linksList = new ArrayList<>();
        for(WebElement e : getDriver().findElements(By.xpath("//ul[contains(@class,'ts-list')]//a"))){
            linksList.add(e.getText().trim());
        }
        return linksList;
    }

}
