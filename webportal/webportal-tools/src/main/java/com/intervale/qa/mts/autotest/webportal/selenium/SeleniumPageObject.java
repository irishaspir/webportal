package com.intervale.qa.mts.autotest.webportal.selenium;

import com.intervale.qa.mts.autotest.webportal.selenium.SearchContextElementLocatorFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.FieldDecorator;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Класс используется для инициализации PageObject-модели
 * <p>
 * Created by dchahovsky.
 */
public abstract class SeleniumPageObject {

    private static final int WAIT_ELEMENT_TIMEOUT = 60;
    private WebDriver driver;
    private SearchContext context;

    public SeleniumPageObject(WebDriver driver) {
        this.driver = driver;
        this.context = null;
        PageFactory.initElements(driver, this);
    }

    public SeleniumPageObject(WebDriver driver, SearchContext searchContext) {
        this.driver = driver;
        this.context = searchContext;
        PageFactory.initElements(new SearchContextElementLocatorFactory(searchContext), this);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public SearchContext getContext() {
        return context;
    }

    /**
     * Search element on the page by String locator
     *
     * @param locator String
     * @return WebElement
     */
    public WebElement searchElement(String locator) {
        new WebDriverWait(getDriver(), WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.presenceOfElementLocated(By.xpath(locator)));
        getDriver().findElements(By.xpath(locator));
        return getDriver().findElement(By.xpath(locator));
    }

    /**
     * Search element on the page by By locator
     *
     * @param locator By
     * @return WebElement
     */
    public WebElement searchElement(By locator) {
        new WebDriverWait(getDriver(), WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.presenceOfElementLocated(locator));
        getDriver().findElements(locator);
        return getDriver().findElement(locator);
    }

}
