package com.intervale.qa.mts.autotest.webportal.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;

/**
 * Created by dchahovsky.
 */
public class ACSPage extends SeleniumPageObject {

    public ACSPage(WebDriver driver) {
        super(driver);
    }

    public void acsAccept() {
        getDriver().findElement(By.id("status-y")).click();
        getDriver().findElement(By.id("submit-answer-button")).click();
        getDriver().findElement(By.id("confirm-button")).click();
    }

    public void acsReject() {
        getDriver().findElement(By.id("status-n")).click();
        getDriver().findElement(By.id("submit-answer-button")).click();
        getDriver().findElement(By.id("confirm-button")).click();
    }

}
