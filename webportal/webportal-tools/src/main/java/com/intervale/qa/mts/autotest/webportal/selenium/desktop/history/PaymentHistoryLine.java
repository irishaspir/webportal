package com.intervale.qa.mts.autotest.webportal.selenium.desktop.history;

import com.intervale.qa.mts.autotest.webportal.selenium.ElementFinder;
import com.intervale.qa.mts.autotest.webportal.selenium.SeleniumPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by dchahovsky.
 */
public class PaymentHistoryLine extends SeleniumPageObject {

    private String paymentPcid;

    public PaymentHistoryLine(WebDriver driver, SearchContext context) {
        super(driver, context);
    }

    public String getDateTime() {
        return getContext().findElement(By.xpath("./td[1]")).getText();
    }

    public String getPaymentSource(){
        return getContext().findElement(By.xpath("./td[2]//img")).getAttribute("title").trim();
    }

    public String getPaymentDescription() {
        return getContext().findElement(By.xpath("./td[2]//a[@class='hrefPayment']")).getText().trim();
    }

    public String getAmount() {
        return getContext().findElement(By.xpath("./td[3]")).getText();
    }

    public String getResult() {
        return getContext().findElement(By.xpath("./td[4]/span")).getText().trim();
    }

    public WebElement getResultElement() {
        return getContext().findElement(By.xpath("./td[4]/span"));
    }

    public String getPaymentPcid(){
        return paymentPcid;
    }

    /**
     *
     * @return link or null if it is absent
     */
    public WebElement getRepeatLink() {
        return ElementFinder.searchElement(getContext(),By.className("repeat"));
    }

    public void setPaymentPcid(String paymentPcid) {
        this.paymentPcid = paymentPcid;
    }
}
