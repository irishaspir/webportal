package com.intervale.qa.mts.autotest.webportal.selenium.mobile.favorite;

import com.intervale.qa.mts.autotest.webportal.selenium.SeleniumPageObject;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.WhiteboardPage;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.favorite.PaymentsList;
import com.intervale.qa.mts.autotest.webportal.selenium.mobile.MobileWhiteboardPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by ashaporev
 */
public class MobileChooseFavoritePaymentPage extends MobileWhiteboardPage {

    private MobilePaymentsList paymentsList;

    public MobileChooseFavoritePaymentPage(WebDriver driver) {
        super(driver);
        paymentsList = new MobilePaymentsList(driver, driver.findElement(By.className("static-lists")));
    }

    public WebElement getPaymentLinkByText(String text) {
        return getDriver().findElement(By.className("static-lists")).findElement(By.linkText(text));
    }

    public MobilePaymentsList getPaymentsList() {
        return paymentsList;
    }
}
