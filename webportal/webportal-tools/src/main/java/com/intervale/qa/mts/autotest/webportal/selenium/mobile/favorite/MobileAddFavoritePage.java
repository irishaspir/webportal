package com.intervale.qa.mts.autotest.webportal.selenium.mobile.favorite;

import com.intervale.qa.mts.autotest.webportal.selenium.SeleniumPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by ashaporev
 */
public class MobileAddFavoritePage extends SeleniumPageObject {

    public MobileAddFavoritePage(WebDriver driver) {
        super(driver);
    }

    public void submit(){
        getDriver().findElement(By.id("btnOk")).click();
    }

    public void cancel(){
        getDriver().findElement(By.id("btncancel")).click();
    }

    public WebElement getInputFieldById(String id){
        return getDriver().findElement(By.id(id));
    }
}
