package com.intervale.qa.mts.autotest.webportal.selenium.mobile.favorite;

import com.intervale.qa.mts.autotest.webportal.selenium.desktop.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by iosdeveloper on 17/05/16.
 */
public class MobileEditFavoritePage extends AbstractPage {

    @FindBy(id = "btnOk")
    private WebElement submit;

    @FindBy(id = "btncancel")
    private WebElement cancel;

    public MobileEditFavoritePage(WebDriver driver) {
        super(driver);
    }

    public void submit(){
        submit.click();
    }

    public void cancel(){
        cancel.click();
    }

    public WebElement getInputFieldById(String id){
        return getDriver().findElement(By.id(id));
    }
}
