package com.intervale.qa.mts.autotest.webportal.selenium.mobile.favorite;

import com.intervale.qa.mts.autotest.webportal.selenium.ElementFinder;
import com.intervale.qa.mts.autotest.webportal.selenium.SeleniumPageObject;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.WhiteboardPage;
import com.intervale.qa.mts.autotest.webportal.selenium.mobile.MobileWhiteboardPage;
import org.hamcrest.MatcherAssert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

import static ru.yandex.qatools.matchers.webdriver.ExistsMatcher.exists;

/**
 * Created by ashaporev
 */
public class MobileFavoritesPage extends MobileWhiteboardPage{

    private List<MobileFavoritePaymentLine> favoritePaymentLines = new ArrayList<>();

    public MobileFavoritesPage(WebDriver driver) {
        super(driver);
        WebElement favoritesTable = driver.findElement(By.xpath("//dl[@class='favs']"));
        int favsCount = favoritesTable.findElements(By.xpath("//dd")).size();
        for (int i=1; i<=favsCount; i++){
            MobileFavoritePaymentLine mobileFavoritePaymentLine = new MobileFavoritePaymentLine();
            String name = favoritesTable.findElement(By.xpath("//dt["+i+"]")).getText();
            mobileFavoritePaymentLine.setName(name);
            WebElement paymentLinks = favoritesTable.findElement(By.xpath("//dd[" + i + "]"));
            mobileFavoritePaymentLine.setPaymentLink(paymentLinks.findElement(By.linkText("Оплатить")));
            mobileFavoritePaymentLine.setEditLink(paymentLinks.findElement(By.linkText("Редактировать")));
            mobileFavoritePaymentLine.setDeleteLink(paymentLinks.findElement(By.linkText("Удалить")));
            favoritePaymentLines.add(mobileFavoritePaymentLine);
        }
    }

    public WebElement getAddToFavoriteLink(){
        return getDriver().findElement(By.linkText("Добавить в Избранное"));
    }

    public boolean containPaymentWithName(String name){
        for (MobileFavoritePaymentLine paymentLine : favoritePaymentLines){
            if (name.equals(paymentLine.getName())){
                return true;
            }
        }
        return false;
    }

    public List<MobileFavoritePaymentLine> getFavoritesList() {
        return favoritePaymentLines;
    }

    public MobileFavoritePaymentLine getPaymentLine(String paymentTitle){
        for (MobileFavoritePaymentLine line : favoritePaymentLines){
            if (paymentTitle.equals(line.getName())){
                return line;
            }
        }
        return null;
    }

    public List<String> listFavoritePayments() {
        List<WebElement> favElmList = getDriver().findElements(By.xpath("//dt"));
        List<String> favTitleList = new ArrayList<>();
        for (WebElement e : favElmList) {
            favTitleList.add(e.getText().trim());
        }
        return favTitleList;
    }

    public void confirmDeleteFavoritePayment() {
        MatcherAssert.assertThat("Delete form should exists on page", ElementFinder.searchElement(getDriver(), By.className("delFav")), exists());
        new WebDriverWait(getDriver(), 5)
                .until(ExpectedConditions.invisibilityOfElementLocated(By.className("delFav")));
        getDriver().findElement(By.xpath("//form[@class='delFav' and not(contains(@style,'display: none'))]//input[@class='favSbmt']")).click();
    }


}
