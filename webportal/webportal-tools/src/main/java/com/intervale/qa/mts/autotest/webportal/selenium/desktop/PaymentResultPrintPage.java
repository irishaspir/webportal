package com.intervale.qa.mts.autotest.webportal.selenium.desktop;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by ashaporev
 */
public class PaymentResultPrintPage extends WhiteboardPage {

    public PaymentResultPrintPage(WebDriver driver) {
        super(driver);
    }

    public String getParamValueByName(String paramName){
        return getDriver().findElement(By.xpath("//th[contains(string(), '"+paramName+"')]/../td")).getText().trim();
    }
}
