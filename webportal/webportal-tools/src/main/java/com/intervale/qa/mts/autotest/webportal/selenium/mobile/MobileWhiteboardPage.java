package com.intervale.qa.mts.autotest.webportal.selenium.mobile;

import com.intervale.qa.mts.autotest.webportal.selenium.SeleniumPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by iosdeveloper on 15/04/16.
 */
public class MobileWhiteboardPage extends SeleniumPageObject {

    public MobileWhiteboardPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "mainTitle")
    private WebElement title;


    @FindBy(xpath = "//h1[not(@class='spanned')]")
    private WebElement titleH;

    public String getPageTitle() {
        return title.getText();
    }

    public String getPageTitleH() {
        return titleH.getText();
    }

}
