package com.intervale.qa.mts.autotest.webportal.selenium.mobile.history;

import com.intervale.qa.mts.autotest.webportal.selenium.desktop.history.PaymentHistoryLine;
import com.intervale.qa.mts.autotest.webportal.selenium.mobile.MobileWhiteboardPage;
import com.intervale.qa.mts.autotest.webportal.selenium.mobile.history.MobilePaymentHistoryLine;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by iosdeveloper on 18/05/16.
 */
public class MobileHistoryPage extends MobileWhiteboardPage {

    @FindBy(id = "dateFrom")
    private WebElement dateFrom;

    @FindBy(id = "dateTo")
    private WebElement dateTo;

    @FindBy(id = "btnprint")
    private WebElement printButton;

    public MobileHistoryPage(WebDriver driver) {
        super(driver);
    }

    public void doFilter(DateTime from, DateTime till) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd.MM.yyyy");
        dateFrom.click();
        dateFrom.clear();
        dateFrom.sendKeys(formatter.print(from));
        dateTo.click();
        dateTo.clear();
        dateTo.sendKeys(formatter.print(till));
        getDriver().findElement(By.xpath("//input[@class='submit']")).click();
    }

    public List<String> listAvailablePages() {
        List<WebElement> elList = getDriver().findElements(By.xpath("//div[@class='pager']/ul/li/a/text()"));
        List<String> sList = new ArrayList<>();
        for (WebElement e : elList) {
            sList.add(e.getText());
        }
        return sList;
    }

    public void goToPage(String pageNumber){
        List<WebElement> elList = getDriver().findElements(By.xpath("//div[@class='pager']/ul/li/a"));
        boolean pageFound = false;
        for (WebElement e : elList) {
            if (pageNumber.equals(e.getText())){
                pageFound = true;
                e.click();
                break;
            }
        }
        if (!pageFound){
            throw new RuntimeException("Page "+pageNumber+" not found");
        }
    }

    public void goToNextPage(){
        List<WebElement> elList = getDriver().findElements(By.xpath("//p[@class='pager']/a"));
        boolean pageFound = false;
        for (WebElement e : elList) {
            if (e.getText().contains("Вперед »")){
                pageFound = true;
                e.click();
                break;
            }
        }
        if (!pageFound){
            throw new RuntimeException("Next page not found");
        }
    }

    public List<MobilePaymentHistoryLine> getHistoryList() {
        List<MobilePaymentHistoryLine> list = new ArrayList<>();
        for (WebElement e : getDriver().findElements(By.xpath("//table[@class='list']/tbody/tr[position()>0]"))) {
            MobilePaymentHistoryLine mobilePaymentHistoryLine = new MobilePaymentHistoryLine(getDriver(), e);
            mobilePaymentHistoryLine.setPaymentPcid(e.getAttribute("id"));
            list.add(mobilePaymentHistoryLine);
        }
        return list;
    }

    public int getFoundPaymentsCount(){
        String paymentsCountString = getDriver().findElement(By.xpath("//*[contains(text(), 'Всего платежей:')]")).getText();
        String paymentsCount = paymentsCountString.replaceAll("Всего платежей:", "").trim().replace(".", "");
        return Integer.parseInt(paymentsCount);
    }

    public WebElement getWeekFilterLink(){
        return getDriver().findElement(By.xpath("//*[@class='small']//a[text()='неделя']"));
    }

    public WebElement getMonthFilterLink(){
        return getDriver().findElement(By.xpath("//*[@class='small']//a[text()='месяц']"));
    }

    public WebElement get3MonthFilterLink(){
        return getDriver().findElement(By.xpath("//*[@class='small']//a[text()='3 месяца']"));
    }

    public WebElement get6MonthFilterLink(){
        return getDriver().findElement(By.xpath("//*[@class='small']//a[text()='6 месяцев']"));
    }

    @Deprecated
    public void setViewPaymentsOnPageCount(int count){
        if (count==10 || count==25 || count==50 || count==100){
            getDriver().findElement(By.className("jqTransformSelectOpen")).click();
            getDriver().findElement(By.className("jqTransformSelectWrapper"))
                    .findElement(By.linkText(Integer.toString(count))).click();
        } else {
            throw new RuntimeException("Can't view "+count+" payments on page. Select from: 10, 25, 50, 100");
        }
    }

    public WebElement getDatePeriodFrom() {
        return dateFrom;
    }

    public WebElement getDatePeriodTo() {
        return dateTo;
    }

    public WebElement getPrintButton() {
        return printButton;
    }
}
