package com.intervale.qa.mts.autotest.webportal.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;

/**
 * Created by ashaporev
 */
public class ElementFinder {

    public static WebElement searchElement(SearchContext context, By by){
        try {
            return context.findElement(by);
        } catch (NoSuchElementException e){
            return null;
        }
    }
}
