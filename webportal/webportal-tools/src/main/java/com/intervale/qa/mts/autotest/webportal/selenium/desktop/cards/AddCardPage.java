package com.intervale.qa.mts.autotest.webportal.selenium.desktop.cards;

import com.intervale.qa.mts.autotest.webportal.selenium.desktop.WhiteboardPage;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.LoggerFactory;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by ashaporev
 */
public class AddCardPage extends WhiteboardPage {

    @FindBy(id = "pan")
    private WebElement panInputField;

    @FindBy(id = "cvv2")
    private WebElement cvvInputField;

    @FindBy(id = "alias")
    private WebElement cardNameInputField;

    @FindBy(id = "btnaccept")
    private WebElement acceptButton;

    @FindBy(id = "msgprocessing")
    private WebElement msgprocessingDiv;

    @FindBy(tagName = "h1")
    private WebElement title;

    public AddCardPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getPanInputField(){
        return panInputField;
    }

    public WebElement getCvvInputField(){
        return cvvInputField;
    }

    public WebElement getCardNameInputField(){
        return cardNameInputField;
    }

    public String getTitle(){
        return title.getText();
    }

    public void fillPan(String pan) {
        /*
        Страшный костыль, но по-другому не получилось.
        Из-за маскированного ввода при sendKeys курсор иногда поадает в конец и значение не вводится.
         */
        JavascriptExecutor jse = (JavascriptExecutor) getDriver();
        jse.executeScript("arguments[0].value = '" + pan + "'", panInputField);
        panInputField.click();
        //panInputField.sendKeys(pan);
    }

    public void fillCvc(String cvc) {
        cvvInputField.click();
        cvvInputField.clear();
        cvvInputField.sendKeys(cvc);
    }

    public void fillName(String name) {
        cardNameInputField.click();
        cardNameInputField.clear();
        cardNameInputField.sendKeys(name);
    }

    public void selectExpYear(String year){
        getDriver().findElement(By.xpath("//*[@id='tdexpiry']/div[select[@id='expyear']]/div/a")).click();
        WebDriverWait wait = new WebDriverWait(getDriver(), 5);
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='jqtransform-big-elems']/div/ul/li/a[text()='" + year + "']")));
        element.click();
    }

    /**
     * Проверяет выболняется ли в данный момент обработка добавления карты
     * @return true, если элемент виден
     */
    public boolean isProcessing() {
        return msgprocessingDiv.isDisplayed();
    }

    public WebElement getAcceptButton(){
        return acceptButton;
    }




}
