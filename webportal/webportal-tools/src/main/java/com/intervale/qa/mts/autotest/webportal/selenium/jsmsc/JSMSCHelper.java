package com.intervale.qa.mts.autotest.webportal.selenium.jsmsc;

import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by dchahovsky.
 */
public class JSMSCHelper {

    // Delay before getting
    private static final long GET_SMS_LIST_INTERVAL_MILLIS = 1_000;
    // Getting timeout
    private static final long GET_SMS_TIMEOUT_MILLIS = 50_000;

    private static final int DEFAULT_FETCH_PERIOD_IN_SECONDS = 10;

    private String baseUrl;
    private int fetchPeriodInSeconds;

    public JSMSCHelper(String jsmscBaseUrl) {
        this.baseUrl = jsmscBaseUrl;
        this.fetchPeriodInSeconds = DEFAULT_FETCH_PERIOD_IN_SECONDS;
    }

    public List<Sms> getLatestSmsListForMsisdn(String msisdn) {
        RestTemplate template = new RestTemplate();
        String url = UriComponentsBuilder.fromHttpUrl(baseUrl + "/get-sms-list")
                .queryParam("destination-address", msisdn)
                .queryParam("within-seconds", fetchPeriodInSeconds)
                .build().toUriString();
        List<Map> smsMap= template.getForObject(url, List.class);
        List<Sms> smsList = new ArrayList<>();
        for (Map sms : smsMap){
            Date date = new Date((long)sms.get("ts")); // todo Учесть timezone
            String sourceAddr = (String)sms.get("sourceAddr");
            String destinationAddr = (String)sms.get("destinationAddr");
            String bodyText = (String)sms.get("bodyText");
            smsList.add(new Sms(date, sourceAddr, destinationAddr, bodyText));
        }
        return smsList;
    }

    /**
     * Отправка SMS от имени клиента
     * @param sourceMsisdn номер телефона клиента
     * @param destinationMsisdn номер телефона получателя (короткий номер сервиса)
     * @param smsText текст сообщения (должен быть не более 60 символов, разбиение не работает)
     * @return успешность отправки запроса в AAG.
     */
    public boolean sendSms(String sourceMsisdn, String destinationMsisdn, String smsText) {
        RestTemplate template = new RestTemplate();
        String url = UriComponentsBuilder.fromHttpUrl(baseUrl + "/do-send-sms")
                .queryParam("source-address", sourceMsisdn)
                .queryParam("destination-address", destinationMsisdn)
                .queryParam("sms-text", smsText)
                .build().toUriString();
        return template.getForObject(url, Boolean.class);
    }

    /**
     * Ожидание SMS в сторону клиента
     * @param msisdn номер телефона клиента
     * @param expectedSmsCount сколько сообщение необходимо дождаться
     * @return список сообщений, отправленных клиенту
     */
    public List<Sms> waitForSmsForMsisdn(String msisdn, int expectedSmsCount){
        long waitUntilTs = System.currentTimeMillis() + GET_SMS_TIMEOUT_MILLIS;
        List<Sms> smsList = getLatestSmsListForMsisdn(msisdn);
        while(smsList.size() < expectedSmsCount && System.currentTimeMillis() < waitUntilTs){
            try {
                TimeUnit.MILLISECONDS.sleep(GET_SMS_LIST_INTERVAL_MILLIS);
            } catch (InterruptedException e) {
                throw new RuntimeException("Sleep interrupted", e);
            }
            smsList = getLatestSmsListForMsisdn(msisdn);
        }
        if (smsList.size() < expectedSmsCount){
            throw new RuntimeException("Timeout reached waiting for " + expectedSmsCount
                    + " SMS. Actual count: " + smsList.size()
                    + "\n" + smsList.toString());
        }
        return smsList;
    }

}
