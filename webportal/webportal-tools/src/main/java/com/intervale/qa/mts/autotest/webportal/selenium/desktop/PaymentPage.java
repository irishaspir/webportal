package com.intervale.qa.mts.autotest.webportal.selenium.desktop;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dchahovsky.
 */
public class PaymentPage extends WhiteboardPage {

    @FindBy(id = "tablePaymentParams")
    private WebElement paymentParamsTable;

    @FindBy(id = "cardform")
    private WebElement cardForm;

    @FindBy(id = "full-amount-wrapper")
    private WebElement fullAmountBlock;

    @FindBy(id = "btnOK")
    private WebElement submitButton;

    public PaymentPage(WebDriver driver) {
        super(driver);
    }

    public List<String> listInputParamNames() {
        List<WebElement> inputParams = paymentParamsTable.findElements(By.className("inputParam"));
        List<String> inputParamNames = new ArrayList<>();
        for (WebElement e : inputParams) {
            inputParamNames.add(e.getAttribute("name"));
        }
        return inputParamNames;
    }

    public WebElement getInputParamFieldByName(String name) {
        return paymentParamsTable.findElement(By.name(name));
    }

    public boolean isInputParamFieldHighlighted(String name){
        return getInputParamFieldByName(name).getAttribute("class").contains("error");
    }

    public boolean isErrorMessagePresent(String message){
        String errorText = getDriver().findElement(By.xpath("//*[@id='input-error-msg']/div")).getText().trim();
        return message.equals(errorText);
    }

    public void submit(){
        fullAmountBlock.click();
        new WebDriverWait(getDriver(), 2).until(ExpectedConditions.elementToBeClickable(submitButton));
        submitButton.click();
    }

    public List<WebElement> getRegisteredCardRadioButtons(){
        //return getDriver().findElements(By.xpath("//li[contains(concat(' ', @class, ' '), ' from-registered-card ')]//a"));
        return getDriver().findElements(By.className("from-registered-card"));
    }

    public List<String> listRegisteredCardNames() {
        //List<WebElement> labelList = getDriver().findElements(By.xpath("//li[contains(concat(' ', @class, ' '), ' from-registered-card ')]/label"));
        List<WebElement> labelList = getDriver().findElements(By.className("from-registered-card"));
        List<String> cardTitleList = new ArrayList<>();
        for (WebElement label : labelList) {
            String text = label.getText().trim();
            if (text.matches("Банковской картой .*")) {
                cardTitleList.add(text.substring(18));
            }
            else {
                throw new IllegalStateException("Card title should start with text 'Банковской картой ', but it is: " + text);
            }
        }
        return cardTitleList;
    }

    public void selectRegisteredCardByTitle(String cardTitle) {
        getDriver().findElement(By.xpath("//li[@class='from-registered-card']//label[ends-with(.,'" + cardTitle + "')]/../span/a")).click();
    }

    public WebElement getUnregisteredCardRadioButton(){
        if (getDriver().manage().getCookieNamed("fake-msisdn") != null){
            return getDriver().findElement(By.xpath("//li[@class='from-unregistered-card dynamic-li']//a"));
        }
        return getDriver().findElement(By.xpath("//li[@class='radio payable dynamic-li']//a"));
    }

    public WebElement getAccountRadioButton(){
        return getDriver().findElement(By.xpath("//li[@class='from-account pay-show']//a"));
    }

    public void selectCardByName(String name){
        WebElement label;
        for(WebElement e : getRegisteredCardRadioButtons()){
            label = e.findElement(By.xpath("./label"));
            String text = label.getText();
            if (text != null && text.endsWith(name)){
                label.click();
                // todo verify it is selected
                return;
            }
        }
        throw new IllegalStateException("Карта с именем '" + name + "' не найдена");
    }

    public void selectCardExpYear(String year){
        getDriver().findElement(By.xpath("//*[@id='tdexpiry']/td/div[select[@id='card-expyear']]/div/a")).click();
        WebDriverWait wait = new WebDriverWait(getDriver(), 5);
        // todo investigate wrong year selection
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='jqtransform-big-elems']/div/ul/li/a[text()='" + year + "']")));
        element.click();
    }

    /**
     * Костыль вместо selectCardExpYear
     */
    public void selectSomeFutureCardExpYear(){
        getDriver().findElement(By.xpath("//*[@id='tdexpiry']/td/div[select[@id='card-expyear']]/div/a")).click();
        WebDriverWait wait = new WebDriverWait(getDriver(), 5);
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='jqtransform-big-elems']/div/ul/li[7]/a")));
        element.click();
    }

    public WebElement getCvvInputField(){
        return cardForm.findElement(By.id("card-cvv2"));
    }

    public WebElement getPanInputField(){
        return cardForm.findElement(By.id("card-pan"));
    }

    public WebElement getCardHolderNameInputField(){
        return cardForm.findElement(By.id("card-cardHolderName"));
    }

    public String getPaymentSourceName() {
        return paymentParamsTable.findElement(By.xpath("//input[@type='radio' and  @checked='checked']")).getAttribute("value");
    }

    public WebElement getOfferLink(){
        return getDriver().findElement(By.id("offerLink"));
    }
}