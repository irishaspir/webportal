package com.intervale.qa.mts.autotest.webportal.selenium.mobile.card;

import com.intervale.qa.mts.autotest.webportal.selenium.ElementFinder;
import com.intervale.qa.mts.autotest.webportal.selenium.mobile.MobileWhiteboardPage;
import org.hamcrest.MatcherAssert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static ru.yandex.qatools.matchers.webdriver.ExistsMatcher.exists;

/**
 * Created by dchahovsky.
 */
public class MobileCardsListPage extends MobileWhiteboardPage {


    @FindBy(id = "addcard")
    private WebElement addCardButtonElement;

    private List<MobileCardLine> mobileCardList = new ArrayList<>();


    public MobileCardsListPage(WebDriver driver) {
        super(driver);
        assertThat("Не та страница?", getPageTitleH(), equalTo("Мои карты"));
        for (WebElement e : getDriver().findElements(By.xpath("//form[@id='addcardform']//div[@class='card-type']/parent::div"))) {
            mobileCardList.add(new MobileCardLine(getDriver(), e));
        }
    }

    public List<MobileCardLine> getCardList() {
        return mobileCardList;
    }

    public boolean hasCard(String cardName) {
        List<WebElement> list = getDriver().findElements(By.xpath("//div[@class='card-name']"));
        for (WebElement we : list) {
            if (cardName.equals(we.getText().trim())) {
                return true;
            }
        }
        return false;
    }

    public String getCardStateName(String cardName) {
        List<WebElement> list = getDriver().findElement(By.className("h-pt-wrap")).findElements(By.tagName("div"));
        for (int i = 0; i < list.size(); i++) {
            if ("card-name".equals(list.get(i).getAttribute("class"))) {
                return list.get(i + 2).getText().trim();
            }
        }
        throw new RuntimeException("Карта не найдена: " + cardName);
    }

    public MobileCardLine getCardLineByName(String cardName) {
        for (MobileCardLine line : mobileCardList) {
            if (cardName.equals(line.getCardName())) {
                return line;
            }
        }
        throw new IllegalStateException("Карта не найдена: " + cardName);
    }



    public void clickOnCardState(String cardName) {
        List<WebElement> list = getDriver().findElement(By.className("h-pt-wrap")).findElements(By.tagName("div"));
        for (int i = 0; i < list.size(); i++) {
            if ("card-name".equals(list.get(i).getAttribute("class"))) {
                list.get(i + 2).findElement(By.tagName("a")).click();
                return;
            }
        }
        throw new RuntimeException("Карта не найдена: " + cardName);
    }

    public void addNewCard() {
        addCardButtonElement.click();
    }

    public MobileAddCardPage addCard() {
        getDriver().findElement(By.id("addcard")).click();
        return new MobileAddCardPage(getDriver());
    }

    public WebElement getVirtualCardLink(){
        return getDriver().findElement(By.xpath("//a[@id='virtualcardhref' and contains(text(), 'Виртуальная карта МТС Деньги')]"));
    }

    public void confirmDeleteCard() {
        MatcherAssert.assertThat("Delete form should exists on page", ElementFinder.searchElement(getDriver(), By.className("delCard")), exists());
        new WebDriverWait(getDriver(), 5)
                .until(ExpectedConditions.invisibilityOfElementLocated(By.className("delCard")));
        getDriver().findElement(By.xpath("//div[@class='delCard' and not(contains(@style,'display: none'))]//input[@class='cardSbmt']")).click();
    }

}
