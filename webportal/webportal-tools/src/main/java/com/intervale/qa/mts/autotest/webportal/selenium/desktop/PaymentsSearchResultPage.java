package com.intervale.qa.mts.autotest.webportal.selenium.desktop;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Thread.sleep;

/**
 * Created by ashaporev
 */
public class PaymentsSearchResultPage extends WhiteboardPage {
    public PaymentsSearchResultPage(WebDriver driver) {
        super(driver);
    }

    public List<String> getListPaymentsTitles(){
        List<String> paymentsTitles = new ArrayList<>();
        for(WebElement e :getDriver().findElements(By.xpath("//div[@id='result-search']//div[@class='b-pay-col']//a/div"))){
            paymentsTitles.add(e.getText().trim());
        }
        return paymentsTitles;
    }

    public void openPayment(String paymentName){
        getDriver().findElement(By.xpath("//div[@class='b-pay-col']//a/div[text()='"+paymentName+"']")).click();
    }

    public String getAnswerMessage(){
        try {
            sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return getDriver().findElement(By.xpath("//div[@id='result-search']")).getText();
    }
}
