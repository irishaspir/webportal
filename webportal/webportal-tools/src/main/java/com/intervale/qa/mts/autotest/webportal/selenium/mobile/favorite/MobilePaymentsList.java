package com.intervale.qa.mts.autotest.webportal.selenium.mobile.favorite;

import com.intervale.qa.mts.autotest.webportal.selenium.SeleniumPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by iosdeveloper on 16/05/16.
 */
public class MobilePaymentsList extends SeleniumPageObject {

    public MobilePaymentsList(WebDriver driver, SearchContext searchContext) {
        super(driver, searchContext);
    }

    public void selectByTree(List<String> treePath) {
        WebElement dl = null;
        for (String cat : treePath) {
            // На случай, если в конце прохода есть платеж и кат с одинаковым именем
            dl = cat.equals(treePath.get(treePath.size() - 1)) ? getDriver().findElement(By.xpath("//dl[.//dd//a/text()='" + cat + "']/dd")) :
                    getDriver().findElement(By.xpath("//dl[.//dt//a/text()='" + cat + "']/dt"));
            dl.findElement(By.xpath(".//a")).click();
        }
    }
}

