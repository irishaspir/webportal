package com.intervale.qa.mts.autotest.webportal.selenium.mobile.card;

import com.intervale.qa.mts.autotest.webportal.selenium.SeleniumPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by dchahovsky.
 */
public class MobileCardRegistrationPage extends SeleniumPageObject {

    @FindBy(id = "regamountrub")
    private WebElement rubInputElement;

    @FindBy(id = "regamountkop")
    private WebElement kopInputElement;

    @FindBy(id = "regcard")
    private WebElement okButtonElement;

    public MobileCardRegistrationPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getRegAmountRub(){
        return rubInputElement;
    }

    public WebElement getRegAmountKop(){
        return rubInputElement;
    }

    public WebElement getOkButton(){
        return okButtonElement;
    }

    public void fillRubValue(String value) {
        rubInputElement.click();
        rubInputElement.sendKeys(value);
    }

    public void fillKopValue(String value) {
        kopInputElement.clear();
        kopInputElement.sendKeys(value);
    }

    public void submitReservedAmount() {
        okButtonElement.click();
    }

    public boolean hasErrorMessage() {
        return getDriver().findElement(By.id("amount-validation-error")).isDisplayed();
    }

    public String getErrorMessage() {
        return getDriver().findElement(By.id("amount-validation-error")).getText().trim();
    }

    public String getAttemptsText(){
        return getDriver().findElement(By.className("attempts-text")).getText();
    }

    public String getFailedText(){
        return getDriver().findElement(By.className("failed-text")).getText();
    }

    public WebElement getFailedDeleteLink(){
        return getDriver().findElement(By.xpath("//strong[@class='failed-text']/a[text()='Удалить']"));
    }



}
