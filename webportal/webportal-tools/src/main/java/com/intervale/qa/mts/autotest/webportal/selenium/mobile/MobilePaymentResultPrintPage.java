package com.intervale.qa.mts.autotest.webportal.selenium.mobile;

import com.intervale.qa.mts.autotest.webportal.selenium.desktop.WhiteboardPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by ispiridonova on 14.03.2016.
 */
public class MobilePaymentResultPrintPage extends WhiteboardPage {

    public MobilePaymentResultPrintPage(WebDriver driver) {
        super(driver);
    }

    public String getParamValueByName(String paramName){
        return getDriver().findElement(By.xpath("//th[contains(string(), '"+paramName+"')]/../td")).getText().trim();
    }

}
