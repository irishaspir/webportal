package com.intervale.qa.mts.autotest.webportal.selenium.desktop;

import com.intervale.qa.mts.autotest.webportal.selenium.SeleniumPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dchahovsky.
 */
public class LeftMenu extends SeleniumPageObject {
    private static Logger logger = LoggerFactory.getLogger(LeftMenu.class);

    @FindBy(xpath = ".//li/a")
    List<WebElement> leftMenuLinks;

    public LeftMenu(WebDriver driver, SearchContext context) {
        super(driver, context);
    }

    public boolean hasLinkWithName(String linkName) {
        for (WebElement e : leftMenuLinks){
            if(e.getText().trim().equals(linkName)){
                return true;
            }
        }
        return false;
    }

    public boolean openLinkByName(String linkName) {
        for (WebElement e : leftMenuLinks){
            if(e.getText().trim().equals(linkName)){
                e.click();
                return true;
            }
        }
        return false;
    }

    public WebElement getLinkByName(String linkName) {
        for (WebElement e : leftMenuLinks){
            if(e.getText().trim().equals(linkName)){
                return e;
            }
        }
        return null;
    }

    public List<String> getFirstLevelLinkTexts() {
        List<String> list = new ArrayList<>();
        for (WebElement e : getContext().findElements(By.xpath("./ul/li/a"))) {
            list.add(e.getText());
        }
        return list;
    }

    public List<String> getSecondLevelLinkTexts(String firstLevelLinkText) {
        List<String> list = new ArrayList<>();
        for (WebElement e : getContext().findElements(By.xpath("./ul/li[normalize-space(a/text())='" + firstLevelLinkText + "']/ul/li/a"))) {
            list.add(e.getText());
        }
        logger.debug("Left menu category {} has {} sub-items.", firstLevelLinkText, list.size());
        return list;
    }

}
