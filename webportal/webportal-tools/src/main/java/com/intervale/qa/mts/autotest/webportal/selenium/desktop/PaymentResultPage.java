package com.intervale.qa.mts.autotest.webportal.selenium.desktop;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by ashaporev
 */
public class PaymentResultPage extends WhiteboardPage {

    public PaymentResultPage(WebDriver driver) {
        super(driver);
    }

    public String getResultText(){
        return getDriver().findElement(By.id("adsms_Result")).getText();
    }

    public String getMenuId(){
        String url = getDriver().getCurrentUrl();
        return url.substring(url.lastIndexOf("/") + 1, url.indexOf("?"));
    }

    public String getPcid(){
        String url = getDriver().getCurrentUrl();
        String params = url.substring(url.indexOf("?") + 1);
        for (String param : params.split("&")){
            String[] paramPair = param.split("=");
            if ("pcid".equals(paramPair[0])){
                return paramPair[1];
            }
        }
        return null;
    }

    public String getParamById(String paramId) {
        return getDriver().findElement(By.xpath("//tr[@id='"+paramId+"']/td")).getText();
    }

    public String getDate(){
        return getDriver().findElement(By.id("adsms_Date")).getText();
    }


    public String getPaymentTitle() {
        return getDriver().findElement(By.tagName("h1")).getText().trim();
    }

    public boolean isRepeatLinkDisplayed(){
        new WebDriverWait(getDriver(), 30000).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@id='repeat_payment'][contains(string(), 'Повторить платеж')]")));
        return getDriver().findElement(By.xpath("//a[@id='repeat_payment'][contains(string(), 'Повторить платеж')]")).isDisplayed();
    }

    public boolean isSendToEmailLinkDisplayed(){
        return getDriver().findElement(By.xpath("//a[contains(string(), 'на e-mail')]")).isDisplayed();
    }

    public WebElement getPrintLink(){
        return getDriver().findElement(By.xpath("//a[contains(string(), 'Распечатать')]"));
    }

    public boolean isPrintLinkDisplayed(){
        return getPrintLink().isDisplayed();
    }

    public String getPaymentFormedBy(){
        return getDriver().findElement(By.xpath("//th[contains(string(), 'Платеж сформирован:')]/../td")).getText().trim();
    }

    public String getPaymentSource(){
        return getDriver().findElement(By.xpath("//*[@id='paymentData']//tr[./th[string()='Источник:']]/td")).getText();
    }

    public String getPaidService(){
        return getDriver().findElement(By.xpath("//th[contains(string(), 'Оплата услуг:')]/../td")).getText().trim();
    }

    public String getPaymentRecipient(){
        return getDriver().findElement(By.xpath("//th[contains(string(), 'Получатель:')]/../td")).getText().trim();
    }

    public String getAmountWithCommission(){
        return getDriver().findElement(By.xpath("//td[@id='adsms_fullAmount']")).getText().trim();
    }

    public String getTrxCode(){
        return getDriver().findElement(By.xpath("//td[@id='adsms_CustomerTrxId']")).getText().trim();
    }

    public String getBank(){
        return getDriver().findElement(By.xpath("//td[@id='adsms_AcquirerTitle']")).getText().trim();
    }

    public String getBankTrxNumber(){
        return getDriver().findElement(By.xpath("//td[@id='adsms_Rrn']")).getText().trim();
    }

    public WebElement getAddToFavoriteTitleInputField(){
        return getDriver().findElement(By.xpath("//input[@name='title']"));
    }

    public WebElement getSubmitAddToFavorites(){
        return getDriver().findElement(By.xpath("//button//span[string()='Сохранить']"));
    }
    public String getParamValueByName(String paramName){
        return getDriver().findElement(By.xpath("//th[contains(string(), '"+paramName+"')]/../td")).getText().trim();
    }
}
