package com.intervale.qa.mts.autotest.webportal.selenium.desktop;

import org.openqa.selenium.WebDriver;

/**
 * Created by dchahovsky.
 */
public abstract class CustomContentPage extends AbstractPage {

    public CustomContentPage(WebDriver driver) {
        super(driver);
    }

}
