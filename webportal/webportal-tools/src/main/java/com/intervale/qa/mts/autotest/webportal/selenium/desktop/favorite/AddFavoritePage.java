package com.intervale.qa.mts.autotest.webportal.selenium.desktop.favorite;

import com.intervale.qa.mts.autotest.webportal.selenium.desktop.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by ashaporev
 */
public class AddFavoritePage extends AbstractPage {
    public AddFavoritePage(WebDriver driver) {
        super(driver);
    }

    public void submit(){
        getDriver().findElement(By.xpath("//button[*//text()='Сохранить']")).click();
    }

    public void cancel(){
        getDriver().findElement(By.xpath("//button[*//text()='Отмена']")).click();
    }

    public WebElement getInputFieldById(String id){
        return getDriver().findElement(By.id(id));
    }
}
