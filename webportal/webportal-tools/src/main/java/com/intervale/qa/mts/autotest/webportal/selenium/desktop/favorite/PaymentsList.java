package com.intervale.qa.mts.autotest.webportal.selenium.desktop.favorite;

import com.intervale.qa.mts.autotest.webportal.selenium.SeleniumPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
* Created by ashaporev
*/
public class PaymentsList extends SeleniumPageObject {

    public PaymentsList(WebDriver driver, SearchContext searchContext) {
        super(driver, searchContext);
    }

    public void selectByTree(List<String> treePath) {
        WebElement ul;
        WebElement li = null;
        for (String cat : treePath) {
            ul = (null == li) ? getDriver().findElement(By.className("pay-full-list")) : li.findElement(By.xpath("./ul"));
            // На случай, если в конце прохода есть платеж и кат с одинаковым именем
            li = cat.equals(treePath.get(treePath.size()-1)) ? ul.findElement(By.xpath("./li[./a/text()='"+cat+"']")) :
                    ul.findElement(By.xpath("./li[.//a/text()='"+cat+"']"));
            li.findElement(By.xpath(".//a")).click();
        }
    }

}
