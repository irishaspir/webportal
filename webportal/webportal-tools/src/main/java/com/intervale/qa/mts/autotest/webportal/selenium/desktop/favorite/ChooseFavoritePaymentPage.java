package com.intervale.qa.mts.autotest.webportal.selenium.desktop.favorite;

import com.intervale.qa.mts.autotest.webportal.selenium.desktop.WhiteboardPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by ashaporev
 */
public class ChooseFavoritePaymentPage extends WhiteboardPage {

    private PaymentsList paymentsList;

    public ChooseFavoritePaymentPage(WebDriver driver) {
        super(driver);
        paymentsList = new PaymentsList(driver, driver.findElement(By.className("pay-full-list")));
    }

    public PaymentsList getPaymentsList(){
        return paymentsList;
    }

    // //*[@id='inner']/tbody/tr[2]/td[2]/div/div/div/div/ul/li[1]/ul/li[1]/ul/li[4]/a

}
