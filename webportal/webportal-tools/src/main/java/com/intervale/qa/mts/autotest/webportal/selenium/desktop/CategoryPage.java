package com.intervale.qa.mts.autotest.webportal.selenium.desktop;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static java.lang.Thread.sleep;

/**
 * Created by dchahovsky.
 */
public class CategoryPage extends WhiteboardPage {

    @FindBy(className = "small-cell-word-wrap")
    List<WebElement> subMenuItems;

    public CategoryPage(WebDriver driver) {
        super(driver);
    }

    public List<String> listSubmenuTitles() {
        throw new RuntimeException("todo");
    }

    public void openSubMenuItem(String subMenuItemTitle) {
        throw new RuntimeException("todo");
    }

    public void searchInThisCategory(String searchQuery) {
     //   getDriver().findElement(By.xpath("//span[input[@type='radio' and @id='current-section']]/a")).click();
        getDriver().findElement(By.xpath("//input[@type='text' and @id='searchText']")).click();
        getDriver().findElement(By.xpath("//input[@type='text' and @id='searchText']")).clear();
        getDriver().findElement(By.xpath("//input[@type='text' and @id='searchText']")).sendKeys(searchQuery);
        try {
            sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void searchInAllCategories(String searchQuery) {
      //  getDriver().findElement(By.xpath("//span[input[@type='radio' and @id='all-section']]/a")).click();
        getDriver().findElement(By.xpath("//input[@type='text' and @id='searchText']")).click();
        getDriver().findElement(By.xpath("//input[@type='text' and @id='searchText']")).clear();
        getDriver().findElement(By.xpath("//input[@type='text' and @id='searchText']")).sendKeys(searchQuery);
    }

    public void changeRegion(String newRegionTitle) {
        throw new RuntimeException("todo");
    }

    public String getSelectedRegion() {
        throw new RuntimeException("todo");
    }

}
