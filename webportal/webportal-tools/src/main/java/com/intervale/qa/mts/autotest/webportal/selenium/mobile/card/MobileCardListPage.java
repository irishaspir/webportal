package com.intervale.qa.mts.autotest.webportal.selenium.mobile.card;

import com.intervale.qa.mts.autotest.webportal.selenium.SeleniumPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * Created by dchahovsky.
 */
public class MobileCardListPage extends SeleniumPageObject {


    @FindBy(id = "addcard")
    private WebElement addCardButtonElement;


    public MobileCardListPage(WebDriver driver) {
        super(driver);
    }



    public boolean hasCard(String cardName) {
        List<WebElement> list = getDriver().findElements(By.xpath("//div[@class='card-name']"));
        for (WebElement we : list) {
            if (cardName.equals(we.getText().trim())) {
                return true;
            }
        }
        return false;
    }

    public String getCardStateName(String cardName) {
        List<WebElement> list = getDriver().findElement(By.className("h-pt-wrap")).findElements(By.tagName("div"));
        for (int i = 0; i < list.size(); i++) {
            if ("card-name".equals(list.get(i).getAttribute("class"))) {
                return list.get(i+2).getText().trim();
            }
        }
        throw new RuntimeException("Карта не найдена: " + cardName);
    }

    public void clickOnCardState(String cardName) {
        List<WebElement> list = getDriver().findElement(By.className("h-pt-wrap")).findElements(By.tagName("div"));
        for (int i = 0; i < list.size(); i++) {
            if ("card-name".equals(list.get(i).getAttribute("class"))) {
                list.get(i+2).findElement(By.tagName("a")).click();
                return;
            }
        }
        throw new RuntimeException("Карта не найдена: " + cardName);
    }

    public void addNewCard() {
        addCardButtonElement.click();
    }

}
