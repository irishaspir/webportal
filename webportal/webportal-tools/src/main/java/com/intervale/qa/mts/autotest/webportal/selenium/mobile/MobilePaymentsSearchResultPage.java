package com.intervale.qa.mts.autotest.webportal.selenium.mobile;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Thread.sleep;

/**
 * Created by iosdeveloper on 22/06/16.
 */
public class MobilePaymentsSearchResultPage extends MobileWhiteboardPage {
    public MobilePaymentsSearchResultPage(WebDriver driver) {
        super(driver);
    }

    public List<String> getListPaymentsTitles(){
        List<String> paymentsTitles = new ArrayList<>();
        for(WebElement e :getDriver().findElements(By.xpath("//div[@id='result-search']/div[@class='static-lists']//a"))){
            paymentsTitles.add(e.getText().trim());
        }
        return paymentsTitles;
    }

    public String getAnswerMessage(){
        try {
            sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return getDriver().findElement(By.xpath("//div[@id='result-search']")).getText();
    }

}
