package com.intervale.qa.mts.autotest.webportal.selenium.desktop;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by dchahovsky.
 */
public abstract class WhiteboardPage extends AbstractPage {


    @FindBy(id = "result-search")
    private WebElement pageResultSearch;

    @FindBy(id = "mainTitle")
    private WebElement pageTitle;

    @FindBy(className = "subhead")
    private WebElement menuDescription;

    @FindBy(className = "mobile")
    private WebElement mobileVersionLink;

    public WhiteboardPage(WebDriver driver) {
        super(driver);
    }

    public String getPageTitle() {
        return pageTitle.getText();
    }

    public String getMenuDescription() {
        return menuDescription.getText();
    }

    public WebElement getMobileVersionLink() {
        return mobileVersionLink;
    }

    public WebElement getPageResultSearch() {
        return pageResultSearch;
    }
}
