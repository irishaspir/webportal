package com.intervale.qa.mts.autotest.webportal.selenium.desktop;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by ashaporev
 */
public class LoginPage extends AbstractPage {

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getPhoneInputField(){
        return getDriver().findElement(By.name("IDToken1"));
    }

    public WebElement getPasswordInputField(){
        return getDriver().findElement(By.name("IDToken2"));
    }

    public void submit(){
        getDriver().findElement(By.className("btn red")).click();
    }
}
