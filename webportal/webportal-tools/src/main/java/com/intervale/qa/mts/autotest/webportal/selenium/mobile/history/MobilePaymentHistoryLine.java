package com.intervale.qa.mts.autotest.webportal.selenium.mobile.history;

import com.intervale.qa.mts.autotest.webportal.selenium.ElementFinder;
import com.intervale.qa.mts.autotest.webportal.selenium.SeleniumPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by iosdeveloper on 18/05/16.
 */
public class MobilePaymentHistoryLine extends SeleniumPageObject{

    private String paymentPcid;

    public MobilePaymentHistoryLine(WebDriver driver, SearchContext context) {
        super(driver, context);
    }

    public String getDateTime() {
        return getContext().findElement(By.xpath("./td[1]")).getText();
    }

    public String getPaymentSource(){
        return getContext().findElement(By.xpath("./td[2]//img")).getAttribute("title").trim();
    }

    public String getPaymentDescription() {
        return getContext().findElement(By.xpath("./td[2]//a[contains(@href,'/webportal/payments/')]")).getText().trim();
    }

    public String getAmount() {
        return getContext().findElement(By.xpath("./td[3]")).getText();
    }

    public String getResult() {
        return getContext().findElement(By.xpath("./td[4]/span")).getText().trim();
    }

    public WebElement getResultElement() {
        return getContext().findElement(By.xpath("./td[4]/span"));
    }

    public String getPaymentPcid(){
        return paymentPcid;
    }


    public void setPaymentPcid(String paymentPcid) {
        this.paymentPcid = paymentPcid;
    }
}
