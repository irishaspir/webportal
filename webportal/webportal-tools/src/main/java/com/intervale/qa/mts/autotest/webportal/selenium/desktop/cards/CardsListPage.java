package com.intervale.qa.mts.autotest.webportal.selenium.desktop.cards;

import com.intervale.qa.mts.autotest.webportal.selenium.ElementFinder;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.WhiteboardPage;
import org.hamcrest.MatcherAssert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static ru.yandex.qatools.matchers.webdriver.DisplayedMatcher.displayed;
import static ru.yandex.qatools.matchers.webdriver.ExistsMatcher.exists;

/**
 * Created by ashaporev
 */
public class CardsListPage extends WhiteboardPage {

    private List<CardLine> cardList = new ArrayList<>();

    public CardsListPage(WebDriver driver) {
        super(driver);
        assertThat("Не та страница?", getPageTitle(), equalTo("МОИ КАРТЫ"));
        for (WebElement e : getDriver().findElements(By.xpath("//table[@class='pay-table']/tbody/tr[position()>1]"))) {
            cardList.add(new CardLine(getDriver(), e));
        }
    }

    public List<CardLine> getCardList() {
        return cardList;
    }

    public CardLine getCardLineByCardName(String cardName) {
        for (CardLine line : cardList) {
            if (cardName.equals(line.getCardName())) {
                return line;
            }
        }
        throw new IllegalStateException("Карта не найдена: " + cardName);
    }

    public WebElement getAddCardButton(){
        return getDriver().findElement(By.id("addcard"));
    }

    public AddCardPage addCard() {
        getDriver().findElement(By.id("addcard")).click();
        return new AddCardPage(getDriver());
    }

    public void confirmPopupForm() {
        MatcherAssert.assertThat("Popup form should exists on page", ElementFinder.searchElement(getDriver(), By.id("popup_form")), exists());
        new WebDriverWait(getDriver(), 5)
                .withMessage("Element #popup_form is not visible")
                .until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("popup_form")));
        MatcherAssert.assertThat("Popup form should be displayed", ElementFinder.searchElement(getDriver(), By.id("popup_form")), displayed());
        getDriver().findElement(By.id("popup_form")).findElement(By.xpath(".//button[contains(concat(' ', @class, ' '), ' actnbtn ')]")).click();
    }

    public WebElement getVirtualCardLink(){
        return getDriver().findElement(By.xpath("//a[@id='virtualcardhref' and contains(text(), 'Виртуальная карта МТС Деньги')]"));
    }
}
