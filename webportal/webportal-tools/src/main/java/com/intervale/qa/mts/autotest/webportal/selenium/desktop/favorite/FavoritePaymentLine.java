package com.intervale.qa.mts.autotest.webportal.selenium.desktop.favorite;

import com.intervale.qa.mts.autotest.webportal.selenium.SeleniumPageObject;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by ashaporev
 */
public class FavoritePaymentLine extends SeleniumPageObject {

    @FindBy(xpath = ".//td[1]/a")
    WebElement descriptionLink;

    @FindBy(xpath = ".//a[text()='Удалить']")
    WebElement deleteLink;

    @FindBy(xpath = ".//a[text()='Изменить']")
    WebElement editLink;

    public FavoritePaymentLine(WebDriver driver, SearchContext context){
        super(driver, context);
    }

    public WebElement getDescriptionLink() {
        return descriptionLink;
    }

    public WebElement getDeleteLink() {
        return deleteLink;
    }

    public WebElement getEditLink() {
        return editLink;
    }

    public String getTitle() {
        return descriptionLink.getText();
    }
}
