package com.intervale.qa.mts.autotest.webportal.selenium.desktop;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * Created by ashaporev
 */
public class PaymentSubMenuPage extends WhiteboardPage {

    @FindBy(className = "small-cell-word-wrap")
    private List<WebElement> menuItems;

    public PaymentSubMenuPage(WebDriver driver) {
        super(driver);
    }

    /**
     * Opens payment or directory located on sub menu page
     * @param menuItemTitle
     */
    public void openMenuItem(String menuItemTitle) {
        for (WebElement we : menuItems) {
            if (we.getText().trim().equals(menuItemTitle)) {
                we.click();
                return;
            }
        }
        throw new RuntimeException("Menu item '" + menuItemTitle + "' was not found on page");
    }
}
