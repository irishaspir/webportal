package com.intervale.qa.mts.autotest.webportal.selenium.jsmsc;

import java.util.Date;

/**
 * Created by dchahovsky.
 */
public class Sms {

    private Date ts;
    private String sourceAddr;
    private String destinationAddr;
    private String bodyText;

    public Sms(Date ts, String sourceAddr, String destinationAddr, String bodyText) {
        this.ts = ts;
        this.sourceAddr = sourceAddr;
        this.destinationAddr = destinationAddr;
        this.bodyText = bodyText;
    }

    public Date getTs() {
        return ts;
    }

    public String getSourceAddr() {
        return sourceAddr;
    }

    public String getDestinationAddr() {
        return destinationAddr;
    }

    public String getBodyText() {
        return bodyText;
    }

    public String toString() {
        return "{'" + sourceAddr + "', '" + destinationAddr + "', '" + bodyText + "'}";
    }

}
