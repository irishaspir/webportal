package com.intervale.qa.mts.autotest.webportal.selenium.desktop;

import com.intervale.qa.mts.autotest.webportal.selenium.jsmsc.JSMSCHelper;
import com.intervale.qa.mts.autotest.webportal.selenium.jsmsc.Sms;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Thread.sleep;

/**
 * Created by ispiridonova on 20.10.2015.
 */
public class AutoPaymentPage extends WhiteboardPage {

    private final String OPEN_PAGE = "//a[contains(@href,'/webportal/autopay')]";
    private final String LINK_ADD_AUTO_PAYMENT = "//a[contains(@href,'/webportal/autopay/select')]";
    private final String SELECT_CATEGORY = "//span[contains(@class,'lev2-bul')]//a[contains(text(),'autotest')]";
    private final String SELECT_AUTO_PAYMENT = "//a[contains(text(),'Автовеб - 1')]";
    private final String SELECT_TYPE_AUTO_PAYMENT = "//td[@id = 'TDtypeAutopay']//div[not(contains(@style,'none')) and contains(@class,'jqTransformSelectWrapper')]//a[contains(@class,'jqTransformSelectOpen')]";
    private final String BALANCE = "//div[@id='jqtransform-big-elems']//a[@index='1']";
    private final String PHONE = "9164791212";
    private final String PHONE_CLIENT = "79859900700"; //public static final String DEFAULT_PROFILE = "9859900700";
    private final String VALUE_AMOUNT = "10";
    private final String VALUE_BALANCE = "1000";
    private final String VALUE_LIMIT = "500";
    private final String SMS_GET_CHECKBOX = "//label[@id = 'labelPaymentAcceptRequired']//a[contains(@class,'jqTransformCheckbox')]";
    private final String SMSC_BASE_URL = "http://mtspay-test-gomel.intervale.ru/jsmsc";
    private final String SET_PASSWORD = "//a[contains(@href,'setPassword')]";
    private final String STATUS_AUTO_PAYMENT = "//table[@id='mainTable']//tr[not(contains(@class,'noActiveAutopayList'))]//span[contains(text(),'%s')]/../../..//span[@class='stateTitle']";
    private final By PARAM_PAYMENT = By.id("param1");
    private final By AMOUNT = By.id("amount");
    private final By BTN_OK = By.id("btnOK");
    private final By NAME_AUTO_PAYMENT = By.id("nameAutopay");
    private final By BALANCE_THRESHOLD = By.id("balanceThreshold");
    private final By MONTHLY_PAYMENTS_LIMIT = By.id("monthlyPaymentsLimit");
    private final By BEGIN_DATE = By.id("beginDateTrigger");
    private final By SELECT_BEGIN_DATE = By.xpath("//td[contains(@class,'today')]");
    private final By SELECT_END_DATE = By.xpath("//td[contains(@class,'over')]");
    private final By END_DATE = By.id("endDateTrigger");
    private final By TITLE = By.id("mainTitle");
    private final By SMS_CODE = By.id("codeSMS");

    private String EXIST_AUTO_PAYMENT_PATTERN = "//table[@id='mainTable']//tr[not(contains(@class,'noActiveAutopayList'))]//span[contains(text(),'%s')]";
    private String NAME = "autopayment" + RandomStringUtils.randomNumeric(2);


    private JSMSCHelper sms = new JSMSCHelper(SMSC_BASE_URL);


    public AutoPaymentPage(WebDriver driver) {
        super(driver);
    }

    public void openPage() {
        searchElement(OPEN_PAGE).click();
    }

    public void clickLinkAddAutoPayment() {
        searchElement(LINK_ADD_AUTO_PAYMENT).click();
    }

    public void selectCategory() {
        searchElement(SELECT_CATEGORY).click();
    }

    public void selectAutoPayment() {
        searchElement(SELECT_AUTO_PAYMENT).click();
    }

    public void inputValuesAutoPayment() {
        searchElement(PARAM_PAYMENT).sendKeys(PHONE);
        searchElement(AMOUNT).sendKeys(VALUE_AMOUNT);
        searchElement(BTN_OK).click();

    }

    public String inputNameAutoPaymentAndSelectType() {
        searchElement(NAME_AUTO_PAYMENT).sendKeys(NAME);
        searchElement(SELECT_TYPE_AUTO_PAYMENT).click();
        searchElement(BALANCE).click();
        searchElement(BALANCE_THRESHOLD).sendKeys(VALUE_BALANCE);
        searchElement(MONTHLY_PAYMENTS_LIMIT).sendKeys(VALUE_LIMIT);
        searchElement(BEGIN_DATE).click();
        searchElement(SELECT_BEGIN_DATE).click();
        searchElement(END_DATE).click();
        searchElement(SELECT_END_DATE).click();
        searchElement(SMS_GET_CHECKBOX).click();
        searchElement(BTN_OK).click();
        searchElement(TITLE);
        searchElement(String.format(EXIST_AUTO_PAYMENT_PATTERN, NAME));
        return NAME;
    }

    public String takeNameAutoPaymentFromPage(){
        return searchElement(String.format(EXIST_AUTO_PAYMENT_PATTERN, NAME)).getText().trim();
    }

    public String getCode() {
        String code = null;
        List<Sms> textSMS = sms.waitForSmsForMsisdn(PHONE_CLIENT, 1);
        for (int i = 0; i < textSMS.size(); i++) {
            System.out.println(textSMS.get(i).getBodyText());
            Pattern patternSMS = Pattern.compile("(Код для подключения автоплатежа autopayment\\d{2}: )(\\d{4,})(. Пожалуйста, никому не передавайте код в целях безопасности.)");
            Matcher m = patternSMS.matcher(textSMS.get(i).getBodyText());
            if (m.matches()) {
                code = m.group(2);
            }
        }
        return code;
    }

    public String confirmAutoPayment() {
        searchElement(SET_PASSWORD).click();
        searchElement(SMS_CODE).sendKeys(getCode());
        searchElement(BTN_OK).click();
        searchElement(TITLE);
        searchElement(String.format(EXIST_AUTO_PAYMENT_PATTERN, NAME));
       return searchElement(String.format(STATUS_AUTO_PAYMENT,NAME)).getText();

    }

}
