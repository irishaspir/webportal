package com.intervale.qa.mts.autotest.webportal.selenium.mobile;

import com.intervale.qa.mts.autotest.webportal.selenium.SeleniumPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Created by ashaporev
 */
public class MobilePaymentsPage extends SeleniumPageObject {

    @FindBy(xpath = "//ul[@class='main']//li/a")
    List<WebElement> rootCategories;

    public MobilePaymentsPage(WebDriver driver) {
        super(driver);
    }

    public MobilePaymentPage openPaymentByName(String rootCategoryTitle, String paymentTitle) {
        WebElement rootCat = getCategoryLinkByName(rootCategoryTitle);
        assertThat("Категория не найдена на главной странице: " + rootCategoryTitle, rootCat, notNullValue());
        WebElement ul = rootCat.findElement(By.xpath("parent::li/ul"));
        List<WebElement> paymentLinks = ul.findElements(By.linkText(paymentTitle));
        assertThat("Платеж '"+paymentTitle+"' не найден в категории '"+rootCategoryTitle+"' на главной странице",
                paymentLinks, hasSize(1));
        paymentLinks.get(0).click();
        return new MobilePaymentPage(getDriver());
    }

    public List<String> getFirstLevelLinkTexts() {
        List<String> list = new ArrayList<>();
        for (WebElement e : getDriver().findElements(By.xpath("//ul[@class='main']/li/a"))) {
            list.add(e.getText());
        }
        return list;
    }

    public List<WebElement> getFirstLevelLinks() {
        List<WebElement> list = new ArrayList<>();
        for (WebElement e : getDriver().findElements(By.xpath("//ul[@class='main']/li/a"))) {
            list.add(e);
        }
        return list;
    }

    public List<String> getSecondLevelLinkTexts(String firstLevelLinkText) {
        List<String> list = new ArrayList<>();
        for (WebElement e : getDriver().findElements(By.xpath("//ul[@class='main']/li[normalize-space(a/text())='" + firstLevelLinkText + "']/ul/li/a"))) {
            list.add(e.getText());
        }
        return list;
    }

    public WebElement getCategoryLinkByName(String linkName) {
        for (WebElement e : rootCategories){
            if(e.getText().trim().equals(linkName)){
                return e;
            }
        }
        return null;
    }

    public boolean openCategoryLinkByName(String linkName) {
        for (WebElement e : rootCategories){
            if(e.getText().trim().equals(linkName)){
                e.click();
                return true;
            }
        }
        return false;
    }
}
