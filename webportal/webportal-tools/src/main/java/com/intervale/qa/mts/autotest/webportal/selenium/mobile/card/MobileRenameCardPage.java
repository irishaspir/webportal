package com.intervale.qa.mts.autotest.webportal.selenium.mobile.card;

import com.intervale.qa.mts.autotest.webportal.selenium.mobile.MobileWhiteboardPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by iosdeveloper on 20/04/16.
 */
public class MobileRenameCardPage extends MobileWhiteboardPage {

    @FindBy(id = "new_alias")
    private WebElement newCardAliasElement;

    @FindBy(xpath = "//*[@type='submit']")
    private WebElement saveButtonElement;

    public MobileRenameCardPage(WebDriver driver) {
        super(driver);
    }

    public String getCardAlias() {
        return newCardAliasElement.getAttribute("value");
    }

    public void fillNewAlias(String alias) {
        newCardAliasElement.click();
        newCardAliasElement.clear();
        newCardAliasElement.sendKeys(alias);
    }

    public MobileCardsListPage save() {
        saveButtonElement.click();
        return new MobileCardsListPage(getDriver());
    }
}
