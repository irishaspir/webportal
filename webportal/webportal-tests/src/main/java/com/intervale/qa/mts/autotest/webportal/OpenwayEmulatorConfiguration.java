package com.intervale.qa.mts.autotest.webportal;

import com.intervale.qa.toolbox.http.Browser;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: asapanovich
 * Date: 11/13/14
 * Time: 12:05 PM
 */
public class OpenwayEmulatorConfiguration {

    private enum BankMsgType {
        CREDIT,
        CANCEL_CREDIT,
        CREDIT_REVERSAL,

        PURCHASE,
        PURCHASE_REVERSAL,
        PURCHASE_COMM_REVERSAL,

        AUTH,
        AUTH_CONFIRMATION,
        REVERSAL,
    }

    private String urlBemulator;
    private Browser browser;

    public OpenwayEmulatorConfiguration(String emulatorUrl) {
        this.urlBemulator = emulatorUrl;
        browser = new Browser(false);
    }

    public void addCreditRC(String responseCode) {
        addResponse(BankMsgType.CREDIT, responseCode, 0);
    }

    public void addCreditRC(String responseCode, int delaySeconds) {
        addResponse(BankMsgType.CREDIT, responseCode, delaySeconds);
    }

    public void addCancelCreditRC(String rc) {
        addResponse(BankMsgType.CANCEL_CREDIT, rc, 0);
    }

    public void addCancelCreditRC(String rc, int delaySeconds) {
        addResponse(BankMsgType.CANCEL_CREDIT, rc, delaySeconds);
    }

    public void addCreditReversalRC(String rc) {
        addResponse(BankMsgType.CREDIT_REVERSAL, rc, 0);
    }

    public void addCreditReversalRC(String rc, int delaySeconds) {
        addResponse(BankMsgType.CREDIT_REVERSAL, rc, delaySeconds);
    }

    public void addPurchaseRC(String rc) {
        addResponse(BankMsgType.PURCHASE, rc, 0);
    }

    public void addPurchaseRC(String rc, int delaySeconds) {
        addResponse(BankMsgType.PURCHASE, rc, delaySeconds);
    }

    public void addPurchaseReversalRC(String rc) {
        addResponse(BankMsgType.PURCHASE_REVERSAL, rc, 0);
    }

    public void addPurchaseReversalRC(String rc, int delaySeconds) {
        addResponse(BankMsgType.PURCHASE_REVERSAL, rc, delaySeconds);
    }

    public void addPurchaseCommReversalRC(String rc) {
        addResponse(BankMsgType.PURCHASE_COMM_REVERSAL, rc, 0);
    }

    public void addPurchaseCommReversalRC(String rc, int delaySeconds) {
        addResponse(BankMsgType.PURCHASE_COMM_REVERSAL, rc, delaySeconds);
    }

    public void addAuthRC(String rc) {
        addResponse(BankMsgType.AUTH, rc, 0);
    }

    public void addAuthRC(String rc, int delaySeconds) {
        addResponse(BankMsgType.AUTH, rc, delaySeconds);
    }

    public void addAuthConfRC(String rc) {
        addResponse(BankMsgType.AUTH_CONFIRMATION, rc, 0);
    }

    public void addAuthConfRC(String rc, int delaySeconds) {
        addResponse(BankMsgType.AUTH_CONFIRMATION, rc, delaySeconds);
    }

    public void addAuthReversalRC(String rc) {
        addResponse(BankMsgType.REVERSAL, rc, 0);
    }

    public void addAuthReversalRC(String rc, int delaySeconds) {
        addResponse(BankMsgType.REVERSAL, rc, delaySeconds);
    }

    /* --- */

    public void addCreditError() {
        addErrorResponse(BankMsgType.CREDIT);
    }

    public void addCancelCreditError() {
        addErrorResponse(BankMsgType.CANCEL_CREDIT);
    }

    public void addCreditReversalError() {
        addErrorResponse(BankMsgType.CREDIT_REVERSAL);
    }

    public void addPurchaseError() {
        addErrorResponse(BankMsgType.PURCHASE);
    }

    public void addPurchaseReversalError() {
        addErrorResponse(BankMsgType.PURCHASE_REVERSAL);
    }

    public void addPurchaseCommReversalError() {
        addErrorResponse(BankMsgType.PURCHASE_COMM_REVERSAL);
    }

    public void addAuthError() {
        addErrorResponse(BankMsgType.AUTH);
    }

    public void addAuthConfError() {
        addErrorResponse(BankMsgType.AUTH_CONFIRMATION);
    }

    public void addAuthReversalError() {
        addErrorResponse(BankMsgType.REVERSAL);
    }



    /* --- */

    public void clearConfiguration(BankMsgType msgType) {
        browser.simpleGetRequest(urlBemulator + "setup?action=clear&requesttype=" + msgType.name());
    }

    public void clearAllConfiguration() {
        browser.simpleGetRequest(urlBemulator + "setup?action=clear-all");
    }

    private void addResponse(BankMsgType msgType, String responseCode, int delaySeconds) {
        String url = urlBemulator + "setup?action=append" +
                "&requesttype=" + msgType.name() +
                "&responsetype=SUCCESS" +
                "&responsecode=" + responseCode +
                "&timeout=" + Integer.toString(delaySeconds);
        HttpResponse httpResponse = browser.getRequest(url);
        consumeEntity(httpResponse.getEntity());
        if (302 != httpResponse.getStatusLine().getStatusCode()) {
            throw new IllegalStateException("Illegal bank emulator status code: " + httpResponse.getStatusLine().getStatusCode());
        }
    }

    private void addErrorResponse(BankMsgType msgType) {
        String url = urlBemulator + "setup?action=append&requesttype="+msgType.name()+"&responsetype=ERROR";
        HttpResponse httpResponse = browser.getRequest(url);
        consumeEntity(httpResponse.getEntity());
        if (302 != httpResponse.getStatusLine().getStatusCode()) {
            throw new IllegalStateException("Illegal bank emulator status code: " + httpResponse.getStatusLine().getStatusCode());
        }
    }

    private String consumeEntity(HttpEntity entity) {
        try {
            if (entity != null) {
                try {
                    return IOUtils.toString(entity.getContent(), "UTF-8");
                }
                finally {
                    // ensure the connection gets released to the manager
                    EntityUtils.consume(entity);
                }
            }
            else {
                throw new RuntimeException("Null response");
            }
        }
        catch (IOException io) {
            throw new RuntimeException("Http request failure: " + io.getMessage(), io);
        }
    }

}
