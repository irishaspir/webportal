package com.intervale.qa.mts.autotest.webportal.matchers;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;

/**
 * Created by ashaporev
 */
public class DateTimeMatcher {

    public static class GreaterThanMatcher extends TypeSafeMatcher<DateTime>{
        private DateTime comparableDateTime;
        private boolean dateOnly = false;

        private GreaterThanMatcher(DateTime comparableDateTime){
            this.comparableDateTime = comparableDateTime;
        }

        @Override
        protected boolean matchesSafely(DateTime baseDateTime) {
            DateTimeComparator comparator;
            if (dateOnly){
                comparator = DateTimeComparator.getDateOnlyInstance();
            } else {
                comparator = DateTimeComparator.getInstance();
            }
            return comparator.compare(comparableDateTime, baseDateTime) <= 0;
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("Ожидалось, что дата будет больше чем ").appendValue(comparableDateTime);
        }

        public GreaterThanMatcher dateOnly(){
            dateOnly = true;
            return this;
        }
    }

    public static class LessThanMatcher extends TypeSafeMatcher<DateTime>{
        private DateTime comparableDateTime;
        private boolean dateOnly = false;

        private LessThanMatcher(DateTime comparableDateTime){
            this.comparableDateTime = comparableDateTime;
        }

        @Override
        protected boolean matchesSafely(DateTime baseDateTime) {
            DateTimeComparator comparator;
            if (dateOnly){
                comparator = DateTimeComparator.getDateOnlyInstance();
            } else {
                comparator = DateTimeComparator.getInstance();
            }
            return comparator.compare(comparableDateTime, baseDateTime) >= 0;
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("Ожидалось, что дата будет меньше чем ").appendValue(comparableDateTime);
        }

        public LessThanMatcher dateOnly(){
            dateOnly = true;
            return this;
        }
    }

    public static GreaterThanMatcher greaterThan(DateTime comparableDateTime){
        return new GreaterThanMatcher(comparableDateTime);
    }

    public static LessThanMatcher lessThen(DateTime comparableDateTime){
        return new LessThanMatcher(comparableDateTime);
    }
}
