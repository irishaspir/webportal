package com.intervale.qa.mts.autotest.webportal.matchers;

import com.intervale.qa.mts.autotest.webportal.selenium.mobile.favorite.MobileFavoritePaymentLine;
import com.intervale.qa.mts.autotest.webportal.selenium.mobile.favorite.MobileFavoritesPage;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

/**
 * Created by ashaporev
 */
public class MobileFavoritesPageMatcher {

    private static class ContainsFavPaymentMatcher extends TypeSafeMatcher<MobileFavoritesPage>{

        private String paymentName;

        private ContainsFavPaymentMatcher(String paymentName){
            this.paymentName = paymentName;
        }

        @Override
        protected boolean matchesSafely(MobileFavoritesPage mobileFavoritesPage) {
            return mobileFavoritesPage.containPaymentWithName(paymentName);
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("В списке избранных должен присутствовать платеж ").appendValue(paymentName);
        }

        @Override
        public void describeMismatchSafely(final MobileFavoritesPage mobileFavoritesPage, final Description description){
            description.appendText("Платеж ").appendValue(paymentName).appendText(" не найден среди платежей: ");
            for (MobileFavoritePaymentLine favoritePaymentLine : mobileFavoritesPage.getFavoritesList()){
                description.appendText("\n").appendValue(favoritePaymentLine.getName());
            }
        }
    }

    public static ContainsFavPaymentMatcher containsPayment(String paymentName){
        return new ContainsFavPaymentMatcher(paymentName);
    }
}
