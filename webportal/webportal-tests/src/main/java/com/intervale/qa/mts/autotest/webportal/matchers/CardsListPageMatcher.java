package com.intervale.qa.mts.autotest.webportal.matchers;

import com.intervale.qa.mts.autotest.webportal.selenium.desktop.cards.CardLine;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.cards.CardsListPage;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

import java.util.List;

/**
 * Created by ashaporev
 */
public class CardsListPageMatcher {

    public static class ContainsStateCardMather extends TypeSafeMatcher<CardsListPage>{
        private String cardName;

        private boolean cardAdded = false;
        private String expectedState;
        private String actualState = null;

        private ContainsStateCardMather(String cardName, String stateName){
            this.cardName = cardName;
            this.expectedState = stateName;
        }

        @Override
        protected boolean matchesSafely(CardsListPage cardsListPage) {
            List<CardLine> cardLineList = cardsListPage.getCardList();
            for (CardLine cardLine : cardLineList){
                if (cardLine.getCardName().equals(cardName)){
                    cardAdded = true;
                    if(expectedState.equals(cardLine.getStateText())){
                        return true;
                    } else {
                        // Сохраняем в классе, чтобы второй раз не искать (долго)
                        actualState = cardLine.getStateText();
                        return false;
                    }
                }
            }
            return false;
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("Должна присутствовать карта с именем: ").appendValue(cardName).appendText(" и статусом ").appendValue(expectedState);
        }

        @Override
        public void describeMismatchSafely(final CardsListPage cardsListPage, final Description description) {
            if (cardAdded){
                description.appendText("Карта ").appendValue(cardName).appendText(" найдена, но её статус ").appendValue(actualState);
            } else {
                description.appendText("Карта ").appendValue(cardName).appendText(" не найдена. Получили список карт:\n");
                for(CardLine cardLine : cardsListPage.getCardList()){
                    description.appendText("Название ").appendValue(cardLine.getCardName()).appendText(" Статус ").
                            appendValue(cardLine.getStateText()).appendText("\n");
                }
            }
        }
    }

    public static ContainsStateCardMather containsActiveCard(String cardName){
        return new ContainsStateCardMather(cardName, "Активна");
    }

    public static ContainsStateCardMather containsFailedCard(String cardName){
        return new ContainsStateCardMather(cardName, "Подтвердить не удалось");
    }

}
