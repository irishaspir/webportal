package com.intervale.qa.mts.autotest.webportal.matchers;

import com.intervale.qa.mts.autotest.webportal.selenium.mobile.card.MobileCardLine;
import com.intervale.qa.mts.autotest.webportal.selenium.mobile.card.MobileCardsListPage;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

import java.util.List;

/**
 * Created by iosdeveloper on 20/04/16.
 */
public class MobileCardsListPageMatcher {

    public static class MobileContainsStateCardMather extends TypeSafeMatcher<MobileCardsListPage> {
        private String cardName;

        private boolean cardAdded = false;
        private String expectedState;
        private String actualState = null;

        private MobileContainsStateCardMather(String cardName, String stateName) {
            this.cardName = cardName;
            this.expectedState = stateName;
        }

        @Override
        protected boolean matchesSafely(MobileCardsListPage mobileCardsListPage) {
            List<MobileCardLine> mobileCardLineList = mobileCardsListPage.getCardList();
            for (MobileCardLine cardLine : mobileCardLineList) {
                if (cardLine.getCardName().equals(cardName)) {
                    cardAdded = true;
                    if (expectedState.equals(cardLine.getStateText())) {
                        return true;
                    } else {
                        // Сохраняем в классе, чтобы второй раз не искать (долго)
                        actualState = cardLine.getStateText();
                        return false;
                    }
                }
            }
            return false;
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("Должна присутствовать карта с именем: ").appendValue(cardName).appendText(" и статусом ").appendValue(expectedState);
        }

        @Override
        public void describeMismatchSafely(final MobileCardsListPage mobileCardsListPage, final Description description) {
            if (cardAdded) {
                description.appendText("Карта ").appendValue(cardName).appendText(" найдена, но её статус ").appendValue(actualState);
            } else {
                description.appendText("Карта ").appendValue(cardName).appendText(" не найдена. Получили список карт:\n");
                for (MobileCardLine cardLine : mobileCardsListPage.getCardList()) {
                    description.appendText("Название ").appendValue(cardLine.getCardName()).appendText(" Статус ").
                            appendValue(cardLine.getStateText()).appendText("\n");
                }
            }
        }
    }

    public static MobileContainsStateCardMather containsActiveCardMobile(String cardName) {
        return new MobileContainsStateCardMather(cardName, "Активна");
    }

    public static MobileContainsStateCardMather containsFailedCardMobile(String cardName) {
        return new MobileContainsStateCardMather(cardName, "Подтвердить не удалось");
    }

}
