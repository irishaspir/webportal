package com.intervale.qa.mts.autotest.webportal.matchers;

import com.intervale.qa.mts.autotest.webportal.selenium.desktop.history.HistoryPage;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.history.PaymentHistoryLine;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

/**
 * Created by ashaporev
 */
public class HistoryPageMatcher {

    public static class ContainsHistoryPaymentMatcher extends TypeSafeMatcher<HistoryPage>{

        private String pcid;
        private String date;
        private String amount;
        private String paymentDescription;

        private boolean pageContainPayment = false;
        private boolean datesEquals = false;
        private String receivedDate;
        private boolean amountsEquals = false;
        private String receivedAmount;
        private boolean descriptionsEquals = false;
        private String receivedDescription;

        private ContainsHistoryPaymentMatcher(String pcid, String date, String amount, String paymentDescription){
            this.pcid = pcid;
            this.date = date;
            this.amount = amount;
            this.paymentDescription = paymentDescription;
        }

        @Override
        protected boolean matchesSafely(HistoryPage historyPage) {
            for (PaymentHistoryLine line : historyPage.getHistoryList()){
                if (line.getPaymentPcid().equals(pcid)){
                    pageContainPayment = true;
                    receivedDate = line.getDateTime();
                    receivedAmount = line.getAmount();
                    receivedDescription = line.getPaymentDescription();

                    datesEquals = date.equals(receivedDate);
                    amountsEquals = amount.equals(receivedAmount);
                    descriptionsEquals = paymentDescription.equals(receivedDescription);
                    return datesEquals && amountsEquals && descriptionsEquals;
                }
            }
            return false;
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("Должен присутствовать платеж с датой ").appendValue(date).appendText(", суммой ")
                    .appendValue(amount).appendText(" и описанием ").appendValue(paymentDescription)
                    .appendText(" (ID транзакции ").appendValue(pcid).appendText(").");
        }

        @Override
        public void describeMismatchSafely(final HistoryPage historyPage, final Description description) {
            if (!pageContainPayment){
                description.appendText("Платеж с ID ").appendValue(pcid).appendText(" не найден на странице истории. ");
            } else {
                if (!datesEquals){
                    description.appendText("Получили дату транзакции ").appendValue(receivedDate).appendText(". ");
                }
                if (!amountsEquals){
                    description.appendText("Получили сумму транзакции ").appendValue(receivedAmount).appendText(". ");
                }
                if (!descriptionsEquals){
                    description.appendText("Получили описание транзакции ").appendValue(receivedDescription).appendText(". ");
                }
            }
        }
    }

    public static ContainsHistoryPaymentMatcher containsHistoryPayment(String pcid, String date, String amount, String paymentDescription){
        return new ContainsHistoryPaymentMatcher(pcid, date, amount, paymentDescription);
    }
}
