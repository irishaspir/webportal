package com.intervale.qa.mts.autotest.webportal;

import com.intervale.qa.toolbox.properties.AbstractPropertyHelper;

import java.io.File;

/**
 * Created by dchahovsky.
 */
public class WebConfig extends AbstractPropertyHelper {

    public static final String BASE_URL = "webportal.base.url";
    public static final String PCENTRE_RPC_URL = "pcentre.cp.rpc.url";
    public static final String PCENTRE_RPC_USERNAME = "pcentre.cp.rpc.username";
    public static final String PCENTRE_RPC_PASSWORD = "pcentre.cp.rpc.password";
    public static final String SELENIUM_HUB_URL = "selenium.hub.url";
    private static final String DUMMY_PAGE_FOR_COOKIE_MANAGEMENT = "dummy.page.to.manege.cookies";

    private static final String configPropertyName = "webportal-tests-config";
    private static final String localWebportalPropertiesFileName = "etc/webportal-local.properties";

    static {
        String filename = System.getProperty(configPropertyName);
        if (filename == null && new File(localWebportalPropertiesFileName).exists()) {
            System.setProperty(configPropertyName, localWebportalPropertiesFileName);
        }
    }

    public WebConfig() {
        super(configPropertyName);
    }
    public String getBaseUrl(){
        String baseUrl = getProperty(BASE_URL);
        if (null != baseUrl) {
            return baseUrl;
        }
        else {
            throw new IllegalArgumentException("Webportal base url is not defined");
        }
    }

    public String getMainPageUrl() {
        return getBaseUrl() + "/webportal/payments";
    }

    public String getFavoritesUrl() {
        return getBaseUrl() + "/webportal/favorites";
    }

    public String getCardsListUrl() {
        return getBaseUrl() + "/webportal/cards/real/show";
    }

    public String getCardAddPageUrl() {
        return getBaseUrl() + "/webportal/cards/real/add";
    }

    public String getCardReservedAmountInputPage() {
        return getBaseUrl() + "/webportal/cards/real/register";
    }

    public String getHistoryUrl(){
        return getBaseUrl() + "/webportal/history";
    }

    public String getMobileMainPageUrl(){
        return getBaseUrl() + "/webportal/?view=mobile";
    }

    public String getMobilePaymentsPageUrl(){
        return getBaseUrl() + "/webportal/payments?view=mobile";
    }

    public String getDummyUrlForCookieManagement() {
        return getBaseUrl() + getProperty(DUMMY_PAGE_FOR_COOKIE_MANAGEMENT);
    }

    public String getPcentreCpRpcUrl(){
        String baseUrl = getProperty(PCENTRE_RPC_URL);
        if (null != baseUrl) {
            return baseUrl;
        } else {
            throw new IllegalArgumentException("PCentre-cp rpc url is not defined");
        }
    }

    public String getPcentreCpRpcUsername(){
        String baseUrl = getProperty(PCENTRE_RPC_USERNAME);
        if (null != baseUrl) {
            return baseUrl;
        } else {
            throw new IllegalArgumentException("PCentre-cp rpc username is not defined");
        }
    }

    public String getPcentreCpRpcPassword(){
        String baseUrl = getProperty(PCENTRE_RPC_PASSWORD);
        if (null != baseUrl) {
            return baseUrl;
        } else {
            throw new IllegalArgumentException("PCentre-cp rpc password is not defined");
        }
    }

    public String getPersonalCabinetDatabaseUri() {
        return getProperty("pcab.db.uri");
    }

    public String getPersonalCabinetDatabaseUsername() {
        return getProperty("pcab.db.username");
    }

    public String getPersonalCabinetDatabasePassword() {
        return getProperty("pcab.db.password");
    }

    public String getBankEmulatorBaseUrl() {
        return getProperty("emulator.openway.base.url");
    }

    public String getMobileFavoritesUrl() {
        return getBaseUrl() + "/webportal/favorites?view=mobile";
    }

    public String getMobileCardListUrl() {
        return getBaseUrl() + "/webportal/cards/real/show?view=mobile";
    }

    public String getMobileHistoryUrl() {
        return getBaseUrl() + "/webportal/history?view=mobile";
    }
}
