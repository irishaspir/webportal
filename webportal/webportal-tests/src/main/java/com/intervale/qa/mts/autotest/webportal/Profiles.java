package com.intervale.qa.mts.autotest.webportal;

/**
 * Created by ashaporev
 */
public class Profiles {

   // public static final String DEFAULT_PROFILE = "9154791212";
    public static final String DEFAULT_PROFILE = "9859900700";
    public static final String WITH_ALL_CARD_STATES = "9859900701";
    public static final String WITH_FAVORITES_FROM_VARIOUS_PORTALS = "9859900702";
    public static final String WITH_PAYMENTS_FROM_VARIOUS_PORTALS_WITH_VARIOUS_STATUSES = "9859900703";
    public static final String PARTIAL_RESTRICTION = "9859900704";
}
