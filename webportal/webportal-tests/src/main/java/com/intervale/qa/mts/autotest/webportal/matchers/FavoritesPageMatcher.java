package com.intervale.qa.mts.autotest.webportal.matchers;

import com.intervale.qa.mts.autotest.webportal.selenium.desktop.favorite.FavoritePaymentLine;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.favorite.FavoritesPage;
import com.intervale.qa.mts.autotest.webportal.selenium.mobile.favorite.MobileFavoritesPage;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

/**
 * Created by ashaporev
 */
public class FavoritesPageMatcher {

    public static class ContainsPaymentMatcher extends TypeSafeMatcher<FavoritesPage>{
        private String paymentName;

        private ContainsPaymentMatcher(String paymentName){
            this.paymentName = paymentName;
        }

        @Override
        protected boolean matchesSafely(FavoritesPage favoritesPage) {
            return favoritesPage.containsPayment(paymentName);
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("Должен присутствовать платеж ").appendValue(paymentName);
        }

        @Override
        public void describeMismatchSafely(final FavoritesPage favoritesPage, final Description description) {
            description.appendText("Платеж ").appendValue(paymentName).appendText(" не найден среди платежей: ");
            for (FavoritePaymentLine favoritePaymentLine : favoritesPage.getFavoritesList()){
                description.appendText("\n").appendValue(favoritePaymentLine.getTitle());
            }
        }
    }

    public static ContainsPaymentMatcher containsPayment(String paymentName){
        return new ContainsPaymentMatcher(paymentName);
    }
}
