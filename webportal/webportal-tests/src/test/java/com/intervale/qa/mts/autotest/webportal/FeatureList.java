package com.intervale.qa.mts.autotest.webportal;

/**
 * Created by dchahovsky.
 */
public abstract class FeatureList {

    public static final String PREPARE = "Подготовительные тесты";
    public static final String DEPRECATED = "Устаревшие тесты";

    public static final String DESKTOP_FEATURE = "Основная версия";
    public static final String MOBILE_FEATURE = "Мобильная версия";


}

