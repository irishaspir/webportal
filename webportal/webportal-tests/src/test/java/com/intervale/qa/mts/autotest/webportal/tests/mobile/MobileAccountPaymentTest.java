package com.intervale.qa.mts.autotest.webportal.tests.mobile;

import com.intervale.qa.mts.autotest.webportal.Profiles;
import com.intervale.qa.mts.autotest.webportal.selenium.mobile.MobilePaymentPage;
import com.intervale.qa.mts.autotest.webportal.selenium.mobile.MobilePaymentResultPage;
import com.intervale.qa.mts.autotest.webportal.selenium.mobile.MobilePaymentResultPrintPage;
import com.intervale.qa.mts.autotest.webportal.selenium.mobile.MobilePaymentsPage;
import com.intervale.qa.mts.autotest.webportal.selenium.mobile.favorite.MobileFavoritesPage;
import com.intervale.qa.mts.autotest.webportal.tests.AbstractSeleniumTest;
import org.junit.Ignore;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.intervale.qa.mts.autotest.webportal.FeatureList.*;
import static com.intervale.qa.mts.autotest.webportal.StoriesList.ACCOUNT_PAYMENT;
import static com.intervale.qa.mts.autotest.webportal.StoriesList.MANAGE_FAVOURITES;
import static com.intervale.qa.mts.autotest.webportal.matchers.MobileFavoritesPageMatcher.containsPayment;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * Created by dchahovsky.
 */
public class MobileAccountPaymentTest extends AbstractSeleniumTest {

    @Features(MOBILE_FEATURE)
    @Stories(ACCOUNT_PAYMENT)
    @Description("Ввод символов вместо суммы оплаты. Проверка подсветки поля суммы и наличия текста ошибки на странице.")
    @Test
    public void invalidParameterIsHighlighted() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobilePaymentsPageUrl());
        MobilePaymentsPage mobilePaymentsPage = new MobilePaymentsPage(getDriver());
        mobilePaymentsPage.openPaymentByName("autotest","Автовеб - 1");
        MobilePaymentPage mobilePaymentPage = new MobilePaymentPage(getDriver());
        mobilePaymentPage.getInputParamFieldByName("param1").sendKeys("9104451325");
        mobilePaymentPage.getInputParamFieldByName("amount").sendKeys("five");
        mobilePaymentPage.submit();
        assertThat("Поле ввода суммы должно быть подсвечено", mobilePaymentPage.isInputParamFieldHighlighted("amount"), is(true));
        String expectedErrorMessage = "Внимание! При заполнении формы были допущены ошибки. Проверьте, пожалуйста, значения параметров и повторите отправку данных.";
        assertThat("Должно отображаться сообщение об ошибке \""+expectedErrorMessage+"\"",
                mobilePaymentPage.isErrorMessagePresent(expectedErrorMessage), is(true));
    }

    @Features(MOBILE_FEATURE)
    @Stories(ACCOUNT_PAYMENT)
    @Test
    public void successfulResultPageTest() {
        /* проверить все поля формы результата платежа */
        /* дополнительно: проверить наличие ссылки "повторить платеж" */
        /* проверить наличие ссылки "отправить на электронный ящик" */
        /* проверить наличие ссылки "распечатать" */
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobilePaymentsPageUrl());
        MobilePaymentsPage mobilePaymentsPage = new MobilePaymentsPage(getDriver());
        MobilePaymentPage mobilePaymentPage = mobilePaymentsPage.openPaymentByName("autotest", "Автовеб - 1");
        mobilePaymentPage.getInputParamFieldByName("param1").click();
        mobilePaymentPage.getInputParamFieldByName("param1").sendKeys("9104451325");
        mobilePaymentPage.getInputParamFieldByName("amount").sendKeys("10");
        mobilePaymentPage.getAccountRadioButton().click();
        mobilePaymentPage.submit();
        assertThat("Платеж должен завершиться успешно", waitAndGetPaymentResult(), is("Выполнено"));
        MobilePaymentResultPage mobilePaymentResultPage = new MobilePaymentResultPage(getDriver());
        assertThat("Должна отображаться ссылка 'Повторить платеж'", mobilePaymentResultPage.isRepeatLinkDisplayed(), is(true));
        assertThat("Должна отображаться ссылка отправки на e-mail", mobilePaymentResultPage.isSendToEmailLinkDisplayed(), is(true));
        assertThat("Должна отображаться ссылка распечатки квитанции", mobilePaymentResultPage.isPrintLinkDisplayed(), is(true));
        assertThat("Неверно указано место формирования платежа", mobilePaymentResultPage.getPaymentFormedBy(), is("На сайте «Легкий платеж»"));
        assertThat("Неверно указан источник", mobilePaymentResultPage.getPaymentSource(), is("Лицевой счет МТС"));
        assertThat("Неверно указана оплачиваемая услуга", mobilePaymentResultPage.getPaidService(), is("Автовеб - 1"));
        assertThat("Неверно указан получатель", mobilePaymentResultPage.getPaymentRecipient(), is("ЗАО «Получатель платежа»"));
        assertThat("Неверно указана сумма платежа", mobilePaymentResultPage.getParamById("amount"), is("10 руб."));
        assertThat("Неверно указан номер телефона", mobilePaymentResultPage.getParamById("param1"), is("9104451325"));
        assertThat("Неверно указана сумма с комиссией", mobilePaymentResultPage.getAmountWithCommission(), is("10 руб."));
    }

    @Features(MOBILE_FEATURE)
    @Stories(ACCOUNT_PAYMENT)
    @Test
    public void failedResultPageTest() {
        /* проверить все поля на форме с неуспешным результатом платежа */
        /* дополнительно: проверить отсутствие ссылки "повторить платеж" */
        /* проверить наличие ссылки "отправить на электронный ящик" */
        /* проверить наличие ссылки "распечатать" */
        try {
            setActiveProfile(Profiles.DEFAULT_PROFILE);
            getDriver().get(CONFIG.getMobilePaymentsPageUrl());
            MobilePaymentsPage mobilePaymentsPage = new MobilePaymentsPage(getDriver());
            MobilePaymentPage mobilePaymentPage = mobilePaymentsPage.openPaymentByName("autotest", "Автовеб - 1");
            mobilePaymentPage.getInputParamFieldByName("param1").click();
            mobilePaymentPage.getInputParamFieldByName("param1").sendKeys("9104451325");
            mobilePaymentPage.getInputParamFieldByName("amount").sendKeys("10");
            mobilePaymentPage.getAccountRadioButton().click();
            BANK.addCreditRC("25"); // <<< configure emulator just before request
            mobilePaymentPage.submit();
            assertThat("Платеж не должен завершиться успешно", waitAndGetPaymentResult(), is("Не выполнено"));
            MobilePaymentResultPage paymentResultPage = new MobilePaymentResultPage(getDriver());
            assertThat("Должна отображаться ссылка 'Повторить платеж'", paymentResultPage.isRepeatLinkDisplayed(), is(true));
            assertThat("Должна отображаться ссылка отправки на e-mail", paymentResultPage.isSendToEmailLinkDisplayed(), is(true));
            assertThat("Должна отображаться ссылка распечатки квитанции", paymentResultPage.isPrintLinkDisplayed(), is(true));
            assertThat("Неверно указано место формирования платежа", paymentResultPage.getPaymentFormedBy(), is("На сайте «Легкий платеж»"));
            assertThat("Неверно указан источник", paymentResultPage.getPaymentSource(), is("Лицевой счет МТС"));
            assertThat("Неверно указана оплачиваемая услуга", paymentResultPage.getPaidService(), is("Автовеб - 1"));
            assertThat("Неверно указан получатель", paymentResultPage.getPaymentRecipient(), is("ЗАО «Получатель платежа»"));
            assertThat("Неверно указана сумма платежа", paymentResultPage.getParamById("amount"), is("10 руб."));
            assertThat("Неверно указан номер телефона", paymentResultPage.getParamById("param1"), is("9104451325"));
            assertThat("Неверно указана сумма с комиссией", paymentResultPage.getAmountWithCommission(), is("10 руб."));
        } finally {
            BANK.clearAllConfiguration();
        }
    }

    @Features(MOBILE_FEATURE)
    @Stories(ACCOUNT_PAYMENT)
    @Test
    public void customOfferUrlOnAccountPayment() {
        /* Проверить ссылку на договор-оферту при выборе платежа с л/с */
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobilePaymentsPageUrl());
        MobilePaymentsPage mobilePaymentsPage = new MobilePaymentsPage(getDriver());
        MobilePaymentPage mobilePaymentPage = mobilePaymentsPage.openPaymentByName("autotest", "Автовеб - 1");
        assertThat("Должна отображаться ссылка на договор-оферту", mobilePaymentPage.getOfferLink().isDisplayed(), is(true));
        String ref = mobilePaymentPage.getOfferLink().getAttribute("href");
        assertThat("Неверная ссылка на договор-оферту по карте", ref, startsWith(CONFIG.getBaseUrl()+"/webportal/offer-account"));
    }

    @Features(MOBILE_FEATURE)
    @Stories(ACCOUNT_PAYMENT)
    @Test
    public void testPrintResultPageContainsSameParams() {
        /* проверить, что на странице печати те же параметры, что и на стр. результата */
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobilePaymentsPageUrl());
        MobilePaymentsPage mobilePaymentsPage = new MobilePaymentsPage(getDriver());
        MobilePaymentPage mobilePaymentPage = mobilePaymentsPage.openPaymentByName("autotest", "Автовеб - 1");
        mobilePaymentPage.getInputParamFieldByName("param1").click();
        mobilePaymentPage.getInputParamFieldByName("param1").sendKeys("9104451325");
        mobilePaymentPage.getInputParamFieldByName("amount").sendKeys("10");
        mobilePaymentPage.getAccountRadioButton().click();
        mobilePaymentPage.submit();
        assertThat("Платеж должен завершиться успешно", waitAndGetPaymentResult(), is("Выполнено"));
        MobilePaymentResultPage paymentResultPage = new MobilePaymentResultPage(getDriver());

        String paymentFormedBy = paymentResultPage.getPaymentFormedBy();
        String paymentSource = paymentResultPage.getPaymentSource();
        String paidService = paymentResultPage.getPaidService();
        String paymentRecipient = paymentResultPage.getPaymentRecipient();
        String amount = paymentResultPage.getParamById("amount");
        String phoneNumber = paymentResultPage.getParamById("param1");
        String amountWithCommission = paymentResultPage.getAmountWithCommission();
        String date = paymentResultPage.getDate();
        String trxCode = paymentResultPage.getTrxCode();
        String bank = paymentResultPage.getBank();
        String bankTrxNumber = paymentResultPage.getBankTrxNumber();

        paymentResultPage.getPrintLink().click();
        MobilePaymentResultPrintPage mobilePaymentResultPrintPage = new MobilePaymentResultPrintPage(getDriver());

        assertThat("Неверно указано место формирования платежа", mobilePaymentResultPrintPage.getParamValueByName("Платеж сформирован:"), is(paymentFormedBy));
        assertThat("Неверно указан источник", mobilePaymentResultPrintPage.getParamValueByName("Источник:"), is(paymentSource));
        assertThat("Неверно указана оплачиваемая услуга", mobilePaymentResultPrintPage.getParamValueByName("Оплата услуг:"), is(paidService));
        assertThat("Неверно указан получатель", mobilePaymentResultPrintPage.getParamValueByName("Получатель:"), is(paymentRecipient));
        assertThat("Неверно указана сумма платежа", mobilePaymentResultPrintPage.getParamValueByName("Сумма платежа:"), is(amount));
        assertThat("Неверно указан номер телефона", mobilePaymentResultPrintPage.getParamValueByName("Параметр:"), is(phoneNumber));
        assertThat("Неверно указана сумма с комиссией", mobilePaymentResultPrintPage.getParamValueByName("Сумма с комиссией:"), is(amountWithCommission));
        assertThat("Неверно указана дата", mobilePaymentResultPrintPage.getParamValueByName("Дата:"), is(date));
        assertThat("Неверно указан код транзакции", mobilePaymentResultPrintPage.getParamValueByName("Код транзакции:"), is(trxCode));
        assertThat("Неверно указан расчетный банк", mobilePaymentResultPrintPage.getParamValueByName("Расчетный банк:"), is(bank));
        assertThat("Неверно указан номер банковской транзакции", mobilePaymentResultPrintPage.getParamValueByName("Номер банковской транзакции:"), is(bankTrxNumber));
    }

    @Features(MOBILE_FEATURE)
    @Stories({ACCOUNT_PAYMENT, MANAGE_FAVOURITES})
    @Test
    public void addToFavouritesFromPaymentResultPage() {
        /* добавить в избранное из результата платежа */
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobilePaymentsPageUrl());
        MobilePaymentsPage mobilePaymentsPage = new MobilePaymentsPage(getDriver());
        MobilePaymentPage mobilePaymentPage = mobilePaymentsPage.openPaymentByName("autotest", "Автовеб - 1");
        mobilePaymentPage.getInputParamFieldByName("param1").click();
        String phone = "9104451325";
        mobilePaymentPage.getInputParamFieldByName("param1").sendKeys(phone);
        String amount = "10";
        mobilePaymentPage.getInputParamFieldByName("amount").sendKeys(amount);
        mobilePaymentPage.getAccountRadioButton().click();
        mobilePaymentPage.submit();
        assertThat("Платеж должен завершиться успешно", waitAndGetPaymentResult(), is("Выполнено"));
        MobilePaymentResultPage paymentResultPage = new MobilePaymentResultPage(getDriver());

        // add to favorites
        paymentResultPage.getAddToFavoriteTitleInputField().click();
        String favTitle = String.valueOf(System.currentTimeMillis());
        paymentResultPage.getAddToFavoriteTitleInputField().sendKeys(favTitle);
        paymentResultPage.getSubmitAddToFavorites().click();

        // check adding to favorites
        MobileFavoritesPage mobileFavoritesPage = new MobileFavoritesPage(getDriver());
        assertThat(mobileFavoritesPage, containsPayment(favTitle));

        // open fav payment and check parameters
        mobileFavoritesPage.getPaymentLine(favTitle).getPaymentLink().click();
        mobilePaymentPage = new MobilePaymentPage(getDriver());
        String paymentSourceName = mobilePaymentPage.getPaymentSourceName();
        assertThat("Некорректнаый источник платежа", paymentSourceName, is("MOBILE"));
        String paymentAmount = mobilePaymentPage.getInputParamFieldByName("amount").getAttribute("value");
        assertThat("Некорректная сумма на форме из избранного", paymentAmount, is(amount));
        String paymentPhone = mobilePaymentPage.getInputParamFieldByName("param1").getAttribute("value");
        assertThat("Некорректный номер телефона на форме из избранного", paymentPhone, is(phone));
    }

    /* DEPRECATED */

    @Features(DEPRECATED)
    @Test
    @Ignore
    @Deprecated
    public void beelinePaymentFromAccount() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobilePaymentsPageUrl());
        MobilePaymentsPage mobilePaymentsPage = new MobilePaymentsPage(getDriver());
        MobilePaymentPage mobilePaymentPage = mobilePaymentsPage.openPaymentByName("autotest", "Автовеб - 1");
        mobilePaymentPage.getInputParamFieldByName("param1").click();
        mobilePaymentPage.getInputParamFieldByName("param1").sendKeys("9104451325");
        mobilePaymentPage.getInputParamFieldByName("amount").sendKeys("10");
        mobilePaymentPage.getAccountRadioButton().click();
        mobilePaymentPage.submit();
        assertThat("Платеж должен завершиться успешно", waitAndGetPaymentResult(), is("Выполнено"));
    }
}
