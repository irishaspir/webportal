package com.intervale.qa.mts.autotest.webportal.tests.desktop;

import com.intervale.qa.mts.autotest.webportal.Profiles;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.favorite.*;
import com.intervale.qa.mts.autotest.webportal.tests.AbstractSeleniumTest;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;


import java.util.List;

import static com.intervale.qa.mts.autotest.webportal.StoriesList.MANAGE_FAVOURITES;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static com.intervale.qa.mts.autotest.webportal.FeatureList.*;

/**
 * Created by dchahovsky.
 */
public class FavouritesPageTest extends AbstractSeleniumTest {

    @Features(DESKTOP_FEATURE)
    @Stories(MANAGE_FAVOURITES)
    @Description("Проверка наличия ссылки добавления в избранное")
    @Test
    public void hasAddFavoriteLink() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getFavoritesUrl());
        FavoritesPage favoritesPage = new FavoritesPage(getDriver());
        assertThat("Не найдена ссылка для добавления в избранное", favoritesPage.getAddToFavoriteLink().getText(), is("Добавить в Избранное"));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(MANAGE_FAVOURITES)
    @Description("Проверка заголовка страницы избранных")
    @Test
    public void favPageHasCertainTitle() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getFavoritesUrl());
        FavoritesPage favoritesPage = new FavoritesPage(getDriver());
        assertThat("Некорректный заголовок страницы избранных", favoritesPage.getPageTitle(), is("ИЗБРАННЫЕ ПЛАТЕЖИ"));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(MANAGE_FAVOURITES)
    @Description("Проверка всех необходимых данных и ссылок для избранных платежей")
    @Test
    public void rowsHaveAllNecessaryLinks() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getFavoritesUrl());
        FavoritesPage favoritesPage = new FavoritesPage(getDriver());
        List<FavoritePaymentLine> favoritePaymentLines = favoritesPage.getFavoritesList();
        for (FavoritePaymentLine line : favoritePaymentLines){
            assertThat("Не найдена ссылка на описание избранного платежа", line.getDescriptionLink(), not(nullValue()));
            assertThat("Не найдена ссылка на редактирование изьранного платежа", line.getEditLink(), not(nullValue()));
            assertThat("Не найдена ссылка на удаление избранного платежа", line.getDeleteLink(), not(nullValue()));
        }
    }

    @Features(DESKTOP_FEATURE)
    @Stories(MANAGE_FAVOURITES)
    @Description("Профиль 9859900702 содержит избранный платеж для веб-портала и мобильного приложение. Проверка, что в веб-портале отображается только созданный в нем избранный платеж.")
    @Test
    public void pageContainsOnlyWebFavouritePayments() {
        setActiveProfile(Profiles.WITH_FAVORITES_FROM_VARIOUS_PORTALS);
        getDriver().get(CONFIG.getFavoritesUrl());
        FavoritesPage favoritesPage = new FavoritesPage(getDriver());
        List<FavoritePaymentLine> favoritePaymentLines = favoritesPage.getFavoritesList();
        assertThat("Страница избранных должна содержать платеж 'МТС WEBPORTAL'", favoritesPage.containsPayment("МТС WEBPORTAL"), is(true));
        assertThat("Страница избранных должна содержать только один платеж", favoritePaymentLines.size(), is(1));
    }

}
