package com.intervale.qa.mts.autotest.webportal.tests.mobile;

import com.intervale.qa.mts.autotest.pcentre.cp.console.RegistrationTrxManager;
import com.intervale.qa.mts.autotest.webportal.Profiles;
import com.intervale.qa.mts.autotest.webportal.selenium.ACSPage;
import com.intervale.qa.mts.autotest.webportal.selenium.mobile.card.*;
import com.intervale.qa.mts.autotest.webportal.tests.AbstractSeleniumTest;
import com.intervale.qa.toolbox.util.card.Card;
import com.intervale.qa.toolbox.util.card.CardGenerator;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.allure.annotations.*;

import java.util.concurrent.TimeUnit;

import static com.intervale.qa.mts.autotest.webportal.StoriesList.MANAGE_CARDS;
import static com.intervale.qa.mts.autotest.webportal.matchers.MobileCardsListPageMatcher.containsActiveCardMobile;
import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.number.OrderingComparison.lessThanOrEqualTo;
import static com.intervale.qa.mts.autotest.webportal.FeatureList.*;
import static org.junit.Assume.assumeThat;
import static org.junit.Assume.assumeTrue;

/**
 * Created by dchahovsky.
 */
public class MobileCardManageTest extends AbstractSeleniumTest {

    private RegistrationTrxManager registrationTrxManager =
            new RegistrationTrxManager(CONFIG.getPcentreCpRpcUrl(), CONFIG.getPcentreCpRpcUsername(), CONFIG.getPcentreCpRpcPassword());


    @Parameter
    private Card generatedCard;

    @Before
    public void setUp() {
        generatedCard = null;
    }


    @Features(MOBILE_FEATURE)
    @Stories(MANAGE_CARDS)
    @Test
    public void addRenameAndRemoveNon3dsCard() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        Card card = generateNewNon3dsCard();
        String cardName = "test-" + card.getPan();
        addNon3dsCardStep(card, cardName);

        /* rename card */
        MobileCardsListPage mobileCardsListPage = new MobileCardsListPage(getDriver());
        MobileCardLine mobileCardLine = mobileCardsListPage.getCardLineByName(cardName);
        mobileCardLine.getEditLink().click();
        MobileRenameCardPage mobileRenameCardPage = new MobileRenameCardPage(getDriver());
        assertThat("Неправильное имя карты на форме переименования", mobileRenameCardPage.getCardAlias(), is(cardName));
        String newCardName = cardName + "edit";
        assumeThat("Имя карты не должно превышать 25 символов", newCardName.length(), lessThanOrEqualTo(25));
        mobileRenameCardPage.fillNewAlias(newCardName);
        assertThat("Не удалось ввести новое имя карты", mobileRenameCardPage.getCardAlias(), is(newCardName));
        mobileCardsListPage = mobileRenameCardPage.save();
        assertThat(mobileCardsListPage, containsActiveCardMobile(newCardName));

        /* remove card */
        mobileCardLine = mobileCardsListPage.getCardLineByName(newCardName);
        mobileCardLine.getDeleteLink().click();
        mobileCardsListPage.confirmDeleteCard();
        mobileCardsListPage = new MobileCardsListPage(getDriver());
        assertThat(mobileCardsListPage, not(containsActiveCardMobile(newCardName)));

    }

    @Features(MOBILE_FEATURE)
    @Stories(MANAGE_CARDS)
    @Test
    public void add3dsCard() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        Card card = generateNew3dsCard();
        String cardName = "test-" + card.getPan();
        add3dsCard(card, cardName);
    }

    @Features(MOBILE_FEATURE)
    @Stories(MANAGE_CARDS)
    @Description("Тест добавления карты без 3ds. Переход на страницу ввода зарезервированной суммы со страницы списка карт.")
    @Test
    public void continueAddingNon3dsCardFromCardList() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        Card card = generateNewNon3dsCard();
        String cardName = "test-" + card.getPan();
        getDriver().get(CONFIG.getMobileCardListUrl());
        MobileCardsListPage mobileCardListPage = new MobileCardsListPage(getDriver());
        mobileCardListPage.addNewCard();
        MobileAddCardPage mobileAddCardPage = new MobileAddCardPage(getDriver());
        mobileAddCardPage.fillPan(card.getPan());
        mobileAddCardPage.selectYear("2020");
        mobileAddCardPage.fillCvc(card.getSecurityCode());
        mobileAddCardPage.fillCardName(cardName);
        mobileAddCardPage.submitAddCard();
        assertTrue("Должен отображаться блок обработки", mobileAddCardPage.isProcessing());
        waitForCertainCurrentUrlUseContains(CONFIG.getCardReservedAmountInputPage());

        getDriver().get(CONFIG.getMobileCardListUrl());
        MobileCardsListPage mobileCardsListPage = new MobileCardsListPage(getDriver());
        MobileCardLine mobileLine = mobileCardsListPage.getCardLineByName(cardName);
        assertThat(mobileLine.getStateText(), is("Регистрация не подтверждена"));
        mobileLine.clickOnState();
        waitForCertainCurrentUrlUseMatches(CONFIG.getCardReservedAmountInputPage() + "\\?cardId=[A-F0-9]{32}");

        MobileCardRegistrationPage mobileCardRegistrationPage = new MobileCardRegistrationPage(getDriver());
        String reservedAmount = registrationTrxManager.getFirstReservedAmountForNewlyAddedCard(card);
        assumeThat("Неудалось получить зарезервированную сумму", reservedAmount, not(nullValue()));
        assumeTrue("Неверный формат зарезервированный суммы: " + reservedAmount, reservedAmount.matches("\\d\\.\\d\\d"));
        String[] amountParts = reservedAmount.split("\\.");
        mobileCardRegistrationPage.fillRubValue(amountParts[0]);
        mobileCardRegistrationPage.fillKopValue(amountParts[1]);
        mobileCardRegistrationPage.getOkButton().click();
        assertTrue("Должен отображаться блок обработки", mobileAddCardPage.isProcessing());
        waitForCertainCurrentUrlUseContains(CONFIG.getMobileCardListUrl());
        mobileCardListPage = new MobileCardsListPage(getDriver());
        assertThat(mobileCardListPage, containsActiveCardMobile(cardName));

    }

    @Features(MOBILE_FEATURE)
    @Stories(MANAGE_CARDS)
    @Description("Тест добавления 3ds карты. Продолжение добавления со страницы списка карт.")
    @Test
    public void continueAdding3dsCardFromCardList() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        Card card = generateNew3dsCard();
        String cardName = "test-" + card.getPan();
        getDriver().get(CONFIG.getMobileCardListUrl());
        MobileCardsListPage mobileCardListPage = new MobileCardsListPage(getDriver());
        MobileAddCardPage mobileAddCardPage = mobileCardListPage.addCard();
        mobileAddCardPage.fillPan(card.getPan());
        mobileAddCardPage.selectYear("2020");
        mobileAddCardPage.fillCvc(card.getSecurityCode());
        mobileAddCardPage.fillCardName(cardName);
        mobileAddCardPage.submitAddCard();
        assertTrue("Должен отображаться блок обработки", mobileAddCardPage.isProcessing());
        waitForDifferentCurrentUrl(CONFIG.getCardAddPageUrl());
        assertThat("Должен осуществиться переход на ACS", getDriver().getTitle(), is("Basic ACS"));

        getDriver().get(CONFIG.getMobileCardListUrl());
        MobileCardsListPage mobileCardsListPage = new MobileCardsListPage(getDriver());
        MobileCardLine mobileLine = mobileCardsListPage.getCardLineByName(cardName);
        assertThat(mobileLine.getStateText(), is("Регистрация не подтверждена"));
        mobileLine.clickOnState();
        waitForCertainCurrentUrlUseMatches(".*/basic-acs");
        ACSPage acsPage = new ACSPage(getDriver());
        acsPage.acsAccept();

        getDriver().navigate().refresh(); // Костыль для перезагрузки страницы, т.к. сразу карта не отображается
        mobileCardsListPage = new MobileCardsListPage(getDriver());
        assertThat(mobileCardsListPage, containsActiveCardMobile(cardName));


    }

    @Features(MOBILE_FEATURE)
    @Stories(MANAGE_CARDS)
    @Description("Проверка текстов ошибок при вводе неверной зарезервированной суммы, после трех ошибочных вводов и удаления карты по ссылке в сообщении об ошибке.")
    @Test
    public void testMessagesOnSubmittingIncorrectSumThreeTimes() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        Card card = generateNewNon3dsCard();
        String cardName = "test-" + card.getPan();
        getDriver().get(CONFIG.getMobileCardListUrl());
        MobileCardsListPage mobileCardListPage = new MobileCardsListPage(getDriver());
        mobileCardListPage.addNewCard();
        MobileAddCardPage mobileAddCardPage = new MobileAddCardPage(getDriver());
        mobileAddCardPage.fillPan(card.getPan());
        mobileAddCardPage.selectYear("2020");
        mobileAddCardPage.fillCvc(card.getSecurityCode());
        mobileAddCardPage.fillCardName(cardName);
        mobileAddCardPage.submitAddCard();
        assertTrue("Должен отображаться блок обработки", mobileAddCardPage.isProcessing());
        waitForCertainCurrentUrlUseContains(CONFIG.getCardReservedAmountInputPage());
        MobileCardRegistrationPage mobileCardRegistrationPage = new MobileCardRegistrationPage(getDriver());

        mobileCardRegistrationPage.fillRubValue("0");
        mobileCardRegistrationPage.fillKopValue("00");
        mobileCardRegistrationPage.getOkButton().click();
        assertTrue("Должен отображаться блок обработки", mobileAddCardPage.isProcessing());
        waitWhileIsProcessing(mobileAddCardPage);
        Assert.assertThat("Не правильный текст предупреждения", mobileCardRegistrationPage.getAttemptsText(),
                Matchers.is("Внимание! Операция не выполнена: введена неверная зарезервированная сумма. Остались 2 попытки. Узнайте точную сумму, заблокированную на счете вашей карты, с помощью SMS-информирования, интернет-банкинга или позвонив в банк по телефону, указанному на обратной стороне Вашей банковской карты."));
        mobileCardRegistrationPage.getOkButton().click();
        waitWhileIsProcessing(mobileAddCardPage);
        Assert.assertThat("Не правильный текст предупреждения", mobileCardRegistrationPage.getAttemptsText(),
                Matchers.is("Внимание! Операция не выполнена: введена неверная зарезервированная сумма. Осталась 1 попытка. Узнайте точную сумму, заблокированную на счете вашей карты, с помощью SMS-информирования, интернет-банкинга или позвонив в банк по телефону, указанному на обратной стороне Вашей банковской карты."));
        mobileCardRegistrationPage.getOkButton().click();
        waitWhileIsProcessing(mobileAddCardPage);
        Assert.assertThat("Не правильный текст предупреждения", mobileCardRegistrationPage.getFailedText(),
                Matchers.is("Внимание! Проверка карты не пройдена. Указана неверная зарезервированная сумма. Вам необходимо еще раз пройти процедуру регистрации карты, предварительно удалив ее из списка карт. Удалить?"));
        mobileCardRegistrationPage.getFailedDeleteLink().click();

        getDriver().findElement(By.xpath(".//input[contains(@class,'cardSbmt')]")).click();
        mobileCardListPage = new MobileCardsListPage(getDriver());
        assertThat(mobileCardListPage, not(containsActiveCardMobile(cardName)));

    }

    @Features(MOBILE_FEATURE)
    @Stories(MANAGE_CARDS)
    @Description("Проверка статуса карты при ACS reject при добавлении")
    @Test
    public void testStatusAndBehaviourOnAcsReject() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        Card card = generateNew3dsCard();
        String cardName = "test-" + card.getPan();
        getDriver().get(CONFIG.getMobileCardListUrl());
        MobileCardsListPage mobileCardListPage = new MobileCardsListPage(getDriver());
        MobileAddCardPage mobileAddCardPage = mobileCardListPage.addCard();
        mobileAddCardPage.fillPan(card.getPan());
        mobileAddCardPage.selectYear("2020");
        mobileAddCardPage.fillCvc(card.getSecurityCode());
        mobileAddCardPage.fillCardName(cardName);
        mobileAddCardPage.submitAddCard();
        assertTrue("Должен отображаться блок обработки", mobileAddCardPage.isProcessing());
        waitForDifferentCurrentUrl(CONFIG.getCardAddPageUrl());
        assertThat("Должен осуществиться переход на ACS", getDriver().getTitle(), is("Basic ACS"));
        ACSPage acsPage = new ACSPage(getDriver());
        acsPage.acsReject();
        mobileCardListPage = new MobileCardsListPage(getDriver());
        assertThat(mobileCardListPage, not(containsActiveCardMobile(cardName)));
    }

    @Features(MOBILE_FEATURE)
    @Stories(MANAGE_CARDS)
    @Description("Удаление карты после первой фазы ей добавления.")
    @Test
    public void removeCardAfterFirstPhase() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        Card card = generateNewNon3dsCard();
        String cardName = "test-" + card.getPan();
        getDriver().get(CONFIG.getMobileCardListUrl());
        MobileCardsListPage mobileCardsListPage = new MobileCardsListPage(getDriver());
        mobileCardsListPage.addNewCard();
        MobileAddCardPage mobileAddCardPage = new MobileAddCardPage(getDriver());
        mobileAddCardPage.fillPan(card.getPan());
        mobileAddCardPage.selectYear("2020");
        mobileAddCardPage.fillCvc(card.getSecurityCode());
        mobileAddCardPage.fillCardName(cardName);
        mobileAddCardPage.submitAddCard();
        assertTrue("Должен отображаться блок обработки", mobileAddCardPage.isProcessing());
        waitForCertainCurrentUrlUseContains(CONFIG.getCardReservedAmountInputPage());

        getDriver().get(CONFIG.getMobileCardListUrl());
        mobileCardsListPage = new MobileCardsListPage(getDriver());
        MobileCardLine mobileCardLine = mobileCardsListPage.getCardLineByName(cardName);
        mobileCardLine.getDeleteLink().click();
        mobileCardsListPage.confirmDeleteCard();
        mobileCardsListPage = new MobileCardsListPage(getDriver());
        assertThat(mobileCardsListPage, not(containsActiveCardMobile(cardName)));
    }

     /* ================== */

    @Step("Добавление банковской карты без 3ds")
    private void addNon3dsCardStep(Card card, String cardName) {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobileCardListUrl());
        MobileCardsListPage mobileCardListPage = new MobileCardsListPage(getDriver());
        mobileCardListPage.addNewCard();
        MobileAddCardPage mobileAddCardPage = new MobileAddCardPage(getDriver());
        mobileAddCardPage.fillPan(card.getPan());
        mobileAddCardPage.selectYear("2020");
        mobileAddCardPage.fillCvc(card.getSecurityCode());
        mobileAddCardPage.fillCardName(cardName);
        mobileAddCardPage.submitAddCard();

        assertTrue("Должен отображаться блок обработки", mobileAddCardPage.isProcessing());
        waitForCertainCurrentUrlUseContains(CONFIG.getCardReservedAmountInputPage());
        MobileCardRegistrationPage mobileCardRegistrationPage = new MobileCardRegistrationPage(getDriver());
        String reservedAmount = registrationTrxManager.getFirstReservedAmountForNewlyAddedCard(card);
        assumeThat("Неудалось получить зарезервированную сумму", reservedAmount, not(nullValue()));
        assumeTrue("Неверный формат зарезервированный суммы: " + reservedAmount, reservedAmount.matches("\\d\\.\\d\\d"));
        String[] amountParts = reservedAmount.split("\\.");
        mobileCardRegistrationPage.fillRubValue(amountParts[0]);
        mobileCardRegistrationPage.fillKopValue(amountParts[1]);
        mobileCardRegistrationPage.getOkButton().click();
        assertTrue("Должен отображаться блок обработки", mobileAddCardPage.isProcessing());
        waitForCertainCurrentUrlUseContains(CONFIG.getMobileCardListUrl());
        mobileCardListPage = new MobileCardsListPage(getDriver());
        assertThat(mobileCardListPage, containsActiveCardMobile(cardName));
    }


    @Step("Добавление банковской карты с 3ds")
    private void add3dsCard(Card card, String cardName) {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobileCardListUrl());
        MobileCardsListPage mobileCardsListPage = new MobileCardsListPage(getDriver());
        MobileAddCardPage mobileAddCardPage = mobileCardsListPage.addCard();
        mobileAddCardPage.fillPan(card.getPan());
        mobileAddCardPage.selectYear("2020");
        mobileAddCardPage.fillCvc(card.getSecurityCode());
        mobileAddCardPage.fillCardName(cardName);
        mobileAddCardPage.submitAddCard();
        assertTrue("Должен отображаться блок обработки", mobileAddCardPage.isProcessing());
        waitForDifferentCurrentUrl(CONFIG.getCardAddPageUrl());
        assertThat("Должен осуществиться переход на ACS", getDriver().getTitle(), is("Basic ACS"));
        ACSPage acsPage = new ACSPage(getDriver());
        acsPage.acsAccept();

        getDriver().navigate().refresh(); // Костыль для перезагрузки страницы, т.к. сразу карта не отображается
        mobileCardsListPage = new MobileCardsListPage(getDriver());
        assertThat(mobileCardsListPage, containsActiveCardMobile(cardName));
    }

    @Features(MOBILE_FEATURE)
    @Stories(MANAGE_CARDS)
    @Description("Проверка автоматически генерируемого имени карты MasterCard при добавлении")
    @Test
    public void autogeneratedMCCardName() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobileCardListUrl());
        MobileCardsListPage mobileCardsListPage = new MobileCardsListPage(getDriver());
        MobileAddCardPage mobileAddCardPage = mobileCardsListPage.addCard();
        WebElement mobileCardNameElement = mobileAddCardPage.getCardNameInputField();
        {
            Card card = new CardGenerator("53").generate();
            mobileAddCardPage.fillPan("");
            mobileCardNameElement.clear();
            mobileAddCardPage.fillPan(card.getPan());
            mobileCardNameElement.click();
            Assert.assertThat("Неправильное имя карты",  mobileCardNameElement.getAttribute("value"), Matchers.is("MC*" + card.getTail()));
        }
    }

    @Features(MOBILE_FEATURE)
    @Stories(MANAGE_CARDS)
    @Description("Проверка автоматически генерируемого имени карты visa при добавлении")
    @Test
    public void autogeneratedVisaCardName() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobileCardListUrl());
        MobileCardsListPage mobileCardsListPage = new MobileCardsListPage(getDriver());
        MobileAddCardPage mobileAddCardPage = mobileCardsListPage.addCard();
        WebElement mobileCardNameElement = mobileAddCardPage.getCardNameInputField();
        {
            Card card = new CardGenerator("44").generate();
            mobileAddCardPage.fillPan(card.getPan());
            mobileAddCardPage.selectYear("2020");
            mobileAddCardPage.fillCvc(card.getSecurityCode());
            mobileCardNameElement.click();
            Assert.assertThat("Неправильное имя карты", mobileCardNameElement.getAttribute("value"), is("Visa*" + card.getTail()));
        }
    }

    /* ====================== */

    private Card generateNew3dsCard() {
        generatedCard = VISA_3DS_GENERATOR.generate();
        return generatedCard;
    }

    private Card generateNewNon3dsCard() {
        generatedCard = VISA_NON_3DS_GENERATOR.generate();
        return generatedCard;
    }

    private void waitForDifferentCurrentUrl(String currentUrl) {
        long waitFor = System.currentTimeMillis() + 10_000;
        while (currentUrl.equals(getDriver().getCurrentUrl()) && (waitFor > System.currentTimeMillis())) {
            sleep(200);
        }
        if (currentUrl.equals(getDriver().getCurrentUrl())) {
            throw new IllegalStateException("Закончилось время ожидания перехода на другую страницу");
        }
    }

    private void waitForCertainCurrentUrlUseContains(String expectedUrlPattern) {
        long waitFor = System.currentTimeMillis() + 10_000;
        while ((!getDriver().getCurrentUrl().matches(expectedUrlPattern)) && (waitFor > System.currentTimeMillis())) {
            sleep(200);
        }
        if (!expectedUrlPattern.contains(getDriver().getCurrentUrl())) {
            throw new IllegalStateException("Закончилось время ожидания перехода на страницу " + expectedUrlPattern + "\nТекущая: " + getDriver().getCurrentUrl());
        }
    }

    private void waitForCertainCurrentUrlUseMatches(String expectedUrlPattern) {
        long waitFor = System.currentTimeMillis() + 10_000;
        while ((!getDriver().getCurrentUrl().matches(expectedUrlPattern)) && (waitFor > System.currentTimeMillis())) {
            sleep(200);
        }
        if (!getDriver().getCurrentUrl().matches(expectedUrlPattern)) {
            throw new IllegalStateException("Закончилось время ожидания перехода на страницу " + expectedUrlPattern + "\nТекущая: " + getDriver().getCurrentUrl());
        }
    }

    private boolean elementHasClass(WebElement webElement, String className) {
        String classes = " " + webElement.getAttribute("class") + " ";
        return classes.contains(" " + className + " ");
    }

    private void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ie) {
            throw new RuntimeException("Interrupted", ie);
        }
    }

    private void waitWhileIsProcessing(MobileAddCardPage mobileAddCardPage) {
        long timeout = System.currentTimeMillis() + 40_000;
        while (mobileAddCardPage.isProcessing() && System.currentTimeMillis() < timeout) {
            try {
                TimeUnit.MILLISECONDS.sleep(500);
            } catch (InterruptedException e) {
                throw new RuntimeException("Interrupted");
            }
        }
        if (mobileAddCardPage.isProcessing()) {
            throw new RuntimeException("Истекло 40 секунд ожидания обработки контрольной суммы.");
        }
    }
}
