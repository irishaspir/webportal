package com.intervale.qa.mts.autotest.webportal.tests.mobile;

import com.intervale.qa.mts.autotest.webportal.Profiles;
import com.intervale.qa.mts.autotest.webportal.selenium.ACSPage;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.PaymentPage;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.PaymentResultPage;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.PaymentResultPrintPage;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.PaymentsPage;
import com.intervale.qa.mts.autotest.webportal.selenium.mobile.MobilePaymentPage;
import com.intervale.qa.mts.autotest.webportal.selenium.mobile.MobilePaymentsPage;
import com.intervale.qa.mts.autotest.webportal.tests.AbstractSeleniumTest;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.intervale.qa.mts.autotest.webportal.FeatureList.MOBILE_FEATURE;
import static com.intervale.qa.mts.autotest.webportal.StoriesList.ANONYMOUS_CARD_PAYMENT;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertThat;

/**
 * Created by dchahovsky.
 */
public class MobileAnonymousCardPaymentTest extends AbstractSeleniumTest {



    @Features(MOBILE_FEATURE)
    @Stories(ANONYMOUS_CARD_PAYMENT)
    @Test
    public void anonymousPaymentFromAnonymousNon3dsCardAndPaymentResultPageTest() {
        /* провести анонимный платеж по анонимной карте без 3ds */
        /* проверить все поля на странице результата */
        getDriver().get(CONFIG.getMobilePaymentsPageUrl());
        MobilePaymentsPage mobilePaymentsPage = new MobilePaymentsPage(getDriver());
        MobilePaymentPage mobilePaymentPage = mobilePaymentsPage.openPaymentByName("autotest", "Автовеб - 1");
        mobilePaymentPage.getInputParamFieldByName("param1").click();
        mobilePaymentPage.getInputParamFieldByName("param1").sendKeys("9104451325");
        mobilePaymentPage.getInputParamFieldByName("amount").sendKeys("10");
        mobilePaymentPage.getUnregisteredCardRadioButton().click();
        mobilePaymentPage.getPanInputField().clear();
        mobilePaymentPage.getPanInputField().sendKeys("4890 4940 6492 8153");
        mobilePaymentPage.getCardHolderNameInputField().sendKeys("Qwer");
        mobilePaymentPage.getCvvInputField().sendKeys("123");
        mobilePaymentPage.selectSomeExpYear();
        mobilePaymentPage.submit();
        assertThat("Платеж должен завершиться успешно", waitAndGetPaymentResult(), is("Выполнено"));
        PaymentResultPage paymentResultPage = new PaymentResultPage(getDriver());
        assertThat("Должна отображаться ссылка 'Повторить платеж'", paymentResultPage.isRepeatLinkDisplayed(), is(true));
        assertThat("Должна отображаться ссылка отправки на e-mail", paymentResultPage.isSendToEmailLinkDisplayed(), is(true));
        assertThat("Должна отображаться ссылка распечатки квитанции", paymentResultPage.isPrintLinkDisplayed(), is(true));
      //  assertThat("Неверно указано место формирования платежа", paymentResultPage.getPaymentFormedBy(), is("На сайте «Легкий платеж»"));
        assertThat("Неверно указан источник", paymentResultPage.getPaymentSource(), is("Карта"));
        assertThat("Неверно указана оплачиваемая услуга", paymentResultPage.getPaidService(), is("Автовеб - 1"));
        assertThat("Неверно указан получатель", paymentResultPage.getPaymentRecipient(), is("ЗАО «Получатель платежа»"));
        assertThat("Неверно указана сумма платежа", paymentResultPage.getParamById("amount"), is("10 руб."));
        assertThat("Неверно указан номер телефона", paymentResultPage.getParamById("param1"), is("9104451325"));
        assertThat("Неверно указана сумма с комиссией", paymentResultPage.getAmountWithCommission(), is("20 руб."));
    }

    @Features(MOBILE_FEATURE)
    @Stories(ANONYMOUS_CARD_PAYMENT)
    @Test
    public void anonymousPaymentFromAnonymous3dsCard() {
        /* провести анонимный платеж с анонимной 3ds карты */
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobilePaymentsPageUrl());
        MobilePaymentsPage mobilePaymentsPage = new MobilePaymentsPage(getDriver());
        MobilePaymentPage mobilePaymentPage = mobilePaymentsPage.openPaymentByName("autotest", "Автовеб - 1");
        mobilePaymentPage.getInputParamFieldByName("param1").click();
        mobilePaymentPage.getInputParamFieldByName("param1").sendKeys("9104451325");
        mobilePaymentPage.getInputParamFieldByName("amount").sendKeys("10");
        mobilePaymentPage.getUnregisteredCardRadioButton().click();
        mobilePaymentPage.getPanInputField().clear();
        mobilePaymentPage.getPanInputField().sendKeys("5501 3501 2089 0183 ");
        mobilePaymentPage.getCardHolderNameInputField().sendKeys("Qwer");
        mobilePaymentPage.getCvvInputField().sendKeys("123");
        mobilePaymentPage.selectSomeExpYear();
        mobilePaymentPage.submit();
        waitForDifferentCurrentUrlStartsWith(CONFIG.getBaseUrl());
        assertThat("Должен осуществиться переход на ACS", getDriver().getTitle(), is("Basic ACS"));
        ACSPage acsPage = new ACSPage(getDriver());
        acsPage.acsAccept();
        assertThat("Платеж должен завершиться успешно", waitAndGetPaymentResult(), is("Выполнено"));
        PaymentResultPage paymentResultPage = new PaymentResultPage(getDriver());
        assertThat("Должна отображаться ссылка 'Повторить платеж'", paymentResultPage.isRepeatLinkDisplayed(), is(true));
        assertThat("Должна отображаться ссылка отправки на e-mail", paymentResultPage.isSendToEmailLinkDisplayed(), is(true));
        assertThat("Должна отображаться ссылка распечатки квитанции", paymentResultPage.isPrintLinkDisplayed(), is(true));
        assertThat("Неверно указано место формирования платежа", paymentResultPage.getPaymentFormedBy(), is("На сайте «Легкий платеж»"));
        assertThat("Неверно указан источник", paymentResultPage.getPaymentSource(), is("Карта"));
        assertThat("Неверно указана оплачиваемая услуга", paymentResultPage.getPaidService(), is("Автовеб - 1"));
        assertThat("Неверно указан получатель", paymentResultPage.getPaymentRecipient(), is("ЗАО «Получатель платежа»"));
        assertThat("Неверно указана сумма платежа", paymentResultPage.getParamById("amount"), is("10 руб."));
        assertThat("Неверно указан номер телефона", paymentResultPage.getParamById("param1"), is("9104451325"));
        assertThat("Неверно указана сумма с комиссией", paymentResultPage.getAmountWithCommission(), is("20 руб."));
    }

    @Features(MOBILE_FEATURE)
    @Stories(ANONYMOUS_CARD_PAYMENT)
    @Test
    public void paymentFromAnonymousNon3dsCardAndSuccessfulResultPageTest() {
        /* провести платеж от имени клиента с анонимной карты без 3ds */
        /* проверить все поля на странице результата */
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobilePaymentsPageUrl());
        MobilePaymentsPage mobilePaymentsPage = new MobilePaymentsPage(getDriver());
        MobilePaymentPage mobilePaymentPage = mobilePaymentsPage.openPaymentByName("autotest", "Автовеб - 1");
        mobilePaymentPage.getInputParamFieldByName("param1").click();
        mobilePaymentPage.getInputParamFieldByName("param1").sendKeys("9104451325");
        mobilePaymentPage.getInputParamFieldByName("amount").sendKeys("10");
        mobilePaymentPage.getUnregisteredCardRadioButton().click();
        mobilePaymentPage.getPanInputField().clear();
        mobilePaymentPage.getPanInputField().sendKeys("4890 4940 6492 8153");
        mobilePaymentPage.getCardHolderNameInputField().sendKeys("Qwer");
        mobilePaymentPage.getCvvInputField().sendKeys("123");
        mobilePaymentPage.selectSomeExpYear();
        mobilePaymentPage.submit();
        assertThat("Платеж должен завершиться успешно", waitAndGetPaymentResult(), is("Выполнено"));
        PaymentResultPage paymentResultPage = new PaymentResultPage(getDriver());
        assertThat("Должна отображаться ссылка 'Повторить платеж'", paymentResultPage.isRepeatLinkDisplayed(), is(true));
        assertThat("Должна отображаться ссылка отправки на e-mail", paymentResultPage.isSendToEmailLinkDisplayed(), is(true));
        assertThat("Должна отображаться ссылка распечатки квитанции", paymentResultPage.isPrintLinkDisplayed(), is(true));
        assertThat("Неверно указано место формирования платежа", paymentResultPage.getPaymentFormedBy(), is("На сайте «Легкий платеж»"));
        assertThat("Неверно указан источник", paymentResultPage.getPaymentSource(), is("Карта"));
        assertThat("Неверно указана оплачиваемая услуга", paymentResultPage.getPaidService(), is("Автовеб - 1"));
        assertThat("Неверно указан получатель", paymentResultPage.getPaymentRecipient(), is("ЗАО «Получатель платежа»"));
        assertThat("Неверно указана сумма платежа", paymentResultPage.getParamById("amount"), is("10 руб."));
        assertThat("Неверно указан номер телефона", paymentResultPage.getParamById("param1"), is("9104451325"));
        assertThat("Неверно указана сумма с комиссией", paymentResultPage.getAmountWithCommission(), is("20 руб."));
    }

    @Features(MOBILE_FEATURE)
    @Stories(ANONYMOUS_CARD_PAYMENT)
    @Test
    public void paymentFromAnonymous3dsCardWithAcsReject() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobilePaymentsPageUrl());
        MobilePaymentsPage mobilePaymentsPage = new MobilePaymentsPage(getDriver());
        MobilePaymentPage mobilePaymentPage = mobilePaymentsPage.openPaymentByName("autotest", "Автовеб - 1");
        mobilePaymentPage.getInputParamFieldByName("param1").click();
        mobilePaymentPage.getInputParamFieldByName("param1").sendKeys("9104451325");
        mobilePaymentPage.getInputParamFieldByName("amount").sendKeys("10");
        mobilePaymentPage.getUnregisteredCardRadioButton().click();
        mobilePaymentPage.getPanInputField().clear();
        mobilePaymentPage.getPanInputField().sendKeys("5501 3501 2089 0183 ");
        mobilePaymentPage.getCardHolderNameInputField().sendKeys("Qwer");
        mobilePaymentPage.getCvvInputField().sendKeys("123");
        mobilePaymentPage.selectSomeExpYear();
        mobilePaymentPage.submit();
        waitForDifferentCurrentUrlStartsWith(CONFIG.getBaseUrl());
        assertThat("Должен осуществиться переход на ACS", getDriver().getTitle(), is("Basic ACS"));
        ACSPage acsPage = new ACSPage(getDriver());
        acsPage.acsReject();
        assertThat("Платеж не должен завершиться успешно", waitAndGetPaymentResult(), is("Не выполнено"));
        PaymentResultPage paymentResultPage = new PaymentResultPage(getDriver());
        assertThat("Неверно указано место формирования платежа", paymentResultPage.getPaymentFormedBy(), is("На сайте «Легкий платеж»"));
        assertThat("Неверно указан источник", paymentResultPage.getPaymentSource(), is("Карта"));
        assertThat("Неверно указана оплачиваемая услуга", paymentResultPage.getPaidService(), is("Автовеб - 1"));
        assertThat("Неверно указан получатель", paymentResultPage.getPaymentRecipient(), is("ЗАО «Получатель платежа»"));
        assertThat("Неверно указана сумма платежа", paymentResultPage.getParamById("amount"), is("10 руб."));
        assertThat("Неверно указан номер телефона", paymentResultPage.getParamById("param1"), is("9104451325"));
        assertThat("Неверно указана сумма с комиссией", paymentResultPage.getAmountWithCommission(), is("20 руб."));
    }

    @Features(MOBILE_FEATURE)
    @Stories(ANONYMOUS_CARD_PAYMENT)
    @Test
    public void testLinkForCardOffer() {
        /* проверить наличие и корректность ссылки на договор-оферту по б/к */
        WebDriver driver = getDriver();
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobilePaymentsPageUrl());
        MobilePaymentsPage mobilePaymentsPage = new MobilePaymentsPage(getDriver());
        MobilePaymentPage mobilePaymentPage = mobilePaymentsPage.openPaymentByName("autotest", "Автовеб - 1");
        mobilePaymentPage.getAccountRadioButton().click();
        assertThat("Должна отображаться ссылка на договор-оферту", mobilePaymentPage.getOfferLink().isDisplayed(), is(true));
        String ref = mobilePaymentPage.getOfferLink().getAttribute("href");
        assertThat("Неверная ссылка на договор-оферту по карте", ref, startsWith(CONFIG.getBaseUrl()+"/webportal/offer-account"));
    }

    @Features(MOBILE_FEATURE)
    @Stories(ANONYMOUS_CARD_PAYMENT)
    @Test
    public void printResultPageContainsAllFields() {
        /* провести успешный платеж и проверить, что все поля на печатной форме заполнены */
        WebDriver driver = getDriver();
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobilePaymentsPageUrl());
        MobilePaymentsPage mobilePaymentsPage = new MobilePaymentsPage(getDriver());
        MobilePaymentPage mobilePaymentPage = mobilePaymentsPage.openPaymentByName("autotest", "Автовеб - 1");
        mobilePaymentPage.getInputParamFieldByName("param1").sendKeys("9104451325");
        mobilePaymentPage.getInputParamFieldByName("amount").sendKeys("10");
        mobilePaymentPage.getUnregisteredCardRadioButton().click();
        mobilePaymentPage.getPanInputField().clear();
        mobilePaymentPage.getPanInputField().sendKeys("4890 4940 6492 8153 ");
        mobilePaymentPage.getCardHolderNameInputField().sendKeys("Qwer");
        mobilePaymentPage.getCvvInputField().sendKeys("123");
        mobilePaymentPage.selectSomeExpYear();
        mobilePaymentPage.submit();
        assertThat("Платеж должен завершиться успешно", waitAndGetPaymentResult(), is("Выполнено"));
        PaymentResultPage paymentResultPage = new PaymentResultPage(driver);

        String paymentFormedBy = paymentResultPage.getPaymentFormedBy();
        String paymentSource = paymentResultPage.getPaymentSource();
        String paidService = paymentResultPage.getPaidService();
        String paymentRecipient = paymentResultPage.getPaymentRecipient();
        String amount = paymentResultPage.getParamById("amount");
        String phoneNumber = paymentResultPage.getParamById("param1");
        String amountWithCommission = paymentResultPage.getAmountWithCommission();
        String date = paymentResultPage.getDate();
        String trxCode = paymentResultPage.getTrxCode();
        String bankTrxNumber = paymentResultPage.getBankTrxNumber();

        paymentResultPage.getPrintLink().click();
        PaymentResultPrintPage paymentResultPrintPage = new PaymentResultPrintPage(driver);

        assertThat("Неверно указано место формирования платежа", paymentResultPrintPage.getParamValueByName("Платеж сформирован:"), is(paymentFormedBy));
        assertThat("Неверно указан источник", paymentResultPrintPage.getParamValueByName("Источник:"), is(paymentSource));
        assertThat("Неверно указана оплачиваемая услуга", paymentResultPrintPage.getParamValueByName("Оплата услуг:"), is(paidService));
        assertThat("Неверно указан получатель", paymentResultPrintPage.getParamValueByName("Получатель:"), is(paymentRecipient));
        assertThat("Неверно указана сумма платежа", paymentResultPrintPage.getParamValueByName("Сумма платежа:"), is(amount));
        assertThat("Неверно указан номер телефона", paymentResultPrintPage.getParamValueByName("Параметр:"), is(phoneNumber));
        assertThat("Неверно указана сумма с комиссией", paymentResultPrintPage.getParamValueByName("Сумма с комиссией:"), is(amountWithCommission));
        assertThat("Неверно указана дата", paymentResultPrintPage.getParamValueByName("Дата:"), is(date));
        assertThat("Неверно указан код транзакции", paymentResultPrintPage.getParamValueByName("Код транзакции:"), is(trxCode));
        assertThat("Неверно указан номер банковской транзакции", paymentResultPrintPage.getParamValueByName("Номер банковской транзакции:"), is(bankTrxNumber));
    }


}
