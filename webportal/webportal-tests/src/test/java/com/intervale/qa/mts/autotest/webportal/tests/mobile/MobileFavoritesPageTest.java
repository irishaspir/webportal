package com.intervale.qa.mts.autotest.webportal.tests.mobile;

import com.intervale.qa.mts.autotest.webportal.Profiles;
import com.intervale.qa.mts.autotest.webportal.selenium.mobile.favorite.MobileAddFavoritePage;
import com.intervale.qa.mts.autotest.webportal.selenium.mobile.favorite.MobileChooseFavoritePaymentPage;
import com.intervale.qa.mts.autotest.webportal.selenium.mobile.favorite.MobileFavoritePaymentLine;
import com.intervale.qa.mts.autotest.webportal.selenium.mobile.favorite.MobileFavoritesPage;
import com.intervale.qa.mts.autotest.webportal.tests.AbstractSeleniumTest;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.List;

import static com.intervale.qa.mts.autotest.webportal.FeatureList.MOBILE_FEATURE;
import static com.intervale.qa.mts.autotest.webportal.StoriesList.MANAGE_CARDS;
import static com.intervale.qa.mts.autotest.webportal.StoriesList.MANAGE_FAVOURITES;
import static com.intervale.qa.mts.autotest.webportal.matchers.MobileFavoritesPageMatcher.containsPayment;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.fail;

/**
 * Created by ashaporev
 */
public class MobileFavoritesPageTest extends AbstractSeleniumTest {

    @Features(MOBILE_FEATURE)
    @Stories(MANAGE_FAVOURITES)
    @Description("Проверка заголовка страницы избранных")
    @Test
    public void mobileFavouritesListPageHasCertainTitle() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobileFavoritesUrl());
        MobileFavoritesPage mobileFavoritesPage = new MobileFavoritesPage(getDriver());
        Assert.assertThat("Некорректный заголовок страницы избранных", mobileFavoritesPage.getPageTitle(), is("Избранные платежи"));
    }

    @Features(MOBILE_FEATURE)
    @Stories(MANAGE_FAVOURITES)
    @Description("Проверка наличия ссылки добавления в избранное")
    @Test
    public void pageHasAddFavouriteLink() {
        /* Проверить, что на странице по списком избранных есть ссылка на страницу добавления избранного */
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobileFavoritesUrl());
        MobileFavoritesPage mobileFavoritesPage = new MobileFavoritesPage(getDriver());
        Assert.assertThat("Не найдена ссылка для добавления в избранное", mobileFavoritesPage.getAddToFavoriteLink().getText(), is("Добавить в Избранное"));
    }


    @Features(MOBILE_FEATURE)
    @Stories(MANAGE_FAVOURITES)
    @Description("Проверка всех необходимых данных и ссылок для избранных платежей")
    @Test
    public void rowsHaveAllNecessaryLinks() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobileFavoritesUrl());
        MobileFavoritesPage mobileFavoritesPage = new MobileFavoritesPage(getDriver());
        List<MobileFavoritePaymentLine> mobileFavoritePaymentLines = mobileFavoritesPage.getFavoritesList();
        for (MobileFavoritePaymentLine line : mobileFavoritePaymentLines){
            assertThat("Не найдена ссылка на оплату избранного платежа", line.getPaymentLink(), not(nullValue()));
            assertThat("Не найдена ссылка на редактирование изьранного платежа", line.getEditLink(), not(nullValue()));
            assertThat("Не найдена ссылка на удаление избранного платежа", line.getDeleteLink(), not(nullValue()));
        }
    }

    @Features(MOBILE_FEATURE)
    @Stories(MANAGE_FAVOURITES)
    @Description("Профиль 9859900702 содержит избранный платеж для веб-портала и мобильного приложение. Проверка, что в веб-портале отображается только созданный в нем избранный платеж.")
    @Test
    public void pageContainsOnlyWebFavouritePayments() {
        setActiveProfile(Profiles.WITH_FAVORITES_FROM_VARIOUS_PORTALS);
        getDriver().get(CONFIG.getMobileFavoritesUrl());
        MobileFavoritesPage mobileFavoritesPage = new MobileFavoritesPage(getDriver());
        List<MobileFavoritePaymentLine> mobileFavoritePaymentLines = mobileFavoritesPage.getFavoritesList();
        Assert.assertThat("Страница избранных должна содержать платеж 'МТС WEBPORTAL'", mobileFavoritesPage.containPaymentWithName("МТС WEBPORTAL"), is(true));
        Assert.assertThat("Страница избранных должна содержать только один платеж", mobileFavoritePaymentLines.size(), is(1));
    }

}
