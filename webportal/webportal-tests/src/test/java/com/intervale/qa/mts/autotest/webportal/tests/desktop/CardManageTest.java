package com.intervale.qa.mts.autotest.webportal.tests.desktop;

import com.intervale.qa.mts.autotest.pcentre.cp.console.RegistrationTrxManager;
import com.intervale.qa.mts.autotest.webportal.Profiles;
import com.intervale.qa.mts.autotest.webportal.selenium.ACSPage;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.cards.*;
import com.intervale.qa.mts.autotest.webportal.tests.AbstractSeleniumTest;
import com.intervale.qa.toolbox.util.card.Card;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.allure.annotations.*;

import java.util.concurrent.TimeUnit;

import static com.intervale.qa.mts.autotest.webportal.StoriesList.MANAGE_CARDS;
import static com.intervale.qa.mts.autotest.webportal.matchers.CardsListPageMatcher.containsActiveCard;
import static com.intervale.qa.mts.autotest.webportal.matchers.CardsListPageMatcher.containsFailedCard;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeThat;
import static org.junit.Assume.assumeTrue;
import static com.intervale.qa.mts.autotest.webportal.FeatureList.*;

/**
 * Created by dchahovsky.
 */
public class CardManageTest extends AbstractSeleniumTest {

    private RegistrationTrxManager registrationTrxManager =
            new RegistrationTrxManager(CONFIG.getPcentreCpRpcUrl(), CONFIG.getPcentreCpRpcUsername(), CONFIG.getPcentreCpRpcPassword());


    /* Используется для отображения в отчете. Заполняется при генерации нового pan. */
    @Parameter
    private Card generatedCard;

    @Before
    public void setUp() {
        generatedCard = null;
    }

    @Features(DESKTOP_FEATURE)
    @Stories(MANAGE_CARDS)
    @Description("Тест добавления, переименования и удаления карты без 3ds")
    @Test
    public void addRenameAndRemoveNon3dsCard() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        Card card = generateNewNon3dsCard();
        String cardName = "test-" + card.getPan();
        addNon3dsCardStep(card, cardName);

        /* rename card */
        CardsListPage cardsListPage = new CardsListPage(getDriver());
        CardLine cardLine = cardsListPage.getCardLineByCardName(cardName);
        cardLine.getEditLink().click();
        RenameCardPage renameCardPage = new RenameCardPage(getDriver());
        assertThat("Неправильное имя карты на форме переименования", renameCardPage.getCardAlias(), is(cardName));
        String newCardName =  cardName + "edit";
        assumeThat("Имя карты не должно превышать 25 символов", newCardName.length(), lessThanOrEqualTo(25));
        renameCardPage.fillNewAlias(newCardName);
        assertThat("Не удалось ввести новое имя карты", renameCardPage.getCardAlias(), is(newCardName));
        cardsListPage = renameCardPage.save();
        assertThat(cardsListPage, containsActiveCard(newCardName));

        /* remove card */
        cardLine = cardsListPage.getCardLineByCardName(newCardName);
        cardLine.getDeleteLink().click();
        cardsListPage.confirmPopupForm();
        cardsListPage = new CardsListPage(getDriver());
        assertThat(cardsListPage, not(containsActiveCard(newCardName)));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(MANAGE_CARDS)
    @Description("Тест добавления карты 3ds")
    @Test
    public void add3dsCard() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        Card card = generateNew3dsCard();
        add3dsCard(card, "test-" + card.getPan());
    }

    @Features(DESKTOP_FEATURE)
    @Stories(MANAGE_CARDS)
    @Description("Тест добавления карты без 3ds. Переход на страницу ввода зарезервированной суммы со страницы списка карт.")
    @Test
    public void continueAddingNon3dsCardFromCardList() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        Card card = generateNewNon3dsCard();
        String cardName = "test-" + card.getPan();

        getDriver().get(CONFIG.getCardAddPageUrl());
        AddCardPage addCardPage = new AddCardPage(getDriver());
        addCardPage.fillPan(card.getPan());
        addCardPage.selectExpYear("2020");
        addCardPage.fillCvc(card.getSecurityCode());
        addCardPage.fillName(cardName);
        addCardPage.getAcceptButton().click();
        assertTrue("Должен отображаться блок обработки", addCardPage.isProcessing());
        waitForCertainCurrentUrl(CONFIG.getCardReservedAmountInputPage());

        getDriver().get(CONFIG.getCardsListUrl());
        CardsListPage cardsListPage = new CardsListPage(getDriver());
        CardLine line = cardsListPage.getCardLineByCardName(cardName);
        assertThat(line.getStateText(), is("Регистрация не подтверждена"));
        line.clickOnState();
        waitForCertainCurrentUrl(CONFIG.getCardReservedAmountInputPage() + "\\?cardId=[A-F0-9]{32}");

        RegisterCardPage registerCardPage = new RegisterCardPage(getDriver());
        String reservedAmount = registrationTrxManager.getFirstReservedAmountForNewlyAddedCard(card);
        assumeThat("Неудалось получить зарезервированную сумму", reservedAmount, not(nullValue()));
        assumeTrue("Неверный формат зарезервированный суммы: " + reservedAmount, reservedAmount.matches("\\d\\.\\d\\d"));
        String[] amountParts = reservedAmount.split("\\.");
        registerCardPage.fillRubValue(amountParts[0]);
        registerCardPage.fillKopValue(amountParts[1]);
        registerCardPage.getOkButton().click();
        assertTrue("Должен отображаться блок обработки", addCardPage.isProcessing());
        waitForCertainCurrentUrl(CONFIG.getCardsListUrl());
        cardsListPage = new CardsListPage(getDriver());
        assertThat(cardsListPage, containsActiveCard(cardName));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(MANAGE_CARDS)
    @Description("Тест добавления 3ds карты. Продолжение добавления со страницы списка карт.")
    @Test
    public void continueAdding3dsCardFromCardList() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        Card card = generateNew3dsCard();
        String cardName = "test-" + card.getPan();

        getDriver().get(CONFIG.getCardAddPageUrl());
        AddCardPage addCardPage = new AddCardPage(getDriver());
        addCardPage.fillPan(card.getPan());
        addCardPage.selectExpYear("2020");
        addCardPage.fillCvc(card.getSecurityCode());
        addCardPage.fillName(cardName);
        addCardPage.getAcceptButton().click();
        assertTrue("Должен отображаться блок обработки", addCardPage.isProcessing());
        waitForDifferentCurrentUrl(CONFIG.getCardAddPageUrl());
        assertThat("Должен осуществиться переход на ACS", getDriver().getTitle(), is("Basic ACS"));

        getDriver().get(CONFIG.getCardsListUrl());
        CardsListPage cardsListPage = new CardsListPage(getDriver());
        CardLine line = cardsListPage.getCardLineByCardName(cardName);
        assertThat(line.getStateText(), is("Регистрация не подтверждена"));
        line.clickOnState();
        waitForCertainCurrentUrl(".*/basic-acs");
        ACSPage acsPage = new ACSPage(getDriver());
        acsPage.acsAccept();

        getDriver().navigate().refresh(); // Костыль для перезагрузки страницы, т.к. сразу карта не отображается
        cardsListPage = new CardsListPage(getDriver());
        assertThat(cardsListPage, containsActiveCard(cardName));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(MANAGE_CARDS)
    @Description("Проверка текстов ошибок при вводе неверной зарезервированной суммы, после трех ошибочных вводов и удаления карты по ссылке в сообщении об ошибке.")
    @Test
    public void testMessagesOnSubmittingIncorrectSumThreeTimes() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        Card card = generateNewNon3dsCard();
        String cardName = "test-" + card.getPan();
        getDriver().get(CONFIG.getCardsListUrl());
        CardsListPage cardsListPage = new CardsListPage(getDriver());
        AddCardPage addCardPage = cardsListPage.addCard();
        addCardPage.fillPan(card.getPan());
        addCardPage.selectExpYear("2020");
        addCardPage.fillCvc(card.getSecurityCode());
        addCardPage.fillName(cardName);
        addCardPage.getAcceptButton().click();
        assertTrue("Должен отображаться блок обработки", addCardPage.isProcessing());
        waitForCertainCurrentUrl(CONFIG.getCardReservedAmountInputPage());
        RegisterCardPage registerCardPage = new RegisterCardPage(getDriver());

        registerCardPage.fillRubValue("0");
        registerCardPage.fillKopValue("00");
        registerCardPage.getOkButton().click();
        assertTrue("Должен отображаться блок обработки", addCardPage.isProcessing());
        waitWhileIsProcessing(addCardPage);
        assertThat("Не правильный текст предупреждения", registerCardPage.getAttemptsText(),
                is("Внимание! Операция не выполнена: введена неверная зарезервированная сумма. Остались 2 попытки. Узнайте точную сумму, заблокированную на счете вашей карты, с помощью SMS-информирования, интернет-банкинга или позвонив в банк по телефону, указанному на обратной стороне Вашей банковской карты."));

        registerCardPage.getOkButton().click();
        assertTrue("Должен отображаться блок обработки", addCardPage.isProcessing());
        waitWhileIsProcessing(addCardPage);
        assertThat("Не правильный текст предупреждения", registerCardPage.getAttemptsText(),
                is("Внимание! Операция не выполнена: введена неверная зарезервированная сумма. Осталась 1 попытка. Узнайте точную сумму, заблокированную на счете вашей карты, с помощью SMS-информирования, интернет-банкинга или позвонив в банк по телефону, указанному на обратной стороне Вашей банковской карты."));

        registerCardPage.getOkButton().click();
        assertTrue("Должен отображаться блок обработки", addCardPage.isProcessing());
        waitWhileIsProcessing(addCardPage);
        assertThat("Не правильный текст предупреждения", registerCardPage.getFailedText(),
                is("Внимание! Проверка карты не пройдена. Указана неверная зарезервированная сумма. Вам необходимо еще раз пройти процедуру регистрации карты, предварительно удалив ее из списка карт. Удалить?"));

        registerCardPage.getFailedDeleteLink().click();
        getDriver().findElement(By.id("popup_form")).findElement(By.xpath(".//button[contains(concat(' ', @class, ' '), ' actnbtn ')]")).click();
        cardsListPage = new CardsListPage(getDriver());
        assertThat(cardsListPage, not(containsActiveCard(cardName)));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(MANAGE_CARDS)
    @Description("Проверка статуса карты при ACS reject при добавлении")
    @Test
    public void testStatusAndBehaviourOnAcsReject() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        Card card = generateNew3dsCard();
        String cardName = "test-" + card.getPan();

        getDriver().get(CONFIG.getCardAddPageUrl());
        AddCardPage addCardPage = new AddCardPage(getDriver());
        addCardPage.fillPan(card.getPan());
        addCardPage.selectExpYear("2020");
        addCardPage.fillCvc(card.getSecurityCode());
        addCardPage.fillName(cardName);
        addCardPage.getAcceptButton().click();
        assertTrue("Должен отображаться блок обработки", addCardPage.isProcessing());
        waitForDifferentCurrentUrl(CONFIG.getCardAddPageUrl());
        assertThat("Должен осуществиться переход на ACS", getDriver().getTitle(), is("Basic ACS"));
        ACSPage acsPage = new ACSPage(getDriver());
        acsPage.acsReject();

        getDriver().navigate().refresh(); // Костыль для перезагрузки страницы, т.к. сразу карта не отображается
        CardsListPage cardsListPage = new CardsListPage(getDriver());
        assertThat(cardsListPage, containsFailedCard(cardName));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(MANAGE_CARDS)
    @Description("Удаление карты после первой фазы ей добавления.")
    @Test
    public void removeCardAfterFirstPhase() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        Card card = generateNewNon3dsCard();
        String cardName = "test-" + card.getPan();

        getDriver().get(CONFIG.getCardAddPageUrl());
        AddCardPage addCardPage = new AddCardPage(getDriver());
        addCardPage.fillPan(card.getPan());
        addCardPage.selectExpYear("2020");
        addCardPage.fillCvc(card.getSecurityCode());
        addCardPage.fillName(cardName);
        addCardPage.getAcceptButton().click();
        assertTrue("Должен отображаться блок обработки", addCardPage.isProcessing());
        waitForCertainCurrentUrl(CONFIG.getCardReservedAmountInputPage());

        getDriver().get(CONFIG.getCardsListUrl());
        CardsListPage cardsListPage = new CardsListPage(getDriver());
        cardsListPage.getCardLineByCardName(cardName).getDeleteLink().click();
        cardsListPage.confirmPopupForm();
        cardsListPage = new CardsListPage(getDriver());
        assertThat(cardsListPage, not(containsActiveCard(cardName)));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(MANAGE_CARDS)
    @Description("Проверка подсказки и подсветки поля при вводе неверной зарезервированной суммы")
    @Test
    public void testHintsOnIncorrectReservedAmountValueFormat() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        Card card = generateNewNon3dsCard();
        String cardName = "test-" + card.getPan();

        getDriver().get(CONFIG.getCardAddPageUrl());
        AddCardPage addCardPage = new AddCardPage(getDriver());
        addCardPage.fillPan(card.getPan());
        addCardPage.selectExpYear("2020");
        addCardPage.fillCvc(card.getSecurityCode());
        addCardPage.fillName(cardName);
        addCardPage.getAcceptButton().click();
        assertTrue("Должен отображаться блок обработки", addCardPage.isProcessing());
        waitForCertainCurrentUrl(CONFIG.getCardReservedAmountInputPage());

        RegisterCardPage registerCardPage = new RegisterCardPage(getDriver());
        registerCardPage.submitReservedAmount();

        assertTrue("Должен отображаться блок с описанием ошибки", registerCardPage.hasErrorMessage());
        assertThat("Неправильный текст ошибки", registerCardPage.getErrorMessage(), equalTo("Внимание! Введите корректную сумму."));

        assertTrue("Поле ввода суммы должно подсвечиваться", elementHasClass(registerCardPage.getRegAmountRub(), "error"));
        assertTrue("Поле ввода суммы должно подсвечиваться", elementHasClass(registerCardPage.getRegAmountKop(), "error"));
    }

    /* ================== */

    @Step("Добавление банковской карты без 3ds")
    private void addNon3dsCardStep(Card card, String cardName) {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getCardsListUrl());
        CardsListPage cardsListPage = new CardsListPage(getDriver());
        AddCardPage addCardPage = cardsListPage.addCard();
        addCardPage.fillPan(card.getPan());
        addCardPage.selectExpYear("2020");
        addCardPage.fillCvc(card.getSecurityCode());
        addCardPage.fillName(cardName);
        addCardPage.getAcceptButton().click();
        assertTrue("Должен отображаться блок обработки", addCardPage.isProcessing());
        waitForCertainCurrentUrl(CONFIG.getCardReservedAmountInputPage());
        RegisterCardPage registerCardPage = new RegisterCardPage(getDriver());
        String reservedAmount = registrationTrxManager.getFirstReservedAmountForNewlyAddedCard(card);
        assumeThat("Неудалось получить зарезервированную сумму", reservedAmount, not(nullValue()));
        assumeTrue("Неверный формат зарезервированный суммы: " + reservedAmount, reservedAmount.matches("\\d\\.\\d\\d"));
        String[] amountParts = reservedAmount.split("\\.");
        registerCardPage.fillRubValue(amountParts[0]);
        registerCardPage.fillKopValue(amountParts[1]);
        registerCardPage.getOkButton().click();
        assertTrue("Должен отображаться блок обработки", addCardPage.isProcessing());
        waitForCertainCurrentUrl(CONFIG.getCardsListUrl());
        cardsListPage = new CardsListPage(getDriver());
        assertThat(cardsListPage, containsActiveCard(cardName));
    }

    @Step("Добавление банковской карты с 3ds")
    private void add3dsCard(Card card, String cardName) {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getCardsListUrl());
        CardsListPage cardsListPage = new CardsListPage(getDriver());
        AddCardPage addCardPage = cardsListPage.addCard();
        addCardPage.fillPan(card.getPan());
        addCardPage.selectExpYear("2020");
        addCardPage.fillCvc(card.getSecurityCode());
        addCardPage.fillName(cardName);
        addCardPage.getAcceptButton().click();
        assertTrue("Должен отображаться блок обработки", addCardPage.isProcessing());
        waitForDifferentCurrentUrl(CONFIG.getCardAddPageUrl());
        assertThat("Должен осуществиться переход на ACS", getDriver().getTitle(), is("Basic ACS"));
        ACSPage acsPage = new ACSPage(getDriver());
        acsPage.acsAccept();

        getDriver().navigate().refresh(); // Костыль для перезагрузки страницы, т.к. сразу карта не отображается
        cardsListPage = new CardsListPage(getDriver());
        assertThat(cardsListPage, containsActiveCard(cardName));
    }

    /* ================== */

    private Card generateNew3dsCard() {
        generatedCard = VISA_3DS_GENERATOR.generate();
        return generatedCard;
    }

    private Card generateNewNon3dsCard() {
        generatedCard = VISA_NON_3DS_GENERATOR.generate();
        return generatedCard;
    }

    private void waitForDifferentCurrentUrl(String currentUrl) {
        long waitFor = System.currentTimeMillis() + 10_000;
        while (currentUrl.equals(getDriver().getCurrentUrl()) && (waitFor > System.currentTimeMillis())) {
            sleep(200);
        }
        if (currentUrl.equals(getDriver().getCurrentUrl())) {
            throw new IllegalStateException("Закончилось время ожидания перехода на другую страницу");
        }
    }

    private void waitForCertainCurrentUrl(String expectedUrlPattern) {
        long waitFor = System.currentTimeMillis() + 5_000;
        while ((!getDriver().getCurrentUrl().matches(expectedUrlPattern)) && (waitFor > System.currentTimeMillis())) {
            sleep(200);
        }
        if (!getDriver().getCurrentUrl().matches(expectedUrlPattern)) {
            throw new IllegalStateException("Закончилось время ожидания перехода на страницу " + expectedUrlPattern + "\nТекущая: " + getDriver().getCurrentUrl());
        }
    }

    private boolean elementHasClass(WebElement webElement, String className) {
        String classes = " " + webElement.getAttribute("class") + " ";
        return classes.contains(" " + className + " ");
    }

    private void sleep(long millis) {
        try {
            Thread.sleep(millis);
        }
        catch (InterruptedException ie) {
            throw new RuntimeException("Interrupted", ie);
        }
    }

    private void waitWhileIsProcessing(AddCardPage addCardPage) {
        long timeout = System.currentTimeMillis()+40_000;
        while(addCardPage.isProcessing() && System.currentTimeMillis()<timeout){
            try {
                TimeUnit.MILLISECONDS.sleep(500);
            } catch (InterruptedException e) {
                throw new RuntimeException("Interrupted");
            }
        }
        if (addCardPage.isProcessing()){
            throw new RuntimeException("Истекло 40 секунд ожидания обработки контрольной суммы.");
        }
    }
}
