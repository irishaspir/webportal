package com.intervale.qa.mts.autotest.webportal.tests.desktop;

import com.intervale.qa.mts.autotest.webportal.Profiles;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.*;
import com.intervale.qa.mts.autotest.webportal.tests.AbstractSeleniumTest;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.Arrays;

import static com.intervale.qa.mts.autotest.webportal.FeatureList.*;
import static com.intervale.qa.mts.autotest.webportal.StoriesList.SEARCH;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static ru.yandex.qatools.matchers.collection.HasSameItemsAsListMatcher.hasSameItemsAsList;

/**
 * Created by dchahovsky.
 */
public class PaymentSearchTest extends AbstractSeleniumTest {

    @Features(DESKTOP_FEATURE)
    @Stories(SEARCH)
    @Description("Поиск платежа, проверка списка найденых, выбор платежа, проверка перехода на форму")
    @Test
    public void searchOnMainPage() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        WebDriver driver = getDriver();
        driver.get(CONFIG.getMainPageUrl());
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        paymentsPage.searchForPayment("yota");
        PaymentsSearchResultPage paymentsSearchResultPage = new PaymentsSearchResultPage(driver);
        assertThat(paymentsSearchResultPage.getListPaymentsTitles(), hasSameItemsAsList(Arrays.asList(
                "Yota",
                "Yota - По номеру счета"
        )));
        paymentsSearchResultPage.openPayment("Yota");
        PaymentPage paymentPage = new PaymentPage(driver);
        assertThat("Неправильное название страницы платежа", driver.findElement(By.tagName("h1")).getText(), is("YOTA"));
        assertThat("Неправильное описание страницы платежа", paymentPage.getMenuDescription(), is("ООО «Скартел»"));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(SEARCH)
    @Description("Проверка поиска по частичному имени платежа")
    @Test
    public void partialTitleSearch() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        WebDriver driver = getDriver();
        driver.get(CONFIG.getMainPageUrl());
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        paymentsPage.searchForPayment("yot");
        PaymentsSearchResultPage paymentsSearchResultPage = new PaymentsSearchResultPage(driver);
        assertThat(paymentsSearchResultPage.getListPaymentsTitles(), hasSameItemsAsList(Arrays.asList(
                "Yota",
                "Yota - По номеру счета"
        )));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(SEARCH)
    @Description("Проверка перехода сразу на страницу платежа, если результат поиска - единственный платеж")
    @Test
    public void redirectToPaymentFormIfSearchResultIsSingleItem() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        WebDriver driver = getDriver();
        driver.get(CONFIG.getMainPageUrl());
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        paymentsPage.searchForPayment("гудлайн");
        PaymentPage paymentPage = new PaymentPage(driver);
        assertThat("Неправильное название страницы платежа", driver.findElement(By.className("small-cell-word-wrap")).getText(), is("Гудлайн"));
        driver.findElement(By.className("small-cell-word-wrap")).click();
        assertThat("Неправильное описание страницы платежа", paymentPage.getMenuDescription(), is("ООО «Ворлд Трэвел Телеком»"));
    }


    @Features(DESKTOP_FEATURE)
    @Stories(SEARCH)
    @Description("Проверка поискового ответа, если по запросу ничего не найдено.")
    @Test
    public void testEmptySearchResultPage() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        WebDriver driver = getDriver();
        driver.get(CONFIG.getMainPageUrl());
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        paymentsPage.searchForPayment("1234567890");
        PaymentsSearchResultPage paymentsSearchResultPage = new PaymentsSearchResultPage(driver);
        assertThat("Не правильный текст поискового ответа",
                paymentsSearchResultPage.getAnswerMessage(), is("По запросу 1234567890 ничего не найдено."));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(SEARCH)
    @Description("Ввод части уникального ОПИСАНИЯ платежа, проверка перехода к форме оплаты")
    @Test
    public void searchIsPerformedWithinDescriptionToo() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        WebDriver driver = getDriver();
        driver.get(CONFIG.getMainPageUrl());
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        paymentsPage.searchForPayment("ворлд трэвел теле");
        PaymentPage paymentPage = new PaymentPage(driver);
        assertThat("Неправильное название страницы платежа", driver.findElement(By.className("small-cell-word-wrap")).getText(), is("Гудлайн"));
        driver.findElement(By.className("small-cell-word-wrap")).click();
        assertThat("Неправильное описание страницы платежа", paymentPage.getMenuDescription(), is("ООО «Ворлд Трэвел Телеком»"));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(SEARCH)
    @Description("Поиск уникального платежа из категории только по этой категории. В другой категории присутствуют платежи, удовлетворяющие критерию поиска. Проверка перехода сразу к форме оплаты.")
    @Test
    public void searchFromCategoryOnlyWithinTheCategory() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        WebDriver driver = getDriver();
        driver.get(CONFIG.getMainPageUrl());
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        paymentsPage.openRootCategoryByTitle("Интернет и ТВ");
        CategoryPage categoryPage = new CategoryPage(driver);
        categoryPage.searchInThisCategory("билайн");
        PaymentPage paymentPage = new PaymentPage(driver);
        assertThat("Неправильное название страницы платежа",
                driver.findElement(By.className("small-cell-word-wrap")).getText(), is("Домашний Интернет Билайн"));
        driver.findElement(By.className("small-cell-word-wrap")).click();
        assertThat("Неправильное описание страницы платежа",
                paymentPage.getMenuDescription(), is("ОАО «Вымпел-Коммуникации»"));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(SEARCH)
    @Description("Поиск из категории по всем категориям. Проверка списка результатов поиска")
    @Test
    public void searchFromCategoryWithinAllPaymentTree() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        WebDriver driver = getDriver();
        driver.get(CONFIG.getMainPageUrl());
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        paymentsPage.openRootCategoryByTitle("Благотворительность");
        CategoryPage categoryPage = new CategoryPage(driver);
        categoryPage.searchInAllCategories("yota");
        PaymentsSearchResultPage paymentsSearchResultPage = new PaymentsSearchResultPage(driver);
        assertThat(paymentsSearchResultPage.getListPaymentsTitles(), hasSameItemsAsList(Arrays.asList(
                "Yota",
                "Yota - По номеру счета"
        )));
    }

}
