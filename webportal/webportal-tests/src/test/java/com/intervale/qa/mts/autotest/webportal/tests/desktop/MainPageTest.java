package com.intervale.qa.mts.autotest.webportal.tests.desktop;

import com.intervale.qa.mts.autotest.webportal.Profiles;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.PaymentsPage;
import com.intervale.qa.mts.autotest.webportal.tests.AbstractSeleniumTest;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.Arrays;
import java.util.List;

import static com.intervale.qa.mts.autotest.webportal.StoriesList.MAIN_PAGE;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assume.assumeThat;
import static ru.yandex.qatools.matchers.collection.HasSameItemsAsListMatcher.hasSameItemsAsList;
import static com.intervale.qa.mts.autotest.webportal.FeatureList.*;

/**
 * Created by dchahovsky.
 */
public class MainPageTest extends AbstractSeleniumTest {



    @Features(DESKTOP_FEATURE)
    @Stories(MAIN_PAGE)
    @Description("Проверка заголовков главной страницы и блока новостей")
    @Test
    public void mainPageContainsNews() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMainPageUrl());
        PaymentsPage page = new PaymentsPage(getDriver());
        assumeThat("Неверный заголовок на заглавной странице", page.getPageHeader(), is("ОПЛАТА ТОВАРОВ И УСЛУГ"));
        assertThat("Неверный заголовок блока новостей на заглавной странице", page.getNewsHeader(), is("ПОСЛЕДНИЕ НОВОСТИ"));
        assertThat("Не найдены новости на заглавной странице", page.getNews(), not(empty()));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(MAIN_PAGE)
    @Description("Проверка наличия всех необходимых категорий")
    @Ignore
    @Test
    public void mainPageContainsCertainCategories() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMainPageUrl());
        PaymentsPage page = new PaymentsPage(getDriver());
        List<String> rootCategories = page.listRootCategories();
        assertThat("Не найдена главные категории платежей", rootCategories, not(empty()));
        assertThat(rootCategories, hasSameItemsAsList(Arrays.asList(
                "МОБИЛЬНЫЙ ТЕЛЕФОН",
                "ИНТЕРНЕТ И ТВ",
                "КВАРТПЛАТА",
                "ПОГАШЕНИЕ КРЕДИТОВ",
                "ДЕНЕЖНЫЕ ПЕРЕВОДЫ",
                "СВЯЗЬ",
                "ЭЛЕКТРОННЫЕ ДЕНЬГИ",
                "БЛАГОТВОРИТЕЛЬНОСТЬ",
                "ТРАНСПОРТ",
                "РАЗНОЕ"
        )));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(MAIN_PAGE)
    @Description("Проверка наличия трех платежей в каждой категории")
    @Test
    public void everyRootCategoryHasAtMostThreeChildElementsAndAtLeastOne() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMainPageUrl());
        PaymentsPage page = new PaymentsPage(getDriver());
        List<String> rootCategories = page.listRootCategories();
        /*
        for (String rootCategoryTitle : rootCategories) {
            List<String> paymentInCategory = page.listPaymentTitlesInRootCategory(rootCategoryTitle);
            assertThat("Every root category on main page should have at least one item", paymentInCategory, not(empty()));
            assertThat("Not more than 3 items can be in each category on main page", paymentInCategory.size(), lessThanOrEqualTo(3));
        }
        */
        // костыль
        List<WebElement> tabs = getDriver().findElements(By.className("grid-in-table"));
        assertThat("Не совпадает количество таблиц и количество категорий (внутренняя ошибка)", tabs.size(), is(rootCategories.size()));
        for (WebElement tab : tabs) {
            int count = tab.findElements(By.className("menu-title-wrap")).size();
            assertThat("Каждая рутовая категория должна содержать от 1 до 3 вложенных элементов", count, allOf(greaterThanOrEqualTo(1), lessThanOrEqualTo(3)));
        }
    }

    @Features(DESKTOP_FEATURE)
    @Stories(MAIN_PAGE)
    @Description("Проверка наличия ссылки на все платежи на главной странице")
    @Test
    public void mainPageContainsLinkToAllPaymentsPage() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMainPageUrl());
        PaymentsPage page = new PaymentsPage(getDriver());
        WebElement tt = page.getAllPaymentsLink();
        assertThat(tt.getText(), is("ПОЛНЫЙ СПИСОК\nТОВАРОВ И УСЛУГ"));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(MAIN_PAGE)
    @Description("Проверка ссылки на мобильную версию портала")
    @Test
    public void mainPageContainsLinkToMobileSiteVersion() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMainPageUrl());
        PaymentsPage page = new PaymentsPage(getDriver());
        assertThat("Неверная ссылка на мобильную версию", page.getLinkToMobileSiteVersion().getText(), is("Мобильная версия сайта"));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(MAIN_PAGE)
    @Description("Проверка наличия блока поиска на главной странице")
    @Test
    public void searchBlockAtTopIsPresent() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMainPageUrl());
        assertThat(getDriver().findElement(By.className("serv-search")).findElement(By.tagName("label")).getText(), is("Поиск платежа:"));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(MAIN_PAGE)
    @Description("Проверка наличия блока выбора региона на главной странице и списка регионов")
    @Test
    public void regionChooseBlockIsPresent() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMainPageUrl());
        PaymentsPage page = new PaymentsPage(getDriver());
        assertThat(getDriver().findElement(By.className("region")).findElement(By.tagName("label")).getText(), is("Регион:"));
        assertThat(page.listAvailableRegions(), hasSameItemsAsList(Arrays.asList(
                "Москва и Подмосковье",
                "Санкт-Петербург, Ленинградская область",
                "Санкт-Петербург, Ленинградская область",
                "Адыгея Республика - Майкоп",
                "Алтай Республика - Горно-Алтайск",
                "Алтайский Край - Барнаул",
                "Амурская область - Благовещенск",
                "Архангельская область",
                "Астраханская область",
                "Башкортостан Республика - Уфа",
                "Белгородская область",
                "Брянская область",
                "Бурятия Республика - Улан-Удэ",
                "Владимирская область",
                "Волгоградская область",
                "Вологодская область",
                "Воронежская область",
                "Дагестан Республика - Махачкала",
                "Еврейская АО - Биробиджан",
                "Забайкальский Край - Чита",
                "Ивановская область",
                "Ингушетия Республика - Магас - Назрань",
                "Иркутская область",
                "Кабардино-Балкарская Республика - Нальчик",
                "Калининградская область",
                "Калмыкия Республика - Элиста",
                "Калужская область",
                "Камчатский Край - Петропавловск-Камчатский",
                "Карачаево-Черкесская Республика - Черкесск",
                "Карелия Республика - Петрозаводск",
                "Кемеровская область",
                "Кировская область",
                "Коми Республика - Сыктывкар",
                "Костромская область",
                "Краснодарский Край - Краснодар",
                "Красноярский Край",
                "Курганская область",
                "Курская область",
                "Липецкая область",
                "Магаданская область",
                "Марий Эл Республика - Йошкар-Ола",
                "Мордовия Республика - Саранск",
                "Мурманская область",
                "Нижегородская область - Нижний Новгород",
                "Новгородская область - Великий Новгород",
                "Новосибирская область",
                "Омская область",
                "Оренбургская область",
                "Орловская область",
                "Пензенская область",
                "Пермский Край",
                "Приморский Край - Владивосток",
                "Псковская область",
                "Ростовская область - Ростов-На-Дону",
                "Рязанская область",
                "Самарская область",
                "Саратовская область",
                "Саха (Якутия) Республика - Якутск",
                "Сахалинская область - Южно-Сахалинск",
                "Свердловская область - Екатеринбург",
                "Северная Осетия-Алания Республика - Владикавказ",
                "Смоленская область",
                "Ставропольский Край",
                "Тамбовская область",
                "Татарстан Республика - Казань",
                "Тверская область",
                "Томская область",
                "Тульская область",
                "Тыва Республика - Кызыл",
                "Тюменская область",
                "Удмуртская Республика - Ижевск",
                "Ульяновская область",
                "Хабаровский Край",
                "Хакасия Республика - Абакан",
                "Ханты-Мансийский АО - Югра - Ханты-Мансийск",
                "Челябинская область",
                "Чеченская Республика - Грозный",
                "Чувашская Республика - Чебоксары",
                "Чукотский АО - Анадырь",
                "Ямало-Ненецкий АО - Салехард",
                "Ярославская область"
        )));
    }

}
