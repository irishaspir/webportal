package com.intervale.qa.mts.autotest.webportal.tests.desktop;

import com.intervale.qa.mts.autotest.webportal.Profiles;
import com.intervale.qa.mts.autotest.webportal.selenium.ACSPage;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.PaymentPage;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.PaymentResultPage;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.PaymentResultPrintPage;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.PaymentsPage;
import com.intervale.qa.mts.autotest.webportal.tests.AbstractSeleniumTest;
import com.intervale.qa.toolbox.util.card.Card;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static com.intervale.qa.mts.autotest.webportal.FeatureList.*;
import static com.intervale.qa.mts.autotest.webportal.StoriesList.*;

/**
 * Created by dchahovsky.
 */
public class AnonymousCardPaymentTest extends AbstractSeleniumTest {

    @Features(DESKTOP_FEATURE)
    @Stories(ANONYMOUS_CARD_PAYMENT)
    @Description("Анонимный платеж по анонимной карте без 3ds, проверка всех полей на странице результата")
    @Test //https://jiraf.intervale.ru/browse/WEBMTS-362
    public void anonymousPaymentFromAnonymousNon3dsCardAndPaymentResultPageTest() {
        /* провести анонимный платеж по анонимной карте без 3ds */
        /* проверить все поля на странице результата */
        WebDriver driver = getDriver();
        driver.get(CONFIG.getMainPageUrl());
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        paymentsPage.openMenuItem("Автовеб - 1");
        PaymentPage paymentPage = new PaymentPage(driver);
        paymentPage.getInputParamFieldByName("param1").sendKeys("9104451325");
        paymentPage.getInputParamFieldByName("amount").sendKeys("10");
        paymentPage.getUnregisteredCardRadioButton().click();
        paymentPage.getPanInputField().click();
        Card card = VISA_NON_3DS_GENERATOR.generate();
        paymentPage.getPanInputField().sendKeys(card.getPan());
        paymentPage.selectCardExpYear("2020");
        paymentPage.getCardHolderNameInputField().click();
        paymentPage.getCardHolderNameInputField().sendKeys("anonymous");
        paymentPage.getCvvInputField().click();
        paymentPage.getCvvInputField().sendKeys("123");
        paymentPage.submit();
        assertThat("Платеж должен завершиться успешно", waitAndGetPaymentResult(), is("Выполнено"));
        PaymentResultPage paymentResultPage = new PaymentResultPage(driver);
        assertThat("Неверно указан источник", paymentResultPage.getPaymentSource(), is("Карта"));
        assertThat("Неверно указана оплачиваемая услуга", paymentResultPage.getPaidService(), is("Автовеб - 1"));
        assertThat("Неверно указан получатель", paymentResultPage.getPaymentRecipient(), is("ЗАО «Получатель платежа»"));
        assertThat("Неверно указана сумма платежа", paymentResultPage.getParamById("amount"), is("10 руб."));
        assertThat("Неверно указан номер телефона", paymentResultPage.getParamById("param1"), is("9104451325"));
        assertThat("Неверно указана сумма с комиссией", paymentResultPage.getAmountWithCommission(), is("20 руб."));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(ANONYMOUS_CARD_PAYMENT)
    @Description("Анонимный платеж по анонимной карте 3ds, проверка всех полей на странице результата")
    @Test
    public void anonymousPaymentFromAnonymous3dsCard() {
        /* провести анонимный платеж с анонимной 3ds карты */
        WebDriver driver = getDriver();
        driver.get(CONFIG.getMainPageUrl());
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        paymentsPage.openMenuItem("Автовеб - 1");
        PaymentPage paymentPage = new PaymentPage(driver);
        paymentPage.getInputParamFieldByName("param1").sendKeys("9104451325");
        paymentPage.getInputParamFieldByName("amount").sendKeys("10");
        paymentPage.getUnregisteredCardRadioButton().click();
        paymentPage.getPanInputField().click();
        Card card = VISA_3DS_GENERATOR.generate();
        paymentPage.getPanInputField().sendKeys(card.getPan());
        paymentPage.selectCardExpYear("2020");
        paymentPage.getCardHolderNameInputField().click();
        paymentPage.getCardHolderNameInputField().sendKeys("anonymous");
        paymentPage.getCvvInputField().click();
        paymentPage.getCvvInputField().sendKeys("123");
        paymentPage.submit();
        waitForDifferentCurrentUrlStartsWith(CONFIG.getBaseUrl());
        assertThat("Должен осуществиться переход на ACS", getDriver().getTitle(), is("Basic ACS"));
        ACSPage acsPage = new ACSPage(getDriver());
        acsPage.acsAccept();
        assertThat("Платеж должен завершиться успешно", waitAndGetPaymentResult(), is("Выполнено"));
        PaymentResultPage paymentResultPage = new PaymentResultPage(driver);
        assertThat("Неверно указан источник", paymentResultPage.getPaymentSource(), is("Карта"));
        assertThat("Неверно указана оплачиваемая услуга", paymentResultPage.getPaidService(), is("Автовеб - 1"));
        assertThat("Неверно указан получатель", paymentResultPage.getPaymentRecipient(), is("ЗАО «Получатель платежа»"));
        assertThat("Неверно указана сумма платежа", paymentResultPage.getParamById("amount"), is("10 руб."));
        assertThat("Неверно указан номер телефона", paymentResultPage.getParamById("param1"), is("9104451325"));
        assertThat("Неверно указана сумма с комиссией", paymentResultPage.getAmountWithCommission(), is("20 руб."));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(ANONYMOUS_CARD_PAYMENT)
    @Description("Платеж от имени клиента с анонимной карты без 3ds, проверка всех полей на странице результата")
    @Test
    public void paymentFromAnonymousNon3dsCardAndSuccessfulResultPageTest() {
        /* провести платеж от имени клиента с анонимной карты без 3ds */
        /* проверить все поля на странице результата */
        WebDriver driver = getDriver();
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        driver.get(CONFIG.getMainPageUrl());
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        paymentsPage.openMenuItem("Автовеб - 1");
        PaymentPage paymentPage = new PaymentPage(driver);
        paymentPage.getInputParamFieldByName("param1").sendKeys("9104451325");
        paymentPage.getInputParamFieldByName("amount").sendKeys("10");
        paymentPage.getUnregisteredCardRadioButton().click();
        paymentPage.getPanInputField().click();
        Card card = VISA_NON_3DS_GENERATOR.generate();
        paymentPage.getPanInputField().sendKeys(card.getPan());
        paymentPage.selectCardExpYear("2020");
        paymentPage.getCardHolderNameInputField().click();
        paymentPage.getCardHolderNameInputField().sendKeys("anonymous");
        paymentPage.getCvvInputField().click();
        paymentPage.getCvvInputField().sendKeys("123");
        paymentPage.submit();
        assertThat("Платеж должен завершиться успешно", waitAndGetPaymentResult(), is("Выполнено"));
        PaymentResultPage paymentResultPage = new PaymentResultPage(driver);
        assertThat("Неверно указано место формирования платежа", paymentResultPage.getPaymentFormedBy(), is("На сайте «Легкий платеж»"));
        assertThat("Неверно указан источник", paymentResultPage.getPaymentSource(), is("Карта"));
        assertThat("Неверно указана оплачиваемая услуга", paymentResultPage.getPaidService(), is("Автовеб - 1"));
        assertThat("Неверно указан получатель", paymentResultPage.getPaymentRecipient(), is("ЗАО «Получатель платежа»"));
        assertThat("Неверно указана сумма платежа", paymentResultPage.getParamById("amount"), is("10 руб."));
        assertThat("Неверно указан номер телефона", paymentResultPage.getParamById("param1"), is("9104451325"));
        assertThat("Неверно указана сумма с комиссией", paymentResultPage.getAmountWithCommission(), is("20 руб."));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(ANONYMOUS_CARD_PAYMENT)
    @Description("Платеж от имени клиента с анонимной 3ds карты c Acs Reject , проверка всех полей на странице результата")
    @Test
    public void paymentFromAnonymous3dsCardWithAcsReject() {
        WebDriver driver = getDriver();
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        driver.get(CONFIG.getMainPageUrl());
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        paymentsPage.openMenuItem("Автовеб - 1");
        PaymentPage paymentPage = new PaymentPage(driver);
        paymentPage.getInputParamFieldByName("param1").sendKeys("9104451325");
        paymentPage.getInputParamFieldByName("amount").sendKeys("10");
        paymentPage.getUnregisteredCardRadioButton().click();
        paymentPage.getPanInputField().click();
        Card card = VISA_3DS_GENERATOR.generate();
        paymentPage.getPanInputField().sendKeys(card.getPan());
        paymentPage.selectCardExpYear("2020");
        paymentPage.getCardHolderNameInputField().click();
        paymentPage.getCardHolderNameInputField().sendKeys("anonymous");
        paymentPage.getCvvInputField().click();
        paymentPage.getCvvInputField().sendKeys("123");
        paymentPage.submit();
        waitForDifferentCurrentUrlStartsWith(CONFIG.getBaseUrl());
        assertThat("Должен осуществиться переход на ACS", getDriver().getTitle(), is("Basic ACS"));
        ACSPage acsPage = new ACSPage(getDriver());
        acsPage.acsReject();
        assertThat("Платеж не должен завершиться успешно", waitAndGetPaymentResult(), is("Не выполнено"));
        PaymentResultPage paymentResultPage = new PaymentResultPage(driver);
        assertThat("Неверно указано место формирования платежа", paymentResultPage.getPaymentFormedBy(), is("На сайте «Легкий платеж»"));
        assertThat("Неверно указан источник", paymentResultPage.getPaymentSource(), is("Карта"));
        assertThat("Неверно указана оплачиваемая услуга", paymentResultPage.getPaidService(), is("Автовеб - 1"));
        assertThat("Неверно указан получатель", paymentResultPage.getPaymentRecipient(), is("ЗАО «Получатель платежа»"));
        assertThat("Неверно указана сумма платежа", paymentResultPage.getParamById("amount"), is("10 руб."));
        assertThat("Неверно указан номер телефона", paymentResultPage.getParamById("param1"), is("9104451325"));
        assertThat("Неверно указана сумма с комиссией", paymentResultPage.getAmountWithCommission(), is("20 руб."));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(ANONYMOUS_CARD_PAYMENT)
    @Description("проверка наличия и корректности ссылки на договор-оферту по б/к")
    @Test
    public void testLinkForCardOffer() {
        WebDriver driver = getDriver();
        driver.get(CONFIG.getMainPageUrl());
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        paymentsPage.openMenuItem("Автовеб - 1");
        PaymentPage paymentPage = new PaymentPage(driver);
        paymentPage.getInputParamFieldByName("param1").sendKeys("9104451325");
        paymentPage.getInputParamFieldByName("amount").sendKeys("10");
        paymentPage.getUnregisteredCardRadioButton().click();
        assertThat("Должна отображаться ссылка на договор-оферту", paymentPage.getOfferLink().isDisplayed(), is(true));
        String ref = paymentPage.getOfferLink().getAttribute("href");
        assertThat("Неверная ссылка на договор-оферту по карте", ref, is(CONFIG.getBaseUrl()+"/webportal/offer-card?id=5EE64B5FF271B0E66470828908BBBF98"));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(ANONYMOUS_CARD_PAYMENT)
    @Description("Проведение успешного платежа и проверка, что все поля на печатной форме соответствуют странице результата платежа")
    @Test
    public void printResultPageContainsAllFields() {
        WebDriver driver = getDriver();
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        driver.get(CONFIG.getMainPageUrl());
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        paymentsPage.openMenuItem("Автовеб - 1");
        PaymentPage paymentPage = new PaymentPage(driver);
        paymentPage.getInputParamFieldByName("param1").sendKeys("9104451325");
        paymentPage.getInputParamFieldByName("amount").sendKeys("10");
        paymentPage.getUnregisteredCardRadioButton().click();
        paymentPage.getPanInputField().click();
        Card card = VISA_NON_3DS_GENERATOR.generate();
        paymentPage.getPanInputField().sendKeys(card.getPan());
        paymentPage.selectCardExpYear("2020");
        paymentPage.getCardHolderNameInputField().click();
        paymentPage.getCardHolderNameInputField().sendKeys("anonymous");
        paymentPage.getCvvInputField().click();
        paymentPage.getCvvInputField().sendKeys("123");
        paymentPage.submit();
//        new WebDriverWait(getDriver(), 5).until(ExpectedConditions.presenceOfElementLocated(By.id("paymentData")));
        assertThat("Платеж должен завершиться успешно", waitAndGetPaymentResult(), is("Выполнено"));
        PaymentResultPage paymentResultPage = new PaymentResultPage(driver);

        String paymentFormedBy = paymentResultPage.getPaymentFormedBy();
        String paymentSource = paymentResultPage.getPaymentSource();
        String paidService = paymentResultPage.getPaidService();
        String paymentRecipient = paymentResultPage.getPaymentRecipient();
        String amount = paymentResultPage.getParamById("amount");
        String phoneNumber = paymentResultPage.getParamById("param1");
        String amountWithCommission = paymentResultPage.getAmountWithCommission();
        String date = paymentResultPage.getDate();
        String trxCode = paymentResultPage.getTrxCode();
        String bankTrxNumber = paymentResultPage.getBankTrxNumber();

        paymentResultPage.getPrintLink().click();
        PaymentResultPrintPage paymentResultPrintPage = new PaymentResultPrintPage(driver);

        assertThat("Неверно указано место формирования платежа", paymentResultPrintPage.getParamValueByName("Платеж сформирован:"), is(paymentFormedBy));
        assertThat("Неверно указан источник", paymentResultPrintPage.getParamValueByName("Источник:"), is(paymentSource));
        assertThat("Неверно указана оплачиваемая услуга", paymentResultPrintPage.getParamValueByName("Оплата услуг:"), is(paidService));
        assertThat("Неверно указан получатель", paymentResultPrintPage.getParamValueByName("Получатель:"), is(paymentRecipient));
        assertThat("Неверно указана сумма платежа", paymentResultPrintPage.getParamValueByName("Сумма платежа:"), is(amount));
        assertThat("Неверно указан номер телефона", paymentResultPrintPage.getParamValueByName("Параметр:"), is(phoneNumber));
        assertThat("Неверно указана сумма с комиссией", paymentResultPrintPage.getParamValueByName("Сумма с комиссией:"), is(amountWithCommission));
        assertThat("Неверно указана дата", paymentResultPrintPage.getParamValueByName("Дата:"), is(date));
        assertThat("Неверно указан код транзакции", paymentResultPrintPage.getParamValueByName("Код транзакции:"), is(trxCode));
        assertThat("Неверно указан номер банковской транзакции", paymentResultPrintPage.getParamValueByName("Номер банковской транзакции:"), is(bankTrxNumber));
    }
}
