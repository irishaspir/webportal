package com.intervale.qa.mts.autotest.webportal.tests.desktop;

import com.intervale.qa.mts.autotest.webportal.Profiles;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.PaymentPage;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.favorite.FavoritesPage;
import com.intervale.qa.mts.autotest.webportal.steps.FavouritesSteps;
import com.intervale.qa.mts.autotest.webportal.tests.AbstractSeleniumTest;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static com.intervale.qa.mts.autotest.webportal.StoriesList.MANAGE_FAVOURITES;
import static com.intervale.qa.mts.autotest.webportal.matchers.FavoritesPageMatcher.containsPayment;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static com.intervale.qa.mts.autotest.webportal.FeatureList.*;


/**
 * Created by dchahovsky.
 */
public class FavouritesManageTest extends AbstractSeleniumTest {

    @Features(DESKTOP_FEATURE)
    @Stories(MANAGE_FAVOURITES)
    @Description("Добавление, редактирование избранного и проверка измененных значений")
    @Test
    public void addEditAndOpenFavouritePayment() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        FavouritesSteps favSteps = new FavouritesSteps(getDriver());

        // adding to favorites
        String paymentName = String.valueOf(System.currentTimeMillis());
        List<String> treePath = Arrays.asList("autotest", "Автовеб - 1");
        favSteps.addPhonePaymentToFavourites(treePath, paymentName, "9152731056", "10");

        // check adding;
        new FavoritesPage(getDriver());
        getDriver().navigate().refresh(); // костыль для перезагрузки страницы. Иначе добавленый платеж не появляется в списке
        FavoritesPage favoritesPage = new FavoritesPage(getDriver());
        assertThat(favoritesPage, containsPayment(paymentName));

        // edit fav payment
        DateTimeFormatter dtf = DateTimeFormat.forPattern("ddMMyyyy-HH:mm:ss");
        String dateTime = dtf.print(new DateTime());
        String newTitle = dateTime+"_"+(new Random().nextInt(9000)+1000);
        String newPhone = "9152731354";
        String newAmount = "20";
        favSteps.editFavPayment(paymentName, newTitle, newPhone, newAmount);

        // check editing
        new FavoritesPage(getDriver());
        getDriver().navigate().refresh(); // костыль для перезагрузки страницы. Иначе отредактированный платеж не появляется в списке
        favoritesPage = new FavoritesPage(getDriver());
        assertThat("Измененный платеж " + newTitle + " должен быть в списке", favoritesPage.listFavoritePayments(), hasItem(newTitle));
        favoritesPage.getPaymentLine(newTitle).getDescriptionLink().click();
        PaymentPage paymentPage = new PaymentPage(getDriver());
        String paymentAmount = paymentPage.getInputParamFieldByName("amount").getAttribute("value");
        assertThat("Некорректная сумма на форме из избранного", paymentAmount, is(newAmount));
        String paymentPhone = paymentPage.getInputParamFieldByName("param1").getAttribute("value"); // if payment for beeline
//        String paymentPhone = paymentPage.getInputParamFieldByName("e_phone").getAttribute("value"); // if payment for mts
        assertThat("Некорректный номер телефона на форме из избранного '" + newPhone + "'", paymentPhone, is(newPhone));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(MANAGE_FAVOURITES)
    @Description("Добавление, переименование и удаление избранного")
    @Test //@Ignore("WEBMTS-219 (temp ignore until bugfixed)")
    public void addRenameAndRemoveFavouritePayment() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        FavouritesSteps favSteps = new FavouritesSteps(getDriver());

        // adding to favorites
        String paymentName = String.valueOf(System.currentTimeMillis());
        List<String> treePath = Arrays.asList("autotest", "Автовеб - 1");
        favSteps.addPhonePaymentToFavourites(treePath, paymentName, "9152731056", "10");

        // check adding;
        new FavoritesPage(getDriver());
        getDriver().navigate().refresh(); // костыль для перезагрузки страницы. Иначе добавленый платеж не появляется в списке
        FavoritesPage favoritesPage = new FavoritesPage(getDriver());
        assertThat(favoritesPage, containsPayment(paymentName));

        // rename fav payment
        DateTimeFormatter dtf = DateTimeFormat.forPattern("ddMMyyyy-HH:mm:ss");
        String dateTime = dtf.print(new DateTime());
        String newTitle = dateTime+"_"+(new Random().nextInt(9000)+1000);
        favSteps.editFavPayment(paymentName, newTitle, null, null);

        // check renaming
        new FavoritesPage(getDriver());
        getDriver().navigate().refresh(); // костыль для перезагрузки страницы. Иначе добавленый платеж не появляется в списке
        favoritesPage = new FavoritesPage(getDriver());
        assertThat(favoritesPage, containsPayment(newTitle));

        // delete fav payment
        favoritesPage.getPaymentLine(newTitle).getDeleteLink().click();
        favoritesPage.confirmDelete();

        //check deleting
        assertThat("Избранный платеж со старым именем " + paymentName + " не должен присутствовать в списке",
                favoritesPage.listFavoritePayments(), not(hasItem(paymentName)));
        assertThat("Избранный платеж с новым именем " + newTitle + " не должен присутствовать в списке",
                favoritesPage.listFavoritePayments(), not(hasItem(newTitle)));

    }

    @Features(DESKTOP_FEATURE)
    @Stories(MANAGE_FAVOURITES)
    @Description("Добавление избранного платежа с пустыми параметрами и проверка его наличия в списке")
    @Test
    public void addingToFavouritesWithBlankParamsIsOk() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        FavouritesSteps favSteps = new FavouritesSteps(getDriver());

        // adding to favorites
        String paymentName = String.valueOf(System.currentTimeMillis());
        List<String> treePath = Arrays.asList("autotest", "Автовеб - 1");
        favSteps.addPhonePaymentToFavourites(treePath, paymentName, null, null);

        // check adding;
        new FavoritesPage(getDriver());
        getDriver().navigate().refresh(); // костыль для перезагрузки страницы. Иначе добавленый платеж не появляется в списке
        FavoritesPage favoritesPage = new FavoritesPage(getDriver());
        assertThat(favoritesPage, containsPayment(paymentName));
    }

}
