package com.intervale.qa.mts.autotest.webportal.tests.desktop;

import com.intervale.qa.mts.autotest.webportal.Profiles;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.PaymentsPage;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.cards.CardsListPage;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.favorite.FavoritesPage;
import com.intervale.qa.mts.autotest.webportal.tests.AbstractSeleniumTest;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;

import static com.intervale.qa.mts.autotest.webportal.matchers.CardsListPageMatcher.containsActiveCard;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static com.intervale.qa.mts.autotest.webportal.FeatureList.*;

/**
 * Created by ashaporev
 */
public class PrepareTest extends AbstractSeleniumTest{


    @Features(PREPARE)
    @Test
    public void cardListShouldContainsActiveAutotestVisaNon3dsCard(){
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getCardsListUrl());
        CardsListPage cardsPage = new CardsListPage(getDriver());
        assertThat(cardsPage, containsActiveCard("autotest-visa-non-3ds"));
    }

    @Features(PREPARE)
    @Test
    public void cardListShouldContainsActiveAutotestVisa3dsCard(){
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getCardsListUrl());
        CardsListPage cardsPage = new CardsListPage(getDriver());
        assertThat(cardsPage, containsActiveCard("autotest-visa-3ds"));
    }

    @Features(PREPARE)
    @Test
    public void autowebPaymentShouldPresentOnMainPage(){
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMainPageUrl());
        PaymentsPage paymentsPage = new PaymentsPage(getDriver());
        assertThat("На главной странице нет платежа 'Автовеб - 1'", paymentsPage.containsMenuItem("Автовеб - 1"), is(true));
    }

    @Features(PREPARE)
    @Test
    public void favoritesPageShouldContainAtLeastOnePayment(){
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getFavoritesUrl());
        FavoritesPage favoritesPage = new FavoritesPage(getDriver());
        assertThat("В избранных должен быть как минимум один добавленный платеж", favoritesPage.getFavoritesList().size(), greaterThanOrEqualTo(1));
    }

}
