package com.intervale.qa.mts.autotest.webportal.tests.desktop;

import com.intervale.qa.mts.autotest.webportal.Profiles;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.PaymentPage;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.PaymentResultPage;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.PaymentResultPrintPage;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.PaymentsPage;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.favorite.FavoritesPage;
import com.intervale.qa.mts.autotest.webportal.tests.AbstractSeleniumTest;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import static com.intervale.qa.mts.autotest.webportal.FeatureList.DESKTOP_FEATURE;
import static com.intervale.qa.mts.autotest.webportal.StoriesList.ACCOUNT_PAYMENT;
import static com.intervale.qa.mts.autotest.webportal.StoriesList.MANAGE_FAVOURITES;
import static com.intervale.qa.mts.autotest.webportal.matchers.FavoritesPageMatcher.containsPayment;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertThat;

/**
 * Created by dchahovsky.
 */
public class AccountPaymentTest extends AbstractSeleniumTest {

    @Features(DESKTOP_FEATURE)
    @Stories(ACCOUNT_PAYMENT)
    @Description("Ввод символов вместо суммы оплаты. Проверка подсветки поля суммы и наличия текста ошибки на странице.")
    @Severity(value = SeverityLevel.CRITICAL)
    @Test
    public void invalidParameterIsHighlighted() {
        WebDriver driver = getDriver();
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        driver.get(CONFIG.getMainPageUrl());
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        paymentsPage.openMenuItem("Автовеб - 1");
        PaymentPage paymentPage = new PaymentPage(driver);
        paymentPage.getInputParamFieldByName("param1").sendKeys("9104451325");
        paymentPage.getInputParamFieldByName("amount").sendKeys("five");
        paymentPage.submit();
        assertThat("Поле ввода суммы должно быть подсвечено", paymentPage.isInputParamFieldHighlighted("amount"), is(true));
        String expectedErrorMessage = "Внимание! При заполнении формы были допущены ошибки. Проверьте, пожалуйста, значения параметров и повторите отправку данных.";
        assertThat("Должно отображаться сообщение об ошибке \""+expectedErrorMessage+"\"",
                paymentPage.isErrorMessagePresent(expectedErrorMessage), is(true));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(ACCOUNT_PAYMENT)
    @Description("Проверка всех полей формы результата успешного платежа")
    @Test
    public void successfulResultPageTest() {
        /* todo проверить все поля формы результата платежа */
        WebDriver driver = getDriver();
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        driver.get(CONFIG.getMainPageUrl());
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        paymentsPage.openMenuItem("Автовеб - 1");
        PaymentPage paymentPage = new PaymentPage(driver);
        paymentPage.getAccountRadioButton().click();
        paymentPage.getInputParamFieldByName("param1").sendKeys("9104451325");
        paymentPage.getInputParamFieldByName("amount").sendKeys("10");
        paymentPage.submit();
//        new WebDriverWait(getDriver(), 5).until(ExpectedConditions.presenceOfElementLocated(By.id("paymentData")));
        assertThat("Платеж должен завершиться успешно", waitAndGetPaymentResult(), is("Выполнено"));
        PaymentResultPage paymentResultPage = new PaymentResultPage(driver);
        assertThat("Должна отображаться ссылка 'Повторить платеж'", paymentResultPage.isRepeatLinkDisplayed(), is(true));
        assertThat("Должна отображаться ссылка отправки на e-mail", paymentResultPage.isSendToEmailLinkDisplayed(), is(true));
        assertThat("Должна отображаться ссылка распечатки квитанции", paymentResultPage.isPrintLinkDisplayed(), is(true));
        assertThat("Неверно указано место формирования платежа", paymentResultPage.getPaymentFormedBy(), is("На сайте «Легкий платеж»"));
        assertThat("Неверно указан источник", paymentResultPage.getPaymentSource(), is("Лицевой счет МТС"));
        assertThat("Неверно указана оплачиваемая услуга", paymentResultPage.getPaidService(), is("Автовеб - 1"));
        assertThat("Неверно указан получатель", paymentResultPage.getPaymentRecipient(), is("ЗАО «Получатель платежа»"));
        assertThat("Неверно указана сумма платежа", paymentResultPage.getParamById("amount"), is("10 руб."));
        assertThat("Неверно указан номер телефона", paymentResultPage.getParamById("param1"), is("9104451325"));
        assertThat("Неверно указана сумма с комиссией", paymentResultPage.getAmountWithCommission(), is("10 руб."));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(ACCOUNT_PAYMENT)
    @Description("Проверка всех полей формы результата неуспешного платежа")
    @Test
    public void failedResultPageTest() {
        /* проверить все поля на форме с неуспешным результатом платежа */
        /* дополнительно: проверить отсутствие ссылки "повторить платеж" */
        /* проверить наличие ссылки "отправить на электронный ящик" */
        /* проверить наличие ссылки "распечатать" */
        try {
            WebDriver driver = getDriver();
            setActiveProfile(Profiles.DEFAULT_PROFILE);
            driver.get(CONFIG.getMainPageUrl());
            PaymentsPage paymentsPage = new PaymentsPage(driver);
            paymentsPage.openMenuItem("Автовеб - 1");
            PaymentPage paymentPage = new PaymentPage(driver);
            paymentPage.getAccountRadioButton().click();
            paymentPage.getInputParamFieldByName("param1").sendKeys("9104451325");
            paymentPage.getInputParamFieldByName("amount").sendKeys("10");
            BANK.addCreditRC("25"); // <<<< configure emulator just before request
            paymentPage.submit();
            assertThat("Платеж не должен завершиться успешно", waitAndGetPaymentResult(), is("Не выполнено"));
            PaymentResultPage paymentResultPage = new PaymentResultPage(driver);
            assertThat("Должна отображаться ссылка 'Повторить платеж'", paymentResultPage.isRepeatLinkDisplayed(), is(true));
            assertThat("Должна отображаться ссылка отправки на e-mail", paymentResultPage.isSendToEmailLinkDisplayed(), is(true));
            assertThat("Должна отображаться ссылка распечатки квитанции", paymentResultPage.isPrintLinkDisplayed(), is(true));
            assertThat("Неверно указано место формирования платежа", paymentResultPage.getPaymentFormedBy(), is("На сайте «Легкий платеж»"));
            assertThat("Неверно указан источник", paymentResultPage.getPaymentSource(), is("Лицевой счет МТС"));
            assertThat("Неверно указана оплачиваемая услуга", paymentResultPage.getPaidService(), is("Автовеб - 1"));
            assertThat("Неверно указан получатель", paymentResultPage.getPaymentRecipient(), is("ЗАО «Получатель платежа»"));
            assertThat("Неверно указана сумма платежа", paymentResultPage.getParamById("amount"), is("10 руб."));
            assertThat("Неверно указан номер телефона", paymentResultPage.getParamById("param1"), is("9104451325"));
            assertThat("Неверно указана сумма с комиссией", paymentResultPage.getAmountWithCommission(), is("10 руб."));
        } finally {
            BANK.clearAllConfiguration();
        }
    }

    @Features(DESKTOP_FEATURE)
    @Stories(ACCOUNT_PAYMENT)
    @Description("Проверка наличия ссылки на договор-оферту при выборе платежа с л/с")
    @Test
    public void customOfferUrlOnAccountPayment() {
        WebDriver driver = getDriver();
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        driver.get(CONFIG.getMainPageUrl());
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        paymentsPage.openMenuItem("Автовеб - 1");
        PaymentPage paymentPage = new PaymentPage(driver);
        paymentPage.getAccountRadioButton().click();
        assertThat("Должна отображаться ссылка на договор-оферту", paymentPage.getOfferLink().isDisplayed(), is(true));
        String ref = paymentPage.getOfferLink().getAttribute("href");
        assertThat("Неверная ссылка на договор-оферту по карте", ref, startsWith(CONFIG.getBaseUrl()+"/webportal/offer-account"));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(ACCOUNT_PAYMENT)
    @Description("Проверка, что на странице печати те же параметры, что и на стр. результата")
    @Test
    public void testPrintResultPageContainsSameParams() {
        WebDriver driver = getDriver();
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        driver.get(CONFIG.getMainPageUrl());
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        paymentsPage.openMenuItem("Автовеб - 1");
        PaymentPage paymentPage = new PaymentPage(driver);
        paymentPage.getAccountRadioButton().click();
        paymentPage.getInputParamFieldByName("param1").sendKeys("9104451325");
        paymentPage.getInputParamFieldByName("amount").sendKeys("10");
        paymentPage.submit();
//        new WebDriverWait(getDriver(), 5).until(ExpectedConditions.presenceOfElementLocated(By.id("paymentData")));
        assertThat("Платеж должен завершиться успешно", waitAndGetPaymentResult(), is("Выполнено"));
        PaymentResultPage paymentResultPage = new PaymentResultPage(driver);

        String paymentFormedBy = paymentResultPage.getPaymentFormedBy();
        String paymentSource = paymentResultPage.getPaymentSource();
        String paidService = paymentResultPage.getPaidService();
        String paymentRecipient = paymentResultPage.getPaymentRecipient();
        String amount = paymentResultPage.getParamById("amount");
        String phoneNumber = paymentResultPage.getParamById("param1");
        String amountWithCommission = paymentResultPage.getAmountWithCommission();
        String date = paymentResultPage.getDate();
        String trxCode = paymentResultPage.getTrxCode();
        String bank = paymentResultPage.getBank();
        String bankTrxNumber = paymentResultPage.getBankTrxNumber();

        paymentResultPage.getPrintLink().click();
        PaymentResultPrintPage paymentResultPrintPage = new PaymentResultPrintPage(driver);

        assertThat("Неверно указано место формирования платежа", paymentResultPrintPage.getParamValueByName("Платеж сформирован:"), is(paymentFormedBy));
        assertThat("Неверно указан источник", paymentResultPrintPage.getParamValueByName("Источник:"), is(paymentSource));
        assertThat("Неверно указана оплачиваемая услуга", paymentResultPrintPage.getParamValueByName("Оплата услуг:"), is(paidService));
        assertThat("Неверно указан получатель", paymentResultPrintPage.getParamValueByName("Получатель:"), is(paymentRecipient));
        assertThat("Неверно указана сумма платежа", paymentResultPrintPage.getParamValueByName("Сумма платежа:"), is(amount));
        assertThat("Неверно указан номер телефона", paymentResultPrintPage.getParamValueByName("Параметр:"), is(phoneNumber));
        assertThat("Неверно указана сумма с комиссией", paymentResultPrintPage.getParamValueByName("Сумма с комиссией:"), is(amountWithCommission));
        assertThat("Неверно указана дата", paymentResultPrintPage.getParamValueByName("Дата:"), is(date));
        assertThat("Неверно указан код транзакции", paymentResultPrintPage.getParamValueByName("Код транзакции:"), is(trxCode));
        assertThat("Неверно указан расчетный банк", paymentResultPrintPage.getParamValueByName("Расчетный банк:"), is(bank));
        assertThat("Неверно указан номер банковской транзакции", paymentResultPrintPage.getParamValueByName("Номер банковской транзакции:"), is(bankTrxNumber));
    }

    @Features(DESKTOP_FEATURE)
    @Stories({ACCOUNT_PAYMENT, MANAGE_FAVOURITES})
    @Description("Добавление в избранное из результата платежа, проверка параметров сохраненного избранного.")
    @Test
    public void addToFavouritesFromPaymentResultPage() {
        WebDriver driver = getDriver();
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        driver.get(CONFIG.getMainPageUrl());
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        paymentsPage.openMenuItem("Автовеб - 1");
        PaymentPage paymentPage = new PaymentPage(driver);
        paymentPage.getAccountRadioButton().click();
        String phone = "9104451325";
        paymentPage.getInputParamFieldByName("param1").sendKeys(phone);
        String amount = "10";
        paymentPage.getInputParamFieldByName("amount").sendKeys(amount);
        paymentPage.submit();
        assertThat("Платеж должен завершиться успешно", waitAndGetPaymentResult(), is("Выполнено"));
        PaymentResultPage paymentResultPage = new PaymentResultPage(driver);

        // add to favorites
        paymentResultPage.getAddToFavoriteTitleInputField().click();
        String favTitle = String.valueOf(System.currentTimeMillis());
        paymentResultPage.getAddToFavoriteTitleInputField().sendKeys(favTitle);
        paymentResultPage.getSubmitAddToFavorites().click();

        // check adding to favorites
        FavoritesPage favoritesPage = new FavoritesPage(getDriver());
        assertThat(favoritesPage, containsPayment(favTitle));

        // open fav payment and check parameters
        favoritesPage.getPaymentLine(favTitle).getDescriptionLink().click();
        paymentPage = new PaymentPage(getDriver());
        String paymentSourceName = paymentPage.getPaymentSourceName();
        assertThat("Некорректнаый источник платежа", paymentSourceName, is("MOBILE"));
        String paymentAmount = paymentPage.getInputParamFieldByName("amount").getAttribute("value");
        assertThat("Некорректная сумма на форме из избранного", paymentAmount, is(amount));
        String paymentPhone = paymentPage.getInputParamFieldByName("param1").getAttribute("value");
        assertThat("Некорректный номер телефона на форме из избранного", paymentPhone, is(phone));
    }

}
