package com.intervale.qa.mts.autotest.webportal.tests.desktop;

import com.intervale.qa.mts.autotest.webportal.Profiles;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.cards.CardLine;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.cards.CardsListPage;
import com.intervale.qa.mts.autotest.webportal.tests.AbstractSeleniumTest;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.List;

import static com.intervale.qa.mts.autotest.webportal.FeatureList.DESKTOP_FEATURE;
import static com.intervale.qa.mts.autotest.webportal.StoriesList.*;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assume.assumeThat;

/**
 * Created by dchahovsky.
 */
public class CardListPageTest extends AbstractSeleniumTest {

    @Features(DESKTOP_FEATURE)
    @Stories(MANAGE_CARDS)
    @Description("Проверка заголовка страницы")
    @Test
    public void cardListPageHasCertainTitle() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getCardsListUrl());
        CardsListPage cardsListPage = new CardsListPage(getDriver());
        assertThat("Некорректный заголовок на странице истории", cardsListPage.getPageTitle(), is("МОИ КАРТЫ"));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(MANAGE_CARDS)
    @Description("Проверка всех необходимых данных, ссылок по картам")
    @Test
    public void rowsShouldHaveAllNecessaryData(){
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getCardsListUrl());
        CardsListPage cardsListPage = new CardsListPage(getDriver());
        List<CardLine> cardLineList = cardsListPage.getCardList();
        assumeThat("Список карт пуст", cardLineList, not(empty()));
        for (CardLine cardLine : cardLineList){
            assertThat("Пиктограмма карты отсутствует", cardLine.getCardTypeImage(), not(nullValue()));
            assertThat("Нет имени карты", cardLine.getCardName(), not(nullValue()));
            assertThat("Нет PAN карты", cardLine.getPan(), not(nullValue()));
            assertThat("Нет статуса карты", cardLine.getStateText(), not(nullValue()));
            assertThat("Нет ссылки на переименование карты", cardLine.getEditLink(), not(nullValue()));
            if ("Активна".equals(cardLine.getStateText())){
                assertThat("Для активной карты "+cardLine.getCardName()+" неактивная кнопка переименования",
                        cardLine.getEditLink().getAttribute("class"), not("rename_inactive"));
            } else {
                assertThat("Для неактивной карты "+cardLine.getCardName()+" активна кнопка переименования",
                        cardLine.getEditLink().getAttribute("class"), is("rename_inactive"));
            }
            assertThat("Нет ссылки для удаления карты", cardLine.getDeleteLink(), not(nullValue()));
        }
    }

    @Features(DESKTOP_FEATURE)
    @Stories(MANAGE_CARDS)
    @Description("Проверка наличия ссылки на виртуальную карту")
    @Test
    public void virtualCardLinkIsPresent() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getCardsListUrl());
        CardsListPage cardsListPage = new CardsListPage(getDriver());
        assertThat("Должна отображаться ссылка на виртуальную карту", cardsListPage.getVirtualCardLink().isDisplayed(), is(true));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(MANAGE_CARDS)
    @Description("Проверка отображения для всех статусов карт и наличия необходимых кнопок. Проводится на специальном профиле 9859900701")
    @Test
    public void testAllCardStatesAndLinksOnCardListPage() {
        /* Необходимо для каждого статуса проверить как он отображается и какие кнопки доступны */
        WebDriver driver = getDriver();
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        driver.get(CONFIG.getCardsListUrl());
        CardsListPage cardsListPage = new CardsListPage(driver);
        List<CardLine> cardLineList = cardsListPage.getCardList();
        assumeThat("Список карт пуст", cardLineList, not(empty()));
        for (CardLine cardLine : cardLineList){
            assertThat("Пиктограмма карты отсутствует", cardLine.getCardTypeImage(), not(nullValue()));
            assertThat("Нет имени карты", cardLine.getCardName(), not(nullValue()));
            assertThat("Нет PAN карты", cardLine.getPan(), not(nullValue()));
            assertThat("Нет статуса карты", cardLine.getStateText(), not(nullValue()));
            assertThat("Нет ссылки на переименование карты", cardLine.getEditLink(), not(nullValue()));
            if ("Активна".equals(cardLine.getStateText())){
                assertThat("Для активной карты "+cardLine.getCardName()+" не активна кнопка переименования",
                        cardLine.getEditLink().getAttribute("class"), not("rename_inactive"));
            } else {
                assertThat("Для неактивной карты "+cardLine.getCardName()+" активна кнопка переименования",
                        cardLine.getEditLink().getAttribute("class"), is("rename_inactive"));
            }

            if ("Регистрация не подтверждена".equals(cardLine.getStateText())){
                assertThat("Для карты " + cardLine.getCardName() + " не активна кнопка продолжения регистрации",
                        cardLine.getStateElement().getAttribute("class"), is("aWaitingCard"));
                assertThat("Для карты " + cardLine.getCardName() + " ссылка статуса должна вести на страницу продолжения регистрации",
                        cardLine.getStateElement().getAttribute("href"),
                        startsWith(CONFIG.getBaseUrl()+"/webportal/cards/real/register?cardId="));
            } else if (!"Активна".equals(cardLine.getStateText())) {
                assertThat("Для карты "+cardLine.getCardName()+" активная кнопка продолжения регистрации",
                        cardLine.getStateElement().getAttribute("class"), is("rename_inactive"));
            }
            assertThat("Нет ссылки для удаления карты", cardLine.getDeleteLink(), not(nullValue()));
        }
    }
}
