package com.intervale.qa.mts.autotest.webportal.tests.mobile;

import com.intervale.qa.mts.autotest.webportal.Profiles;
import com.intervale.qa.mts.autotest.webportal.selenium.mobile.card.MobileCardLine;
import com.intervale.qa.mts.autotest.webportal.selenium.mobile.card.MobileCardsListPage;
import com.intervale.qa.mts.autotest.webportal.tests.AbstractSeleniumTest;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.List;

import static com.intervale.qa.mts.autotest.webportal.FeatureList.*;
import static com.intervale.qa.mts.autotest.webportal.StoriesList.MANAGE_CARDS;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assume.assumeThat;

/**
 * Created by dchahovsky.
 */
public class MobileCardListPageTest extends AbstractSeleniumTest {

    @Features(MOBILE_FEATURE)
    @Stories(MANAGE_CARDS)
    @Test
    public void mobileCardListPageTestHasCertainTitle() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobileCardListUrl());
        MobileCardsListPage mobileCardsListPage = new MobileCardsListPage(getDriver());
        assertThat("Некорректный заголовок на странице истории", mobileCardsListPage.getPageTitleH(), is("Мои карты"));
    }

    @Features(MOBILE_FEATURE)
    @Stories(MANAGE_CARDS)
    @Test
    public void virtualCardLinkIsPresent() {
        /* проверить наличие и расположение ссылки на "виртуальную карту МТС-Деньги" */
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobileCardListUrl());
        MobileCardsListPage mobileCardsListPage = new MobileCardsListPage(getDriver());
        assertThat("Должна отображаться ссылка на виртуальную карту", mobileCardsListPage.getVirtualCardLink().isDisplayed(), is(true));
    }

    @Features(MOBILE_FEATURE)
    @Stories(MANAGE_CARDS)
    @Test
    public void testAllCardStatesAndLinksOnCardListPage() {
        /* Необходимо для каждого статуса проверить как он отображается и какие кнопки доступны */
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobileCardListUrl());
        MobileCardsListPage mobileCardsListPage = new MobileCardsListPage(getDriver());
        List<MobileCardLine> mobileCardLineList = mobileCardsListPage.getCardList();
        assumeThat("Список карт пуст", mobileCardLineList, not(empty()));
        for (MobileCardLine mobileCardLine : mobileCardLineList){
            assertThat("Пиктограмма карты отсутствует", mobileCardLine.getCardTypeImage(), not(nullValue()));
            assertThat("Нет имени карты", mobileCardLine.getCardName(), not(nullValue()));
            assertThat("Нет PAN карты", mobileCardLine.getPan(), not(nullValue()));
            assertThat("Нет статуса карты", mobileCardLine.getStateText(), not(nullValue()));
            assertThat("Нет ссылки на переименование карты", mobileCardLine.getEditLink(), not(nullValue()));
            if ("Активна".equals(mobileCardLine.getStateText())){
                assertThat("Для активной карты "+mobileCardLine.getCardName()+" неактивная кнопка переименования",
                        mobileCardLine.getEditLink().getAttribute("class"), not("rename_inactive"));
            } else {
                assertThat("Для неактивной карты "+mobileCardLine.getCardName()+" активна кнопка переименования",
                        mobileCardLine.getEditLink().getAttribute("class"), is("rename_inactive"));
            }
            assertThat("Нет ссылки для удаления карты", mobileCardLine.getDeleteLink(), not(nullValue()));
        }
    }
}
