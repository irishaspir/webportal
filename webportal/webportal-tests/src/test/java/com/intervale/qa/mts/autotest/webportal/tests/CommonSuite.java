package com.intervale.qa.mts.autotest.webportal.tests;

import com.intervale.qa.mts.autotest.webportal.WebConfig;
import com.intervale.qa.mts.autotest.webportal.tests.desktop.*;
import com.intervale.qa.mts.autotest.webportal.tests.mobile.*;
import com.intervale.qa.toolbox.db.simple.DBHelper;
import com.intervale.qa.toolbox.db.simple.OracleDBHelper;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by dchahovsky.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        AccountPaymentTest.class,
        AnonymousCardPaymentTest.class,
        CardAddPageTest.class,
        CardListPageTest.class,
        CardManageTest.class,
        FavouritesManageTest.class,
        FavouritesPageTest.class,
        HistoryPageTest.class,
        LeftMenuTest.class,
        MainPageTest.class,
        PaymentSearchTest.class,
        PrepareTest.class,
        RegisteredCardPaymentTest.class,
        HistoryTest.class,
        MobileAccountPaymentTest.class,
        MobileAnonymousCardPaymentTest.class,
        MobileCardAddTest.class,
        MobileFavouritesManageTest.class,
        MobileRegisteredCardPaymentTest.class,
        MobileCardListPageTest.class,
        MobileCardManageTest.class,
        MobileFavoritesPageTest.class,
        MobileFavouritesManageTest.class,
        MobileHistoryPageTest.class,
        MobilePaymentSearchTest.class,
        MobileRegisteredCardPaymentTest.class

})
public class CommonSuite {

    public static WebConfig CONFIG = new WebConfig();

    private static WebDriver driver = null;

    private static boolean initialized = false;

    @BeforeClass
    public static void beforeSuite() {
        driver = createWebDriver();
        initialized = true;
        DBHelper db = new OracleDBHelper(
                CONFIG.getPersonalCabinetDatabaseUri(),
                CONFIG.getPersonalCabinetDatabaseUsername(),
                CONFIG.getPersonalCabinetDatabasePassword()
        );
        clearBankCardsFromProfile(db, "79859900700");
        clearFavouritesFromProfile(db, "79859900700");
    }

    private static void clearFavouritesFromProfile(DBHelper db, String msisdn) {
        db.update("delete from PREPARED_PAYMENT where title != 'do-not-delete' and owner_id=" +
                "(select id from customer where identifier=? and canceled_at is null)", msisdn);
    }

    private static void clearBankCardsFromProfile(DBHelper db, String msisdn) {
        db.update("delete from payment_tool where title like 'test-%' and owner_id=" +
                "(select id from customer where identifier=? and canceled_at is null)", msisdn);
    }

    public static WebDriver createWebDriver() {
        DesiredCapabilities desiredCapabilities;
        desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setBrowserName("firefox");
        desiredCapabilities.setVersion("");
        String hubUrl = CONFIG.getProperty(WebConfig.SELENIUM_HUB_URL);
        try {
            driver = new RemoteWebDriver(new URL(hubUrl), desiredCapabilities);
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException("Illegal hub url", e);
        }
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        return driver;
    }

    @AfterClass
    public static void afterSuite() {
        if (null != driver) {
            driver.quit();
        }
        initialized = false;
    }

    public static boolean isInitialized() {
        return initialized;
    }

    public static WebDriver getDriver() {
        return driver;
    }
}
