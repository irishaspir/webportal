package com.intervale.qa.mts.autotest.webportal;

/**
 * Created by dchahovsky.
 */
public class StoriesList {

    public static final String MANAGE_CARDS = "Менеджмент карт";
    public static final String MANAGE_FAVOURITES = "Менеджмент избранного";
    public static final String HISTORY = "История";
    public static final String SEARCH = "Поиск";
    public static final String ACCOUNT_PAYMENT = "Платежи с л/с";
    public static final String ANONYMOUS_CARD_PAYMENT = "Платежи с анонимной б/к";
    public static final String REGISTERED_CARD_PAYMENT = "Платежи с зарегистрированной б/к";
    public static final String LEFT_MENU = "Левое меню";
    public static final String MAIN_PAGE = "Заглавная страница";
    public static final String HISTORY_AND_TARIFF ="Справка и тарифы";
    public static final String AUTOPAYMENT ="Автоплатежи";

}
