package com.intervale.qa.mts.autotest.webportal.tests.mobile;

import com.intervale.qa.mts.autotest.webportal.Profiles;
import com.intervale.qa.mts.autotest.webportal.matchers.FavoritesPageMatcher;
import com.intervale.qa.mts.autotest.webportal.matchers.MobileFavoritesPageMatcher;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.PaymentPage;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.favorite.*;
import com.intervale.qa.mts.autotest.webportal.selenium.mobile.MobilePaymentPage;
import com.intervale.qa.mts.autotest.webportal.selenium.mobile.favorite.*;
import com.intervale.qa.mts.autotest.webportal.tests.AbstractSeleniumTest;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static com.intervale.qa.mts.autotest.webportal.FeatureList.*;
import static com.intervale.qa.mts.autotest.webportal.StoriesList.MANAGE_FAVOURITES;
import static com.intervale.qa.mts.autotest.webportal.matchers.MobileFavoritesPageMatcher.containsPayment;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.fail;

/**
 * Created by dchahovsky.
 */
public class MobileFavouritesManageTest extends AbstractSeleniumTest {

    @Features(MOBILE_FEATURE)
    @Stories(MANAGE_FAVOURITES)
    @Description("Добавление, редактирование избранного и проверка измененных значений")
    @Test
    public void addEditAndOpenFavouritePayment() {
        /* Добавить платеж, изменить все его параметры, затем открыть платежную форму и проверить все параметры */
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobileFavoritesUrl());

        // adding to favorites
        String paymentName = String.valueOf(System.currentTimeMillis());
        List<String> treePath = Arrays.asList("autotest", "Автовеб - 1");
        addPhonePaymentToFavourites(treePath, paymentName, "9152731056", "10");

        // check adding;
        new MobileFavoritesPage(getDriver());
        MobileFavoritesPage mobileFavoritesPage = new MobileFavoritesPage(getDriver());
        Assert.assertThat(mobileFavoritesPage, MobileFavoritesPageMatcher.containsPayment(paymentName));

        // edit fav payment
        DateTimeFormatter dtf = DateTimeFormat.forPattern("ddMMyyyy-HH:mm:ss");
        String dateTime = dtf.print(new DateTime());
        String newTitle = dateTime + "_" + (new Random().nextInt(9000) + 1000);
        String newPhone = "9152731354";
        String newAmount = "20";
        editFavPayment(paymentName, newTitle, newPhone, newAmount);

        // check editing
        new MobileFavoritesPage(getDriver());
        mobileFavoritesPage = new MobileFavoritesPage(getDriver());
        Assert.assertThat("Измененный платеж " + newTitle + " должен быть в списке", mobileFavoritesPage.listFavoritePayments(), hasItem(newTitle));
        mobileFavoritesPage.getPaymentLine(newTitle).getPaymentLink().click();
        MobilePaymentPage mobilePaymentPage = new MobilePaymentPage(getDriver());
        String paymentAmount = mobilePaymentPage.getInputParamFieldByName("amount").getAttribute("value");
        Assert.assertThat("Некорректная сумма на форме из избранного", paymentAmount, is(newAmount));
        String paymentPhone = mobilePaymentPage.getInputParamFieldByName("param1").getAttribute("value"); // if payment for beeline
        //  String paymentPhone = mobilePaymentPage.getInputParamFieldByName("e_phone").getAttribute("value"); // if payment for mts
        Assert.assertThat("Некорректный номер телефона на форме из избранного '" + newPhone + "'", paymentPhone, is(newPhone));

    }

    @Features(MOBILE_FEATURE)
    @Stories(MANAGE_FAVOURITES)
    @Test
    public void addRenameAndRemoveFavouritePayment() {
        /* Добавить платеж, изменить его название, затем удалить его и проверить, что в списке его больше нет */
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobileFavoritesUrl());

        // adding to favorites
        String paymentName = String.valueOf(System.currentTimeMillis());
        List<String> treePath = Arrays.asList("autotest", "Автовеб - 1");
        addPhonePaymentToFavourites(treePath, paymentName, "9152731056", "10");

        // check adding;
        new MobileFavoritesPage(getDriver());
        MobileFavoritesPage mobileFavoritesPage = new MobileFavoritesPage(getDriver());
        Assert.assertThat(mobileFavoritesPage, MobileFavoritesPageMatcher.containsPayment(paymentName));

        // edit name payment
        DateTimeFormatter dtf = DateTimeFormat.forPattern("ddMMyyyy-HH:mm:ss");
        String dateTime = dtf.print(new DateTime());
        String newTitle = dateTime + "_" + (new Random().nextInt(9000) + 1000);
        mobileFavoritesPage = new MobileFavoritesPage(getDriver());
        MobileFavoritePaymentLine mobileFavoritePaymentLine = mobileFavoritesPage.getPaymentLine(paymentName);
        mobileFavoritePaymentLine.getEditLink().click();
        MobileEditFavoritePage mobileEditFavoritePage = new MobileEditFavoritePage(getDriver());
        if (newTitle != null && !"".equals(newTitle)) {
            // changing title
            mobileEditFavoritePage.getInputFieldById("title").click();
            mobileEditFavoritePage.getInputFieldById("title").clear();
            mobileEditFavoritePage.getInputFieldById("title").sendKeys(newTitle);
        }
        mobileEditFavoritePage.submit();

        // delete
        new MobileFavoritesPage(getDriver());
        mobileFavoritesPage = new MobileFavoritesPage(getDriver());
        Assert.assertThat("Измененный платеж " + newTitle + " должен быть в списке", mobileFavoritesPage.listFavoritePayments(), hasItem(newTitle));
        mobileFavoritesPage.getPaymentLine(newTitle).getDeleteLink().click();
        mobileFavoritesPage.confirmDeleteFavoritePayment();
        Assert.assertThat("Измененный платеж " + newTitle + " должен отсутствовать в списке", mobileFavoritesPage.listFavoritePayments(), not(hasItem(newTitle)));
    }

    @Features(MOBILE_FEATURE)
    @Stories(MANAGE_FAVOURITES)
    @Test
    public void addingToFavouritesWithBlankParamsIsOk() {
        /* добавить избранный платеж с пустыми параметрами и проверить его наличие в списке */
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobileFavoritesUrl());

        // adding to favorites
        String paymentName = String.valueOf(System.currentTimeMillis());
        List<String> treePath = Arrays.asList("autotest", "Автовеб - 1");
        addPhonePaymentToFavourites(treePath, paymentName, "", "");

        // check adding;
        new MobileFavoritesPage(getDriver());
        MobileFavoritesPage mobileFavoritesPage = new MobileFavoritesPage(getDriver());
        Assert.assertThat(mobileFavoritesPage, MobileFavoritesPageMatcher.containsPayment(paymentName));

    }


    /* ================== */

    @Step
    public void addPhonePaymentToFavourites(List<String> treePath, String paymentName, String phone, String amount) {
        getDriver().get(CONFIG.getMobileFavoritesUrl());
        MobileFavoritesPage mobileFavoritesPage = new MobileFavoritesPage(getDriver());
        WebElement addLink = mobileFavoritesPage.getAddToFavoriteLink();
        addLink.click();
        MobileChooseFavoritePaymentPage mobileChooseFavoritePaymentPage = new MobileChooseFavoritePaymentPage(getDriver());
        MobilePaymentsList mobilePaymentsList = mobileChooseFavoritePaymentPage.getPaymentsList();
        mobilePaymentsList.selectByTree(treePath);
        MobileAddFavoritePage mobileAddFavoritePage = new MobileAddFavoritePage(getDriver());
        mobileAddFavoritePage.getInputFieldById("title").clear();
        if (paymentName != null && !"".equals(paymentName)) {
            mobileAddFavoritePage.getInputFieldById("title").sendKeys(paymentName);
        }
        if (phone != null && !"".equals(phone)) {
            mobileAddFavoritePage.getInputFieldById("param1").sendKeys(phone);
        }
        if (amount != null && !"".equals(amount)) {
            mobileAddFavoritePage.getInputFieldById("amount").sendKeys(amount);
        }
        mobileAddFavoritePage.submit();
    }

    @Step
    public void editFavPayment(String favTitle, String newFavTitle, String newPhone, String newAmount) {
        getDriver().get(CONFIG.getMobileFavoritesUrl());
        MobileFavoritesPage mobileFavoritesPage = new MobileFavoritesPage(getDriver());
        MobileFavoritePaymentLine mobileFavoritePaymentLine = mobileFavoritesPage.getPaymentLine(favTitle);
        mobileFavoritePaymentLine.getEditLink().click();
        MobileEditFavoritePage mobileEditFavoritePage = new MobileEditFavoritePage(getDriver());
        if (newFavTitle != null && !"".equals(newFavTitle)) {
            // changing title
            mobileEditFavoritePage.getInputFieldById("title").click();
            mobileEditFavoritePage.getInputFieldById("title").clear();
            mobileEditFavoritePage.getInputFieldById("title").sendKeys(newFavTitle);
        }
        if (newPhone != null && !"".equals(newPhone)) {
            // changing phone number
//            String inputFieldId = "e_phone";  // if payment for mts
            String phoneInputFieldId = "param1";  // if payment for beeline
            mobileEditFavoritePage.getInputFieldById(phoneInputFieldId).click();
            mobileEditFavoritePage.getInputFieldById(phoneInputFieldId).clear();
            mobileEditFavoritePage.getInputFieldById(phoneInputFieldId).sendKeys(newPhone);
        }
        if (newAmount != null && !"".equals(newAmount)) {
            // changing amount
            mobileEditFavoritePage.getInputFieldById("amount").click();
            mobileEditFavoritePage.getInputFieldById("amount").clear();
            mobileEditFavoritePage.getInputFieldById("amount").sendKeys(newAmount);
        }
        logger.info("Renaming fav item '{}' to '{}'.", favTitle, newFavTitle);
        mobileEditFavoritePage.submit();
    }

}
