package com.intervale.qa.mts.autotest.webportal.tests.desktop;

import com.intervale.qa.mts.autotest.webportal.Profiles;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.HistoryPage;
import com.intervale.qa.mts.autotest.webportal.tests.AbstractSeleniumTest;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.Arrays;

import static com.intervale.qa.mts.autotest.webportal.FeatureList.DESKTOP_FEATURE;
import static com.intervale.qa.mts.autotest.webportal.StoriesList.HISTORY_AND_TARIFF;
import static java.lang.Thread.sleep;
import static org.junit.Assert.assertThat;
import static ru.yandex.qatools.matchers.collection.HasSameItemsAsListMatcher.hasSameItemsAsList;

/**
 * Created by ispiridonova on 14.10.2015.
 */
public class HistoryTest extends AbstractSeleniumTest {
    @Features(DESKTOP_FEATURE)
    @Stories(HISTORY_AND_TARIFF)
    @Description("Проверка наличия ссылок на странице справки и тарифы")
    @Test
    public void allLinksIsPresent() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMainPageUrl());
        HistoryPage page = new HistoryPage(getDriver());
        page.openPage();
        assertThat(page.listAllLinks(), hasSameItemsAsList(Arrays.asList(
                "Как оплатить услуги на нашем сайте?",
                "Что такое «Избранные платежи»?",
                "Как быстро поступит платеж?",
                "Есть ли комиссия за совершение платежа?",
                "О денежных переводах",
                "О денежных переводах с карты на карту",
                "Как пользоваться виртуальной картой МТС Деньги",
                "Можно ли совершить платеж по реквизитам организации?",
                "Насколько это безопасный сервис?",
                "Что такое 3-D Secure и как совершать платеж с карты, поддерживающей эту технологию?",
                "Другие способы совершения платежей.",
                "Ограничения при оплате."
        )));
    }
}
