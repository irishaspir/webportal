package com.intervale.qa.mts.autotest.webportal.tests.desktop;

import com.intervale.qa.mts.autotest.webportal.Profiles;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.LeftMenu;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.PaymentsPage;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.cards.CardsListPage;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.favorite.FavoritesPage;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.history.HistoryPage;
import com.intervale.qa.mts.autotest.webportal.tests.AbstractSeleniumTest;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.Arrays;

import static com.intervale.qa.mts.autotest.webportal.StoriesList.LEFT_MENU;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assume.assumeThat;
import static ru.yandex.qatools.matchers.collection.HasSameItemsAsListMatcher.hasSameItemsAsList;
import static com.intervale.qa.mts.autotest.webportal.FeatureList.*;

/**
 * Created by dchahovsky.
 */
public class LeftMenuTest extends AbstractSeleniumTest {

    @Features(DESKTOP_FEATURE)
    @Stories(LEFT_MENU)
    @Description("Проверка наличия необходимых корневых ссылок в левом меню")
    @Test
    @Ignore("Надоели автоплатежи!")
    public void allRootLinksShouldBePresentInLeftMenu() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMainPageUrl());
        LeftMenu leftMenu = new PaymentsPage(getDriver()).getLeftMenu();
        assertThat(leftMenu.getFirstLevelLinkTexts(), hasSameItemsAsList(Arrays.asList(
                "Оплата товаров и услуг",
                "Мои банковские карты",
                "Автопополнение счета МТС",
                "Оплата по реквизитам",
                "Избранные платежи",
                "История платежей",
                "Справка и тарифы",
                "Легкий платеж с телефона"
        )));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(LEFT_MENU)
    @Description("Проверка перехода на страницу карт через ссылку в левом меню")
    @Test
    public void transitionFromMainPageToCardsPageShouldBeSuccessful() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMainPageUrl());
        PaymentsPage paymentsPage = new PaymentsPage(getDriver());
        paymentsPage.getLeftMenu().openLinkByName("Мои карты");
        String newUrl = getDriver().getCurrentUrl();
        String cardsListUrl = CONFIG.getCardsListUrl();
        assertThat("Адрес страницы управления карт отличается от "+cardsListUrl, newUrl, is(cardsListUrl));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(LEFT_MENU)
    @Description("Проверка выделения блока 'Мои карты' в левом меню после перехода по нему.")
    @Test
    public void cardsItemInLeftMenuShouldBeActiveOnCardsPage(){
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getCardsListUrl());
        CardsListPage cardsListPage = new CardsListPage(getDriver());
        WebElement leftMenuCardsLink = cardsListPage.getLeftMenu().getLinkByName("Мои карты");
        assumeThat("Не найдена ссылка 'Мои карты' в левом меню", leftMenuCardsLink, not(nullValue()));
        assertThat("Неактивен (невыделен) блок 'Мои карты' в левом меню", leftMenuCardsLink.getAttribute("class"), is("active"));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(LEFT_MENU)
    @Description("Проверка перехода на страницу избранных через левое меню")
    @Test
    public void transitionFromMainPageToFavoritesShouldBeSuccessful() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMainPageUrl());
        PaymentsPage paymentsPage = new PaymentsPage(getDriver());
        paymentsPage.getLeftMenu().openLinkByName("Избранные платежи");
        String newUrl = getDriver().getCurrentUrl();
        String favoritesUrl = CONFIG.getFavoritesUrl();
        assertThat("Адрес избранных отличается от "+favoritesUrl, newUrl, is(favoritesUrl));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(LEFT_MENU)
    @Description("Проверка выделения блока избранных платежей в левом меню после перехода по нему.")
    @Test
    public void favPageItemInLeftMenuShouldBeActiveOnFavoritesPage() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getFavoritesUrl());
        FavoritesPage favoritesPage = new FavoritesPage(getDriver());
        WebElement leftMenuFavLink = favoritesPage.getLeftMenu().getLinkByName("Избранные платежи");
        assumeThat("Ссылка на избранные не найдена в левом меню", leftMenuFavLink, not(nullValue()));
        assertThat("Ссылка на избранные не выделена", leftMenuFavLink.getAttribute("class"), is("active"));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(LEFT_MENU)
    @Description("Проверка перехода на страницу истории через левое меню")
    @Test
    public void transitionFromLeftMenuToHistoryPageShouldBeSuccessful() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMainPageUrl());
        PaymentsPage paymentsPage = new PaymentsPage(getDriver());
        paymentsPage.getLeftMenu().openLinkByName("История платежей");
        String newUrl = getDriver().getCurrentUrl();
        String historyUrl = CONFIG.getHistoryUrl();
        assertThat("Адрес избранных отличается", newUrl, is(historyUrl));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(LEFT_MENU)
    @Description("Проверка выделения блока истории в левом меню после перехода по нему.")
    @Test
    public void historyItemInLeftMenuShouldBeActiveOnHistoryPage() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getHistoryUrl());
        HistoryPage historyPage = new HistoryPage(getDriver());
        WebElement leftMenuHistoryLink = historyPage.getLeftMenu().getLinkByName("История платежей");
        assumeThat("Не найдена ссылка на страницу истории в левом меню", leftMenuHistoryLink, not(nullValue()));
        assertThat("Не выделена ссылка на страницу истории в лдевом меню", leftMenuHistoryLink.getAttribute("class"), is("active"));
    }
}
