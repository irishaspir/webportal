package com.intervale.qa.mts.autotest.webportal.tests.mobile;

import com.intervale.qa.mts.autotest.webportal.Profiles;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.history.HistoryPage;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.history.PaymentHistoryLine;
import com.intervale.qa.mts.autotest.webportal.selenium.mobile.history.MobileHistoryPage;
import com.intervale.qa.mts.autotest.webportal.selenium.mobile.history.MobilePaymentHistoryLine;
import com.intervale.qa.mts.autotest.webportal.tests.AbstractSeleniumTest;
import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Ignore;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.intervale.qa.mts.autotest.webportal.FeatureList.MOBILE_FEATURE;
import static com.intervale.qa.mts.autotest.webportal.StoriesList.HISTORY;
import static com.intervale.qa.mts.autotest.webportal.matchers.DateTimeMatcher.greaterThan;
import static com.intervale.qa.mts.autotest.webportal.matchers.DateTimeMatcher.lessThen;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.junit.Assume.assumeThat;

/**
 * Created by dchahovsky.
 */
public class MobileHistoryPageTest extends AbstractSeleniumTest {

    @Features(MOBILE_FEATURE)
    @Stories(HISTORY)
    @Description("Проверка заголовка на странице истории")
    @Test
    public void mobileHistoryPageHasCertainTitle() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobileHistoryUrl());
        MobileHistoryPage mobileHistoryPage = new MobileHistoryPage(getDriver());
        assertThat("Некорректный заголовок на странице истории", mobileHistoryPage.getPageTitle(), is("История платежей"));

    }

    @Features(MOBILE_FEATURE)
    @Stories(HISTORY)
    @Description("Проверка того, что на следующей странице содержаться более ранние платежи")
    @Test
    public void nextHistoryPageShouldContainEarlierPayments() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobileHistoryUrl());
        MobileHistoryPage mobileHistoryPage = new MobileHistoryPage(getDriver());
        List<MobilePaymentHistoryLine> mobileHistoryList = mobileHistoryPage.getHistoryList();
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm");
        // search earliest DateTime on first page
        DateTime earliestDateTimeOnFirstPage = null;
        for (MobilePaymentHistoryLine line : mobileHistoryList) {
            DateTime lineDateTime = formatter.parseDateTime(line.getDateTime());
            if (earliestDateTimeOnFirstPage == null ||
                    DateTimeComparator.getInstance().compare(earliestDateTimeOnFirstPage, lineDateTime) > 0) {
                earliestDateTimeOnFirstPage = lineDateTime;
            }
        }
        mobileHistoryPage.goToNextPage();
        mobileHistoryPage = new MobileHistoryPage(getDriver());
        mobileHistoryList = mobileHistoryPage.getHistoryList();
        // search latest DateTime on next page
        DateTime latestDateTimeOnNextPage = null;
        for (MobilePaymentHistoryLine line : mobileHistoryList) {
            DateTime lineDateTime = formatter.parseDateTime(line.getDateTime());
            if (latestDateTimeOnNextPage == null ||
                    DateTimeComparator.getInstance().compare(latestDateTimeOnNextPage, lineDateTime) < 0) {
                latestDateTimeOnNextPage = lineDateTime;
            }
        }
        assertThat("На первой странице истории даты платежей должны быть больше", earliestDateTimeOnFirstPage, greaterThan(latestDateTimeOnNextPage));

    }

    @Features(MOBILE_FEATURE)
    @Stories(HISTORY)
    @Description("Проверка сортировки платежей по дате и времени")
    @Test
    public void paymentsShouldBeSortedByDateTime() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobileHistoryUrl());
        MobileHistoryPage mobileHistoryPage = new MobileHistoryPage(getDriver());
        List<MobilePaymentHistoryLine> mobileHistoryList = mobileHistoryPage.getHistoryList();
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm");
        DateTime lastPaymentDateTime = formatter.parseDateTime(mobileHistoryList.get(0).getDateTime());
        for (int i = 1; i < mobileHistoryList.size(); i++) {
            DateTime currentPaymentDateTime = formatter.parseDateTime(mobileHistoryList.get(i).getDateTime());
            assertThat("Неправильный порядок платежей. ", currentPaymentDateTime, lessThen(lastPaymentDateTime));
            lastPaymentDateTime = currentPaymentDateTime;
        }
    }

    @Features(MOBILE_FEATURE)
    @Stories(HISTORY)
    @Test
    @Description("Проверка всех необходимых данных и ссылок для платежей")
    public void rowsHaveAllNecessaryData() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobileHistoryUrl());
        MobileHistoryPage mobileHistoryPage = new MobileHistoryPage(getDriver());
        List<MobilePaymentHistoryLine> mobileHistoryList = mobileHistoryPage.getHistoryList();
        for (MobilePaymentHistoryLine line : mobileHistoryList) {
            assertThat("Нет даты платежа", line.getDateTime(), not(nullValue()));
            assertThat("Нет описание платежа", line.getPaymentDescription(), not(nullValue()));
            assertThat("Нет суммы платежа", line.getAmount(), not(nullValue()));
            assertThat("Нет статуса платежа", line.getResult(), not(nullValue()));
        }
    }

    @Features(MOBILE_FEATURE)
    @Stories(HISTORY)
    @Description("Проверка выбора платежей за заданный период")
    @Test
    public void paymentsDatesOnPageShouldBeWithinSelectedPeriod() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobileHistoryUrl());
        MobileHistoryPage mobileHistoryPage = new MobileHistoryPage(getDriver());
        DateTime today = new DateTime();
        DateTime startPeriod = today.minusDays(4);
        DateTime endPeriod = today.minusDays(1);
        mobileHistoryPage.doFilter(startPeriod, endPeriod);
        mobileHistoryPage = new MobileHistoryPage(getDriver());
        List<MobilePaymentHistoryLine> mobileHistoryList = mobileHistoryPage.getHistoryList();
        assumeThat("Нет истории за указанные период", mobileHistoryList, not(empty()));
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm");
        for (MobilePaymentHistoryLine line : mobileHistoryList) {
            DateTime paymentDateTime = formatter.parseDateTime(line.getDateTime());
            assertThat("Дата платежа должна быть позже начала периода", paymentDateTime, greaterThan(startPeriod).dateOnly());
            assertThat("Дата платежа должна быть раньше завершения периода", paymentDateTime, lessThen(endPeriod).dateOnly());
        }
    }

    @Features(MOBILE_FEATURE)
    @Stories(HISTORY)
    @Description("Проверка выбора периода истории за неделю")
    @Test
    public void weekLinkChoosesCorrectPeriod() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobileHistoryUrl());
        MobileHistoryPage mobileHistoryPage = new MobileHistoryPage(getDriver());
        mobileHistoryPage.getWeekFilterLink().click();
        DateTime today = new DateTime();
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd.MM.yyyy");
        DateTime dateTo = formatter.parseDateTime(mobileHistoryPage.getDatePeriodTo().getAttribute("value"));
        boolean dateToEqualsCurrentDate = DateTimeComparator.getDateOnlyInstance().compare(dateTo, today) == 0;
        assertThat("Правая граница периода должна быть сегодняшней", dateToEqualsCurrentDate, is(true));
        DateTime dateFrom = formatter.parseDateTime(mobileHistoryPage.getDatePeriodFrom().getAttribute("value"));
        boolean dateFrom7DaysEarlier = DateTimeComparator.getDateOnlyInstance().compare(dateFrom, today.minusDays(7)) == 0;
        assertThat("Левая граница периода должна быть 7 дней назад", dateFrom7DaysEarlier, is(true));
    }

    @Features(MOBILE_FEATURE)
    @Stories(HISTORY)
    @Description("Проверка выбора периода истории за месяц")
    @Test
    public void monthLinkChoosesCorrectPeriod() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobileHistoryUrl());
        MobileHistoryPage mobileHistoryPage = new MobileHistoryPage(getDriver());
        mobileHistoryPage.getMonthFilterLink().click();
        DateTime today = new DateTime();
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd.MM.yyyy");
        DateTime dateTo = formatter.parseDateTime(mobileHistoryPage.getDatePeriodTo().getAttribute("value"));
        boolean dateToEqualsCurrentDate = DateTimeComparator.getDateOnlyInstance().compare(dateTo, today) == 0;
        assertThat("Правая граница периода должна быть сегодняшней", dateToEqualsCurrentDate, is(true));
        DateTime dateFrom = formatter.parseDateTime(mobileHistoryPage.getDatePeriodFrom().getAttribute("value"));
        boolean dateFromMonthDaysEarlier = DateTimeComparator.getDateOnlyInstance().compare(dateFrom, today.minusMonths(1)) == 0;
        assertThat("Левая граница периода должна быть 1 месяц назад", dateFromMonthDaysEarlier, is(true));
    }

    @Features(MOBILE_FEATURE)
    @Stories(HISTORY)
    @Description("Проверка выбора периода истории за три месяца")
    @Test
    public void threeMonthLinkChoosesCorrectPeriod() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobileHistoryUrl());
        MobileHistoryPage mobileHistoryPage = new MobileHistoryPage(getDriver());
        mobileHistoryPage.get3MonthFilterLink().click();
        DateTime today = new DateTime();
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd.MM.yyyy");
        DateTime dateTo = formatter.parseDateTime(mobileHistoryPage.getDatePeriodTo().getAttribute("value"));
        boolean dateToEqualsCurrentDate = DateTimeComparator.getDateOnlyInstance().compare(dateTo, today) == 0;
        assertThat("Правая граница периода должна быть сегодняшней", dateToEqualsCurrentDate, is(true));
        DateTime dateFrom = formatter.parseDateTime(mobileHistoryPage.getDatePeriodFrom().getAttribute("value"));
        boolean dateFrom3MonthDaysEarlier = DateTimeComparator.getDateOnlyInstance().compare(dateFrom, today.minusMonths(3)) == 0;
        assertThat("Левая граница периода должна быть 3 месяца назад", dateFrom3MonthDaysEarlier, is(true));
    }

    @Features(MOBILE_FEATURE)
    @Stories(HISTORY)
    @Description("Проверка выбора периода истории за шесть месяцев")
    @Test
    public void sixMonthLinkChoosesCorrectPeriod() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobileHistoryUrl());
        MobileHistoryPage mobileHistoryPage = new MobileHistoryPage(getDriver());
        mobileHistoryPage.get6MonthFilterLink().click();
        DateTime today = new DateTime();
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd.MM.yyyy");
        DateTime dateTo = formatter.parseDateTime(mobileHistoryPage.getDatePeriodTo().getAttribute("value"));
        boolean dateToEqualsCurrentDate = DateTimeComparator.getDateOnlyInstance().compare(dateTo, today) == 0;
        assertThat("Правая граница периода должна быть сегодняшней", dateToEqualsCurrentDate, is(true));
        DateTime dateFrom = formatter.parseDateTime(mobileHistoryPage.getDatePeriodFrom().getAttribute("value"));
        boolean dateFrom6MonthDaysEarlier = DateTimeComparator.getDateOnlyInstance().compare(dateFrom, today.minusMonths(6)) == 0;
        assertThat("Левая граница периода должна быть 6 месяцев назад", dateFrom6MonthDaysEarlier, is(true));
    }

    @Features(MOBILE_FEATURE)
    @Stories(HISTORY)
    @Description("Профиль 9859900703 содержит успешные и неуспешные платежи от различных порталов. Проверка отображения всех статусов платежей в истории: успешно, неуспешно, suspend")
    @Test
    public void testAllTransactionStates() {
        /* Проверить отображение всех статусов платежей в истории: успешно, неуспешно, suspend(?) */
        final String SUCCESS = "Выполнено";
        final String FAIL = "Не выполнено";
        final String SUSPEND = "В обработке";
        Map<String, String> expectedStatusesForPaymentAmounts = new HashMap<>();
        expectedStatusesForPaymentAmounts.put("101", SUCCESS);
        expectedStatusesForPaymentAmounts.put("102", FAIL);
        expectedStatusesForPaymentAmounts.put("103", SUSPEND);
        expectedStatusesForPaymentAmounts.put("104", SUCCESS);
        expectedStatusesForPaymentAmounts.put("105", FAIL);
        expectedStatusesForPaymentAmounts.put("106", SUCCESS);
        expectedStatusesForPaymentAmounts.put("107", SUCCESS);
        expectedStatusesForPaymentAmounts.put("108", SUCCESS);
        expectedStatusesForPaymentAmounts.put("109", SUCCESS);
        expectedStatusesForPaymentAmounts.put("110", SUCCESS);
        expectedStatusesForPaymentAmounts.put("111", SUCCESS);
        setActiveProfile(Profiles.WITH_PAYMENTS_FROM_VARIOUS_PORTALS_WITH_VARIOUS_STATUSES);
        getDriver().get(CONFIG.getHistoryUrl() + "?dateFrom=28.04.2015&dateTo=30.04.2015&items=11&view=mobile");
        MobileHistoryPage mobileHistoryPage = new MobileHistoryPage(getDriver());
        List<MobilePaymentHistoryLine> mobilePaymentHistoryLines = mobileHistoryPage.getHistoryList();
        assertThat("На странице должно отображаться 11 платежей", mobilePaymentHistoryLines.size(), is(11));
        for (MobilePaymentHistoryLine mobilePaymentHistoryLine : mobilePaymentHistoryLines) {
            // Сумма без валюты (Захардкоженные суммы 101 - 111)
            String amount = mobilePaymentHistoryLine.getAmount().substring(0, 3);
            String expectedStatus = expectedStatusesForPaymentAmounts.get(amount);
            String receivedState = mobilePaymentHistoryLine.getResult();
            assertThat("Для платежа '" + mobilePaymentHistoryLine.getPaymentDescription() + "' неверный статус",
                    receivedState, is(expectedStatus));
            if (SUCCESS.equals(receivedState)) {
                assertThat("Для успешного платежа '" + mobilePaymentHistoryLine.getPaymentDescription() + "' ожидается класс элемента статуса 'done' (зеленый цвет текста)",
                        mobilePaymentHistoryLine.getResultElement().getAttribute("class"), is("done"));
            }
            if (FAIL.equals(receivedState)) {
                assertThat("Для не выполненного платежа '" + mobilePaymentHistoryLine.getPaymentDescription() + "' ожидается класс элемента статуса 'deny' (красный цвет текста)",
                        mobilePaymentHistoryLine.getResultElement().getAttribute("class"), is("deny"));
            }
            if (SUSPEND.equals(receivedState)) {
                assertThat("Для платежа в обработке '" + mobilePaymentHistoryLine.getPaymentDescription() + "' ожидается класс элемента статуса 'processing' (серый цвет текста)",
                        mobilePaymentHistoryLine.getResultElement().getAttribute("class"), is("processing"));
            }
        }
    }


}
