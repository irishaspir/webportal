package com.intervale.qa.mts.autotest.webportal.tests.desktop;

import com.intervale.qa.mts.autotest.webportal.Profiles;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.AutoPaymentPage;
import com.intervale.qa.mts.autotest.webportal.tests.AbstractSeleniumTest;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.intervale.qa.mts.autotest.webportal.FeatureList.DESKTOP_FEATURE;
import static com.intervale.qa.mts.autotest.webportal.StoriesList.AUTOPAYMENT;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * Created by ispiridonova on 20.10.2015.
 */
public class AutoPaymentTest extends AbstractSeleniumTest {
    @Features(DESKTOP_FEATURE)
    @Stories(AUTOPAYMENT)
    @Description("Добавление автоплатежа по порогу баланса")
    @Test
    public void addAutoPaymentBalance() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMainPageUrl());
        AutoPaymentPage page = new AutoPaymentPage(getDriver());
        page.openPage();
        page.clickLinkAddAutoPayment();
        page.selectCategory();
        page.selectAutoPayment();
        page.inputValuesAutoPayment();
        String name = page.inputNameAutoPaymentAndSelectType();
        assertThat("AutoPayment isn't added successful", page.takeNameAutoPaymentFromPage(), is(name));
        assertThat("AutoPayment isn't added successful", page.confirmAutoPayment(), is("Активен"));
    }


}
