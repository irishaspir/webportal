package com.intervale.qa.mts.autotest.webportal.tests;

import com.intervale.qa.mts.autotest.webportal.OpenwayEmulatorConfiguration;
import com.intervale.qa.mts.autotest.webportal.WebConfig;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.PaymentResultPage;
import com.intervale.qa.toolbox.util.card.CardGenerator;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.yandex.qatools.allure.annotations.Attachment;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by dchahovsky.
 */
public abstract class AbstractSeleniumTest {

    public static Logger logger = LoggerFactory.getLogger(AbstractSeleniumTest.class);

    protected static final String UNIQUE_TESTRUN_DATETIME = DateTimeFormat.forPattern("yyMMddHHmm").print(DateTime.now());
    protected static final CardGenerator VISA_NON_3DS_GENERATOR = new CardGenerator("44" + UNIQUE_TESTRUN_DATETIME);
    protected static final CardGenerator VISA_3DS_GENERATOR = new CardGenerator("45" + UNIQUE_TESTRUN_DATETIME);
//    protected static final CardGenerator MC_NON_3DS_GENERATOR = new CardGenerator("54" + UNIQUE_TESTRUN_DATETIME);
//    protected static final CardGenerator MC_3DS_GENERATOR = new CardGenerator("55" + UNIQUE_TESTRUN_DATETIME);

    public static WebConfig CONFIG = new WebConfig();

    protected OpenwayEmulatorConfiguration BANK = new OpenwayEmulatorConfiguration(CONFIG.getBankEmulatorBaseUrl());

    private WebDriver driver = null;

    private boolean runningOutOfSuite = false;

    @Rule
    public ScreenshotTestRule screenshotTestRule = new ScreenshotTestRule();

    @Before
    public void initWebDriverForTest() {
        if (CommonSuite.isInitialized()) {
            driver = CommonSuite.getDriver();
            runningOutOfSuite = false;
        }
        else {
            driver = CommonSuite.createWebDriver();
            runningOutOfSuite = true;
        }
        driver.get(CONFIG.getDummyUrlForCookieManagement());
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
    }

    public void setActiveProfile(String msisdn){
        if (null != driver){
            driver.manage().addCookie(new Cookie("fake-msisdn", msisdn, "/", new Date(System.currentTimeMillis() + 300_000L)));
        } else {
            throw new IllegalStateException("Driver has not been initialized!");
        }
    }

    protected WebDriver getDriver() {
        if (null != driver) {
            return driver;
        }
        else {
            throw new IllegalStateException("Driver has not been initialized!");
        }
    }

    protected String waitAndGetPaymentResult() {
        new WebDriverWait(driver, 10)
                .withMessage("Таймаут ожидания старта платежа")
                .until(ExpectedConditions.urlContains("pcid"));
        new WebDriverWait(driver, 5)
                .withMessage("Таймаут ожидания таблицы с результирующими данными")
                .until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("paymentData")));
        new WebDriverWait(driver, 50)
                .withMessage("Таймаут ожидания результата платежа")
                .until(ExpectedConditions.visibilityOfElementLocated(By.className("resultCode")));
        new WebDriverWait(driver, 5)
                .withMessage("Таймаут ожидания ")
                .until(ExpectedConditions.invisibilityOfElementLocated(By.id("msgprocessing")));
        return new PaymentResultPage(driver).getResultText();
    }

    protected void waitForDifferentCurrentUrlStartsWith(String currentUrlStartsWith) {
        long waitFor = System.currentTimeMillis() + 5_000;
        while (getDriver().getCurrentUrl().startsWith(currentUrlStartsWith) && (waitFor > System.currentTimeMillis())) {
            try {
                Thread.sleep(200);
            }
            catch (InterruptedException ie) {
                throw new RuntimeException("Interrupted", ie);
            }
        }
        if (currentUrlStartsWith.equals(getDriver().getCurrentUrl())) {
            throw new IllegalStateException("Закончилось время ожидания перехода на другую страницу");
        }
    }

    @Attachment(type = "image/png")
    protected byte[] takeAllureScreenshot() {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

    public class ScreenshotTestRule implements TestRule {
        @Override
        public Statement apply(final Statement statement, final Description description) {
            return new Statement() {
                @Override
                public void evaluate() throws Throwable {
                    try {
                        statement.evaluate();
                    } catch (Throwable t) {
                        if (null != driver) {
                            takeAllureScreenshot();
                        }
                        throw t; // rethrow to allow the failure to be reported to JUnit
                    } finally {
                        if (runningOutOfSuite) {
                            try {
                                if (null != driver) {
                                    driver.quit();
                                }
                            }
                            catch (Throwable t) {
                                logger.warn("{}", t);
                            }
                        }
                    }
                }
            };
        }
    }

}
