package com.intervale.qa.mts.autotest.webportal.tests.mobile;

import com.intervale.qa.mts.autotest.webportal.Profiles;
import com.intervale.qa.mts.autotest.webportal.selenium.mobile.MobileCategoryPage;
import com.intervale.qa.mts.autotest.webportal.selenium.mobile.MobilePaymentPage;
import com.intervale.qa.mts.autotest.webportal.selenium.mobile.MobilePaymentsSearchResultPage;
import com.intervale.qa.mts.autotest.webportal.tests.AbstractSeleniumTest;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.Arrays;

import static com.intervale.qa.mts.autotest.webportal.FeatureList.MOBILE_FEATURE;
import static com.intervale.qa.mts.autotest.webportal.StoriesList.SEARCH;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static ru.yandex.qatools.matchers.collection.HasSameItemsAsListMatcher.hasSameItemsAsList;

/**
 * Created by dchahovsky.
 */
public class MobilePaymentSearchTest extends AbstractSeleniumTest {

    @Features(MOBILE_FEATURE)
    @Stories(SEARCH)
    @Description("Проверка поиска по частичному имени платежа")
    @Test
    public void partialTitleSearch() {
        /* проверить поиск по частичному имени платежа */
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        WebDriver driver = getDriver();
        driver.get(CONFIG.getMobilePaymentsPageUrl());
        MobilePaymentPage mobilePaymentPage = new MobilePaymentPage(driver);
        mobilePaymentPage.searchForPayment("yot");
        MobilePaymentsSearchResultPage mobilePaymentsSearchResultPage = new MobilePaymentsSearchResultPage(driver);
        assertThat(mobilePaymentsSearchResultPage.getListPaymentsTitles(), hasSameItemsAsList(Arrays.asList(
                "Yota",
                "Yota - По номеру счета"
        )));
    }

    @Features(MOBILE_FEATURE)
    @Stories(SEARCH)
    @Description("Проверка перехода сразу на страницу платежа, если результат поиска - единственный платеж")
    @Test
    public void redirectToPaymentFormIfSearchResultIsSingleItem() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        WebDriver driver = getDriver();
        driver.get(CONFIG.getMobilePaymentsPageUrl());
        MobilePaymentPage mobilePaymentPage = new MobilePaymentPage(driver);
        mobilePaymentPage.searchForPayment("гудлайн");
        assertThat("Неправильное название страницы платежа", driver.findElement(By.xpath("//div[@id='result-search']/div[@class='static-lists']//a")).getText(), is("Гудлайн"));
        driver.findElement(By.xpath("//div[@id='result-search']/div[@class='static-lists']//a")).click();
        assertThat("Неправильное описание страницы платежа", mobilePaymentPage.getTitle(), is("Гудлайн"));
    }

    @Features(MOBILE_FEATURE)
    @Stories(SEARCH)
    @Description("Проверка поискового ответа, если по запросу ничего не найдено.")
    @Test
    public void testEmptySearchResultPage() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        WebDriver driver = getDriver();
        driver.get(CONFIG.getMobilePaymentsPageUrl());
        MobilePaymentPage mobilePaymentPage = new MobilePaymentPage(driver);
        mobilePaymentPage.searchForPayment("1234567890");
        MobilePaymentsSearchResultPage mobilePaymentsSearchResultPage = new MobilePaymentsSearchResultPage(driver);
        assertThat("Не правильный текст поискового ответа",
                mobilePaymentsSearchResultPage.getAnswerMessage(), is("По запросу 1234567890 ничего не найдено."));
    }

    @Features(MOBILE_FEATURE)
    @Stories(SEARCH)
    @Description("Ввод части уникального ОПИСАНИЯ платежа, проверка перехода к форме оплаты")
    @Test
    public void searchIsPerformedWithinDescriptionToo() {
        /* провести поиск по частичному ОПИСАНИЮ какого-то платежа, желательно уникальному */
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        WebDriver driver = getDriver();
        driver.get(CONFIG.getMobilePaymentsPageUrl());
        MobilePaymentPage mobilePaymentPage = new MobilePaymentPage(driver);
        mobilePaymentPage.searchForPayment("ворлд трэвел теле");
        assertThat("Неправильное название страницы платежа", driver.findElement(By.xpath("//div[@id='result-search']/div[@class='static-lists']//a")).getText(), is("Гудлайн"));
        driver.findElement(By.xpath("//div[@id='result-search']/div[@class='static-lists']//a")).click();
        assertThat("Неправильное описание страницы платежа", mobilePaymentPage.getTitle(), is("Гудлайн"));
    }

    @Features(MOBILE_FEATURE)
    @Stories(SEARCH)
    @Description("Поиск уникального платежа из категории только по этой категории. В другой категории присутствуют платежи, удовлетворяющие критерию поиска. Проверка перехода сразу к форме оплаты.")
    @Test
    public void searchFromCategoryOnlyWithinTheCategory() {
        /* поиск из категории только по этой категории. найти платеж ниже по дереву,
            в другой категории тоже должны быть платежи, удовлетворяющие критерию поиска
        */
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        WebDriver driver = getDriver();
        driver.get(CONFIG.getMobilePaymentsPageUrl());
        MobilePaymentPage mobilePaymentPage = new MobilePaymentPage(driver);
        mobilePaymentPage.openRootCategoryByTitle("Интернет и ТВ");
        MobileCategoryPage mobileCategoryPage = new MobileCategoryPage(driver);
        mobileCategoryPage.searchInThisCategory("билайн");
        assertThat("Неправильное название страницы платежа",
                driver.findElement(By.xpath("//div[@id='result-search']/div[@class='static-lists']//a")).getText(), is("Домашний Интернет Билайн"));
        driver.findElement(By.xpath("//div[@id='result-search']/div[@class='static-lists']//a")).click();
        assertThat("Неправильное описание страницы платежа",
                mobilePaymentPage.getTitle(), is("Домашний Интернет Билайн"));
    }

    @Features(MOBILE_FEATURE)
    @Stories(SEARCH)
    @Description("Поиск из категории по всем категориям. Проверка списка результатов поиска")
    @Test
    public void searchFromCategoryWithinAllPaymentTree() {
        /* поиск из категории по всем категориям. Найти платеж из другой категории */
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        WebDriver driver = getDriver();
        driver.get(CONFIG.getMobilePaymentsPageUrl());
        MobilePaymentPage mobilePaymentPage = new MobilePaymentPage(driver);
        mobilePaymentPage.openRootCategoryByTitle("Благотворительность");
        MobileCategoryPage mobileCategoryPage = new MobileCategoryPage(driver);
        mobileCategoryPage.searchInAllCategories("yota");
        MobilePaymentsSearchResultPage mobilePaymentsSearchResultPage = new MobilePaymentsSearchResultPage(driver);
        assertThat(mobilePaymentsSearchResultPage.getListPaymentsTitles(), hasSameItemsAsList(Arrays.asList(
                "Yota",
                "Yota - По номеру счета"
        )));
    }

}
