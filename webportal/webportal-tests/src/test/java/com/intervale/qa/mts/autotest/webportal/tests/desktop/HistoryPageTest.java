package com.intervale.qa.mts.autotest.webportal.tests.desktop;

import com.intervale.qa.mts.autotest.webportal.Profiles;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.*;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.history.HistoryPage;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.history.PaymentHistoryLine;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.history.PrintHistoryPage;
import com.intervale.qa.mts.autotest.webportal.tests.AbstractSeleniumTest;
import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.intervale.qa.mts.autotest.webportal.StoriesList.HISTORY;
import static com.intervale.qa.mts.autotest.webportal.matchers.DateTimeMatcher.greaterThan;
import static com.intervale.qa.mts.autotest.webportal.matchers.DateTimeMatcher.lessThen;
import static com.intervale.qa.mts.autotest.webportal.matchers.HistoryPageMatcher.containsHistoryPayment;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assume.assumeThat;
import static ru.yandex.qatools.matchers.collection.HasSameItemsAsListMatcher.hasSameItemsAsList;
import static com.intervale.qa.mts.autotest.webportal.FeatureList.*;

/**
 * Created by ashaporev
 */
public class HistoryPageTest extends AbstractSeleniumTest {


    @Features(DESKTOP_FEATURE)
    @Stories(HISTORY)
    @Description("Проверка заголовка на странице истории")
    @Test
    public void historyPageHasCertainTitle(){
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getHistoryUrl());
        HistoryPage historyPage = new HistoryPage(getDriver());
        assertThat("Некорректный заголовок на странице истории", historyPage.getPageTitle(), is("ИСТОРИЯ ПЛАТЕЖЕЙ"));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(HISTORY)
    @Description("Проверка того, что на следующей странице содержаться более ранние платежи")
    @Test
    public void nextHistoryPageShouldContainEarlierPayments(){
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getHistoryUrl());
        HistoryPage historyPage = new HistoryPage(getDriver());
        List<PaymentHistoryLine> historyList = historyPage.getHistoryList ();
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm");
        // search earliest DateTime on first page
        DateTime earliestDateTimeOnFirstPage = null;
        for (PaymentHistoryLine line : historyList){
            DateTime lineDateTime = formatter.parseDateTime(line.getDateTime());
            if (earliestDateTimeOnFirstPage == null ||
                    DateTimeComparator.getInstance().compare(earliestDateTimeOnFirstPage, lineDateTime) > 0){
                earliestDateTimeOnFirstPage = lineDateTime;
            }
        }
        historyPage.goToNextPage();
        historyPage = new HistoryPage(getDriver());
        historyList = historyPage.getHistoryList ();
        // search latest DateTime on next page
        DateTime latestDateTimeOnNextPage = null;
        for (PaymentHistoryLine line : historyList){
            DateTime lineDateTime = formatter.parseDateTime(line.getDateTime());
            if (latestDateTimeOnNextPage == null ||
                    DateTimeComparator.getInstance().compare(latestDateTimeOnNextPage, lineDateTime) < 0){
                latestDateTimeOnNextPage = lineDateTime;
            }
        }
        assertThat("На первой странице истории даты платежей должны быть больше", earliestDateTimeOnFirstPage, greaterThan(latestDateTimeOnNextPage));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(HISTORY)
    @Description("Проверка отображения по 10 платежей на странице")
    @Test
    @Ignore("Проблемы с выбором размера страницы из списка")
    public void checkView10PaymentsOnPage(){
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getHistoryUrl());
        HistoryPage historyPage = new HistoryPage(getDriver());
        assumeThat("В профиле менее 10 платежей", historyPage.getFoundPaymentsCount(), greaterThanOrEqualTo(10));
        historyPage.setViewPaymentsOnPageCount(10);
        historyPage = new HistoryPage(getDriver());
        assertThat("На странице должно отображаться 10 платежей", historyPage.getHistoryList().size(), is(10));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(HISTORY)
    @Description("Проверка отображения по 25 платежей на странице")
    @Test
    @Ignore("Проблемы с выбором размера страницы из списка")
    public void checkView25PaymentsOnPage(){
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getHistoryUrl());
        HistoryPage historyPage = new HistoryPage(getDriver());
        assumeThat("В профиле менее 25 платежей", historyPage.getFoundPaymentsCount(), greaterThanOrEqualTo(25));
        historyPage.setViewPaymentsOnPageCount(25);
        historyPage = new HistoryPage(getDriver());
        assertThat("На странице должно отображаться 25 платежей", historyPage.getHistoryList().size(), is(25));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(HISTORY)
    @Description("Проверка отображения по 50 платежей на странице")
    @Test
    @Ignore("Проблемы с выбором размера страницы из списка")
    public void checkView50PaymentsOnPage(){
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getHistoryUrl());
        HistoryPage historyPage = new HistoryPage(getDriver());
        assumeThat("В профиле менее 50 платежей", historyPage.getFoundPaymentsCount(), greaterThanOrEqualTo(50));
        historyPage.setViewPaymentsOnPageCount(50);
        historyPage = new HistoryPage(getDriver());
        assertThat("На странице должно отображаться 50 платежей", historyPage.getHistoryList().size(), is(50));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(HISTORY)
    @Description("Проверка отображения по 100 платежей на странице")
    @Test
    @Ignore("Проблемы с выбором размера страницы из списка")
    public void checkView100PaymentsOnPage(){
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getHistoryUrl());
        HistoryPage historyPage = new HistoryPage(getDriver());
        assumeThat("В профиле менее 100 платежей", historyPage.getFoundPaymentsCount(), greaterThanOrEqualTo(100));
        historyPage.setViewPaymentsOnPageCount(100);
        historyPage = new HistoryPage(getDriver());
        assertThat("На странице должно отображаться 100 платежей", historyPage.getHistoryList().size(), is(100));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(HISTORY)
    @Description("Проверка сортировки платежей по дате и времени")
    @Test
    public void paymentsShouldBeSortedByDateTime() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getHistoryUrl());
        HistoryPage historyPage = new HistoryPage(getDriver());
        List<PaymentHistoryLine> paymentsHistory = historyPage.getHistoryList();
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm");
        DateTime lastPaymentDateTime = formatter.parseDateTime(paymentsHistory.get(0).getDateTime());
        for (int i=1; i<paymentsHistory.size(); i++){
            DateTime currentPaymentDateTime = formatter.parseDateTime(paymentsHistory.get(i).getDateTime());
            assertThat("Неправильный порядок платежей. ", currentPaymentDateTime, lessThen(lastPaymentDateTime));
            lastPaymentDateTime = currentPaymentDateTime;
        }
    }

    @Features(DESKTOP_FEATURE)
    @Stories(HISTORY)
    @Description("Проверка всех необходимых данных и ссылок для платежей")
    @Test
    public void rowsHaveAllNecessaryData() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getHistoryUrl());
        HistoryPage historyPage = new HistoryPage(getDriver());
        List<PaymentHistoryLine> paymentsHistory = historyPage.getHistoryList();
        for (PaymentHistoryLine line : paymentsHistory){
            assertThat("Нет даты платежа", line.getDateTime(), not(nullValue()));
            assertThat("Нет пиктограммы платежного средства", line.getPaymentSource(), not(nullValue()));
            assertThat("Нет описание платежа", line.getPaymentDescription(), not(nullValue()));
            assertThat("Нет суммы платежа", line.getAmount(), not(nullValue()));
            assertThat("Нет статуса платежа", line.getResult(), not(nullValue()));
            if (!"В обработке".equals(line.getResult())){
                /* todo: кнопка повтора должна быть только для платежей, инициированный из web */
                assertThat("Нет кнопки повтора", line.getRepeatLink(), not(nullValue()));
            }
        }
    }

    @Features(DESKTOP_FEATURE)
    @Stories(HISTORY)
    @Description("Проверка выбора платежей за заданный период")
    @Test
    public void paymentsDatesOnPageShouldBeWithinSelectedPeriod() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getHistoryUrl());
        HistoryPage historyPage = new HistoryPage(getDriver());
        DateTime today = new DateTime();
        DateTime startPeriod = today.minusDays(4);
        DateTime endPeriod = today.minusDays(1);
        historyPage.doFilter(startPeriod, endPeriod);
        historyPage = new HistoryPage(getDriver());
        List<PaymentHistoryLine> paymentsHistory = historyPage.getHistoryList();
        assumeThat("Нет истории за указанные период", paymentsHistory, not(empty()));
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm");
        for (PaymentHistoryLine line : paymentsHistory){
            DateTime paymentDateTime = formatter.parseDateTime(line.getDateTime());
            assertThat("Дата платежа должна быть позже начала периода", paymentDateTime, greaterThan(startPeriod).dateOnly());
            assertThat("Дата платежа должна быть раньше завершения периода", paymentDateTime, lessThen(endPeriod).dateOnly());
        }
    }

    @Features(DESKTOP_FEATURE)
    @Stories(HISTORY)
    @Description("Проверка выбора периода истории за неделю")
    @Test
    public void weekLinkChoosesCorrectPeriod(){
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getHistoryUrl());
        HistoryPage historyPage = new HistoryPage(getDriver());
        historyPage.getWeekFilterLink().click();
        historyPage = new HistoryPage(getDriver());
        DateTime today = new DateTime();
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd.MM.yyyy");
        DateTime dateTo = formatter.parseDateTime(historyPage.getDatePeriodTo().getAttribute("value"));
        boolean dateToEqualsCurrentDate = DateTimeComparator.getDateOnlyInstance().compare(dateTo, today) == 0;
        assertThat("Правая граница периода должна быть сегодняшней", dateToEqualsCurrentDate, is(true));
        DateTime dateFrom = formatter.parseDateTime(historyPage.getDatePeriodFrom().getAttribute("value"));
        boolean dateFrom7DaysEarlier = DateTimeComparator.getDateOnlyInstance().compare(dateFrom, today.minusDays(7)) == 0;
        assertThat("Левая граница периода должна быть 7 дней назад", dateFrom7DaysEarlier, is(true));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(HISTORY)
    @Description("Проверка выбора периода истории за месяц")
    @Test
    public void monthLinkChoosesCorrectPeriod(){
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getHistoryUrl());
        HistoryPage historyPage = new HistoryPage(getDriver());
        historyPage.getMonthFilterLink().click();
        historyPage = new HistoryPage(getDriver());
        DateTime today = new DateTime();
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd.MM.yyyy");
        DateTime dateTo = formatter.parseDateTime(historyPage.getDatePeriodTo().getAttribute("value"));
        boolean dateToEqualsCurrentDate = DateTimeComparator.getDateOnlyInstance().compare(dateTo, today) == 0;
        assertThat("Правая граница периода должна быть сегодняшней", dateToEqualsCurrentDate, is(true));
        DateTime dateFrom = formatter.parseDateTime(historyPage.getDatePeriodFrom().getAttribute("value"));
        boolean dateFromMonthDaysEarlier = DateTimeComparator.getDateOnlyInstance().compare(dateFrom, today.minusMonths(1)) == 0;
        assertThat("Левая граница периода должна быть 1 месяц назад", dateFromMonthDaysEarlier, is(true));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(HISTORY)
    @Description("Проверка выбора периода истории за три месяца")
    @Test
    public void threeMonthLinkChoosesCorrectPeriod(){
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getHistoryUrl());
        HistoryPage historyPage = new HistoryPage(getDriver());
        historyPage.get3MonthFilterLink().click();
        historyPage = new HistoryPage(getDriver());
        DateTime today = new DateTime();
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd.MM.yyyy");
        DateTime dateTo = formatter.parseDateTime(historyPage.getDatePeriodTo().getAttribute("value"));
        boolean dateToEqualsCurrentDate = DateTimeComparator.getDateOnlyInstance().compare(dateTo, today) == 0;
        assertThat("Правая граница периода должна быть сегодняшней", dateToEqualsCurrentDate, is(true));
        DateTime dateFrom = formatter.parseDateTime(historyPage.getDatePeriodFrom().getAttribute("value"));
        boolean dateFrom3MonthDaysEarlier = DateTimeComparator.getDateOnlyInstance().compare(dateFrom, today.minusMonths(3)) == 0;
        assertThat("Левая граница периода должна быть 3 месяца назад", dateFrom3MonthDaysEarlier, is(true));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(HISTORY)
    @Description("Проверка выбора периода истории за шесть месяцев")
    @Test
    public void sixMonthLinkChoosesCorrectPeriod(){
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getHistoryUrl());
        HistoryPage historyPage = new HistoryPage(getDriver());
        historyPage.get6MonthFilterLink().click();
        historyPage = new HistoryPage(getDriver());
        DateTime today = new DateTime();
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd.MM.yyyy");
        DateTime dateTo = formatter.parseDateTime(historyPage.getDatePeriodTo().getAttribute("value"));
        boolean dateToEqualsCurrentDate = DateTimeComparator.getDateOnlyInstance().compare(dateTo, today) == 0;
        assertThat("Правая граница периода должна быть сегодняшней", dateToEqualsCurrentDate, is(true));
        DateTime dateFrom = formatter.parseDateTime(historyPage.getDatePeriodFrom().getAttribute("value"));
        boolean dateFrom6MonthDaysEarlier = DateTimeComparator.getDateOnlyInstance().compare(dateFrom, today.minusMonths(6)) == 0;
        assertThat("Левая граница периода должна быть 6 месяцев назад", dateFrom6MonthDaysEarlier, is(true));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(HISTORY)
    @Description("Проверка номера телефона поддержки на страницу истории")
    @Test
    public void printPageHasSupportPhone(){
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getHistoryUrl());
        HistoryPage historyPage = new HistoryPage(getDriver());
        historyPage.getPrintButton().click();
        String mainWindow = getDriver().getWindowHandle();
        for (String winHandle : getDriver().getWindowHandles()){
            if (!mainWindow.equals(winHandle)){
                getDriver().switchTo().window(winHandle);
                break;
            }
        }
        PrintHistoryPage printHistoryPage = new PrintHistoryPage(getDriver());
        assertThat("Неправильный номер поддержки", printHistoryPage.getSupportPhone(), is("8-800-250-0890"));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(HISTORY)
    @Description("Проверка того, что список платежей на странице печати содержит те же платежи.")
    @Test
    @Ignore("Отключено. Нужно разбираться почему локально все работает, а на TCBA01 происходит непонятная хрень")
    public void printPageContainSamePayments(){
        setActiveProfile(Profiles.WITH_PAYMENTS_FROM_VARIOUS_PORTALS_WITH_VARIOUS_STATUSES);
        getDriver().get(CONFIG.getHistoryUrl() + "?dateFrom=28.04.2015&dateTo=30.04.2015&items=11");
        HistoryPage historyPage = new HistoryPage(getDriver());
        List<PaymentHistoryLine> paymentHistory = historyPage.getHistoryList();
        List<String> pcidHistory = new ArrayList<>();
        for (PaymentHistoryLine line : paymentHistory){
            pcidHistory.add(line.getPaymentPcid());
        }
        historyPage.getPrintButton().click();
        String mainWindow = getDriver().getWindowHandle();
        for (String winHandle : getDriver().getWindowHandles()){
            if (!mainWindow.equals(winHandle)){
                getDriver().switchTo().window(winHandle);
                break;
            }
        }
        PrintHistoryPage printHistoryPage = new PrintHistoryPage(getDriver());
        List<PaymentHistoryLine> printPagePaymentHistory = printHistoryPage.getHistoryList();
        assertThat("Количество платежей на странице печати неправильное", printPagePaymentHistory.size(), is(paymentHistory.size()));
        List<String> printPagePaymentsPcid = new ArrayList<>();
        for (PaymentHistoryLine line : printPagePaymentHistory){
            printPagePaymentsPcid.add(line.getPaymentPcid());
        }
        assertThat("Список платежей на странице печати не совпадает", printPagePaymentsPcid, hasSameItemsAsList(pcidHistory));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(HISTORY)
    @Description("Проверка всех необходимых данных на странице печати.")
    @Test
    @Ignore("Нужно переработать тест. Нужно проводить тестирование на статических данных, которые не меняются от теста к тесту")
    public void printPageRowsContainAllNecessaryData(){
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getHistoryUrl());
        HistoryPage historyPage = new HistoryPage(getDriver());
        historyPage.getPrintButton().click();
        String mainWindow = getDriver().getWindowHandle();
        for (String winHandle : getDriver().getWindowHandles()){
            if (!mainWindow.equals(winHandle)){
                getDriver().switchTo().window(winHandle);
                break;
            }
        }
        PrintHistoryPage printHistoryPage = new PrintHistoryPage(getDriver());
        List<PaymentHistoryLine> paymentsHistory = printHistoryPage.getHistoryList();
        for (PaymentHistoryLine line : paymentsHistory){
            assertThat("Нет времени проведения платежа", line.getDateTime(), not(nullValue()));
            assertThat("Нет пиктограммы платьежного средства", line.getPaymentSource(), not(nullValue()));
            assertThat("Нет описания платежа", line.getPaymentDescription(), not(nullValue()));
            assertThat("Нет сумы платежа", line.getAmount(), not(nullValue()));
            assertThat("Нет статуса платежа", line.getResult(), not(nullValue()));
            if (!"В обработке".equals(line.getResult())){
                // todo кнопка повтора должна быть только для платежей, проведенных из WEB
                assertThat("Нет кнопки повтора платежа", line.getRepeatLink(), not(nullValue()));
            }
        }
    }

    @Features(DESKTOP_FEATURE)
    @Stories(HISTORY)
    @Description("Профиль 9859900703 содержит успешные и неуспешные платежи от различных порталов. Проверка, что кнопка повтора присутствует только у платежей, иницированных из web-портала. Переход по кнопке 'Повторить платеж' и проверка, что открывается форма с заполненными параметрами")
    @Test
    public void repeatLinkShouldBeAvailableOnlyForWebportalPayments() {
        setActiveProfile(Profiles.WITH_PAYMENTS_FROM_VARIOUS_PORTALS_WITH_VARIOUS_STATUSES);
        getDriver().get(CONFIG.getHistoryUrl() + "?dateFrom=28.04.2015&dateTo=30.04.2015&items=11");
        HistoryPage historyPage = new HistoryPage(getDriver());
        List<PaymentHistoryLine> paymentHistoryLines = historyPage.getHistoryList();
        for (PaymentHistoryLine paymentHistoryLine : paymentHistoryLines){
            // Сумма без валюты (Захардкоженные суммы 101 - 111)
            String amount = paymentHistoryLine.getAmount().substring(0, 3);
            // Захардкоженные платежи с соответствующими суммами
            /* todo
                CanFindElementMatcher просто не попал в 1.1, а есть только в master. Можно скомуниздить:
                https://github.com/yandex-qatools/matchers-java/blob/master/webdriver-matchers/src/main/java/ru/yandex/qatools/matchers/webdriver/driver/CanFindElementMatcher.java
            */
            if ("101".equals(amount) || "102".equals(amount) || "104".equals(amount) || "105".equals(amount)){
                assertThat("Должна отображаться ссылка на повтор платежа '" + paymentHistoryLine.getPaymentDescription() + "'.",
                        paymentHistoryLine.getRepeatLink(), not(nullValue()));
            } else {
                assertThat("Ссылка повтора для платежа '"+paymentHistoryLine.getPaymentDescription()+"' не должна отображаться.",
                        paymentHistoryLine.getRepeatLink(), is(nullValue()));
            }
        }
    }

    @Features(DESKTOP_FEATURE)
    @Stories(HISTORY)
    @Description("Профиль 9859900703 содержит успешные и неуспешные платежи от различных порталов. Проверка отображения всех статусов платежей в истории: успешно, неуспешно, suspend")
    @Test
    public void testAllTransactionStates() {
        final String SUCCESS = "Выполнено";
        final String FAIL = "Не выполнено";
        final String SUSPEND = "В обработке";
        Map<String, String> expectedStatusesForPaymentAmounts = new HashMap<>();
        expectedStatusesForPaymentAmounts.put("101", SUCCESS);
        expectedStatusesForPaymentAmounts.put("102", FAIL);
        expectedStatusesForPaymentAmounts.put("103", SUSPEND);
        expectedStatusesForPaymentAmounts.put("104", SUCCESS);
        expectedStatusesForPaymentAmounts.put("105", FAIL);
        expectedStatusesForPaymentAmounts.put("106", SUCCESS);
        expectedStatusesForPaymentAmounts.put("107", SUCCESS);
        expectedStatusesForPaymentAmounts.put("108", SUCCESS);
        expectedStatusesForPaymentAmounts.put("109", SUCCESS);
        expectedStatusesForPaymentAmounts.put("110", SUCCESS);
        expectedStatusesForPaymentAmounts.put("111", SUCCESS);
        setActiveProfile(Profiles.WITH_PAYMENTS_FROM_VARIOUS_PORTALS_WITH_VARIOUS_STATUSES);
        getDriver().get(CONFIG.getHistoryUrl() + "?dateFrom=28.04.2015&dateTo=30.04.2015&items=11");
        HistoryPage historyPage = new HistoryPage(getDriver());
        List<PaymentHistoryLine> paymentHistoryLines = historyPage.getHistoryList();
        assertThat("На странице должно отображаться 11 платежей", paymentHistoryLines.size(), is(11));
        for (PaymentHistoryLine paymentHistoryLine : paymentHistoryLines){
            // Сумма без валюты (Захардкоженные суммы 101 - 111)
            String amount = paymentHistoryLine.getAmount().substring(0, 3);
            String expectedStatus = expectedStatusesForPaymentAmounts.get(amount);
            String receivedState = paymentHistoryLine.getResult();
            assertThat("Для платежа '"+paymentHistoryLine.getPaymentDescription()+"' неверный статус",
                    receivedState, is(expectedStatus));
            if (SUCCESS.equals(receivedState)){
                assertThat("Для успешного платежа '"+paymentHistoryLine.getPaymentDescription()+"' ожидается класс элемента статуса 'done' (зеленый цвет текста)",
                        paymentHistoryLine.getResultElement().getAttribute("class"), is("done"));
            }
            if (FAIL.equals(receivedState)){
                assertThat("Для не выполненного платежа '"+paymentHistoryLine.getPaymentDescription()+"' ожидается класс элемента статуса 'deny' (красный цвет текста)",
                        paymentHistoryLine.getResultElement().getAttribute("class"), is("deny"));
            }
            if (SUSPEND.equals(receivedState)){
                assertThat("Для платежа в обработке '"+paymentHistoryLine.getPaymentDescription()+"' ожидается класс элемента статуса 'processing' (серый цвет текста)",
                        paymentHistoryLine.getResultElement().getAttribute("class"), is("processing"));
            }
        }
    }

    /* DEPRECATED */

    @Features(DEPRECATED)
    @Test @Deprecated
    public void transitionFromMainPageShouldBeSuccessful(){
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        WebDriver driver = getDriver();
        driver.get(CONFIG.getMainPageUrl());
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        paymentsPage.getLeftMenu().openLinkByName("История платежей");
        String newUrl = getDriver().getCurrentUrl();
        String historyUrl = CONFIG.getHistoryUrl();
        assertThat("Адрес истории из левого меню отличается от " + historyUrl, newUrl, is(historyUrl));
    }

    @Features(DEPRECATED)
    @Test @Deprecated
    public void paymentFromAccountAppearsInHistory(){
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        WebDriver driver = getDriver();
        driver.get(CONFIG.getMainPageUrl());
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        paymentsPage.openMenuItem("Автовеб - 1");

        PaymentPage paymentPage = new PaymentPage(driver);
        paymentPage.getInputParamFieldByName("param1").sendKeys("9104451325");
        paymentPage.getInputParamFieldByName("amount").sendKeys("10");
        paymentPage.submit();

        assumeThat("Платеж должен завершиться успешно", waitAndGetPaymentResult(), is("Выполнено"));
        PaymentResultPage paymentResultPage = new PaymentResultPage(driver);
        String pcid = paymentResultPage.getPcid();
        String phoneNumber = paymentResultPage.getParamById("param1");
        String amount = paymentResultPage.getParamById("amount");
        String date = paymentResultPage.getDate();

        LeftMenu leftMenu = paymentResultPage.getLeftMenu();
        leftMenu.openLinkByName("История платежей");
        HistoryPage historyPage = new HistoryPage(driver);

        assertThat(historyPage, containsHistoryPayment(pcid, date, amount, "Автовеб - 1 " +phoneNumber));
    }

    @Features(DEPRECATED)
    @Test @Deprecated
    public void paymentFromCardAppearsInHistory(){
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        WebDriver driver = getDriver();
        driver.get(CONFIG.getMainPageUrl());
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        paymentsPage.openMenuItem("Автовеб - 1");

        PaymentPage paymentPage = new PaymentPage(driver);
        paymentPage.getInputParamFieldByName("param1").sendKeys("9104451325");
        paymentPage.getInputParamFieldByName("amount").sendKeys("10");
        paymentPage.getUnregisteredCardRadioButton().click();
        paymentPage.getPanInputField().click();
        paymentPage.getPanInputField().sendKeys("4652000000123458");
//        paymentPage.selectCardExpYear("2020");
        paymentPage.selectSomeFutureCardExpYear();
        paymentPage.getCardHolderNameInputField().click();
        paymentPage.getCardHolderNameInputField().sendKeys("anonymous");
        paymentPage.getCvvInputField().click();
        paymentPage.getCvvInputField().sendKeys("123");
        paymentPage.submit();

        assumeThat("Transaction should finish successfully", waitAndGetPaymentResult(), is("Выполнено"));
        PaymentResultPage paymentResultPage = new PaymentResultPage(driver);
        String pcid = paymentResultPage.getPcid();
        String phoneNumber = paymentResultPage.getParamById("param1");
        String amount = paymentResultPage.getParamById("amount");
        String date = paymentResultPage.getDate();

        LeftMenu leftMenu = paymentResultPage.getLeftMenu();
        leftMenu.openLinkByName("История платежей");
        HistoryPage historyPage = new HistoryPage(driver);

        boolean paymentAppearsInHistory = false;
        for (PaymentHistoryLine line : historyPage.getHistoryList()){
            if (line.getPaymentPcid().equals(pcid)){
                paymentAppearsInHistory = true;
                assumeThat("Даты отличаются", line.getDateTime(), is(date));
                assumeThat("Суммы отличаются", line.getAmount(), is(amount));
                assumeThat("В описании должен быть номер телефона", line.getPaymentDescription(), containsString(phoneNumber));
                break;
            }
        }
        assertThat("Payment should appears in history", true, is(paymentAppearsInHistory));
    }

    @Features(DEPRECATED)
    @Test @Deprecated
    public void countOfPaymentInWeekShouldNotBeNotGreaterThenInMonth(){
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getHistoryUrl());
        HistoryPage historyPage = new HistoryPage(getDriver());
        historyPage.getWeekFilterLink().click();
        historyPage = new HistoryPage(getDriver());
        int paymentsInWeek = historyPage.getFoundPaymentsCount();
        historyPage.getMonthFilterLink().click();
        historyPage = new HistoryPage(getDriver());
        int paymentsInMonth = historyPage.getFoundPaymentsCount();
        assertThat("Count of payments in week should not be greater than in month", paymentsInMonth, greaterThanOrEqualTo(paymentsInWeek));
    }

}
