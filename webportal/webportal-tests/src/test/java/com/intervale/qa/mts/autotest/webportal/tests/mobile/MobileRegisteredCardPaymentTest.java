package com.intervale.qa.mts.autotest.webportal.tests.mobile;

import com.intervale.qa.mts.autotest.webportal.Profiles;
import com.intervale.qa.mts.autotest.webportal.selenium.ACSPage;
import com.intervale.qa.mts.autotest.webportal.selenium.mobile.MobilePaymentPage;
import com.intervale.qa.mts.autotest.webportal.selenium.mobile.MobilePaymentResultPage;
import com.intervale.qa.mts.autotest.webportal.selenium.mobile.MobilePaymentsPage;
import com.intervale.qa.mts.autotest.webportal.tests.AbstractSeleniumTest;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.intervale.qa.mts.autotest.webportal.FeatureList.*;
import static com.intervale.qa.mts.autotest.webportal.StoriesList.REGISTERED_CARD_PAYMENT;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.junit.Assume.assumeThat;

/**
 * Created by dchahovsky.
 */
public class MobileRegisteredCardPaymentTest extends AbstractSeleniumTest {


    @Features(MOBILE_FEATURE)
    @Stories(REGISTERED_CARD_PAYMENT)
    @Description("Платеж с зарегистрированной карты без 3ds, проверка формы результата платежа")
    @Test
    public void paymentFromNon3dsCard() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        WebDriver driver = getDriver();
        driver.get(CONFIG.getMobilePaymentsPageUrl());
        MobilePaymentsPage mobilePaymentsPage = new MobilePaymentsPage(driver);
        mobilePaymentsPage.openPaymentByName("autotest","Автовеб - 1");
        MobilePaymentPage mobilePaymentPage = new MobilePaymentPage(driver);
        mobilePaymentPage.getInputParamFieldByName("param1").sendKeys("9104451325");
        mobilePaymentPage.getInputParamFieldByName("amount").sendKeys("10");
        mobilePaymentPage.getCardByName("autotest-visa-non-3ds").click();
        mobilePaymentPage.getCvvInputField().click();
        mobilePaymentPage.getCvvInputField().sendKeys("123");
        mobilePaymentPage.submit();
        assertThat("Платеж должен завершиться успешно", waitAndGetPaymentResult(), is("Выполнено"));
        MobilePaymentResultPage paymentResultPage = new MobilePaymentResultPage(driver);
        assertThat("Неверно указан источник", paymentResultPage.getPaymentSource(), is("Карта"));
        assertThat("Неверно указана оплачиваемая услуга", paymentResultPage.getPaidService(), is("Автовеб - 1"));
        assertThat("Неверно указан получатель", paymentResultPage.getPaymentRecipient(), is("ЗАО «Получатель платежа»"));
        assertThat("Неверно указана сумма платежа", paymentResultPage.getParamById("amount"), is("10 руб."));
        assertThat("Неверно указан номер телефона", paymentResultPage.getParamById("param1"), is("9104451325"));
        assertThat("Неверно указана сумма с комиссией", paymentResultPage.getAmountWithCommission(), is("20 руб."));
    }

    @Features(MOBILE_FEATURE)
    @Stories(REGISTERED_CARD_PAYMENT)
    @Description("Платеж с зарегистрированной 3ds карты, проверка формы результата платежа")
    @Test
    public void paymentFrom3dsCard() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        WebDriver driver = getDriver();
        driver.get(CONFIG.getMobilePaymentsPageUrl());
        MobilePaymentsPage mobilePaymentsPage = new MobilePaymentsPage(driver);
        mobilePaymentsPage.openPaymentByName("autotest","Автовеб - 1");
        MobilePaymentPage mobilePaymentPage = new MobilePaymentPage(driver);
        mobilePaymentPage.getInputParamFieldByName("param1").sendKeys("9104451325");
        mobilePaymentPage.getInputParamFieldByName("amount").sendKeys("10");
        mobilePaymentPage.getCardByName("autotest-visa-3ds").click();
        mobilePaymentPage.getCvvInputField().click();
        mobilePaymentPage.getCvvInputField().sendKeys("123");
        mobilePaymentPage.submit();
        waitForDifferentCurrentUrlStartsWith(CONFIG.getBaseUrl());
        assertThat("Должен осуществиться переход на ACS", getDriver().getTitle(), is("Basic ACS"));
        ACSPage acsPage = new ACSPage(getDriver());
        acsPage.acsAccept();
        assertThat("Платеж должен завершиться успешно", waitAndGetPaymentResult(), is("Выполнено"));
        MobilePaymentResultPage paymentResultPage = new MobilePaymentResultPage(driver);
        assertThat("Неверно указан источник", paymentResultPage.getPaymentSource(), is("Карта"));
        assertThat("Неверно указана оплачиваемая услуга", paymentResultPage.getPaidService(), is("Автовеб - 1"));
        assertThat("Неверно указан получатель", paymentResultPage.getPaymentRecipient(), is("ЗАО «Получатель платежа»"));
        assertThat("Неверно указана сумма платежа", paymentResultPage.getParamById("amount"), is("10 руб."));
        assertThat("Неверно указан номер телефона", paymentResultPage.getParamById("param1"), is("9104451325"));
        assertThat("Неверно указана сумма с комиссией", paymentResultPage.getAmountWithCommission(), is("20 руб."));
    }

    /* DEPRECATED */

    @Features(DEPRECATED)
    @Test
    @Deprecated
    public void beelinePaymentFromRegisteredCard(){
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        getDriver().get(CONFIG.getMobilePaymentsPageUrl());
        MobilePaymentsPage mobilePaymentsPage = new MobilePaymentsPage(getDriver());
        MobilePaymentPage mobilePaymentPage = mobilePaymentsPage.openPaymentByName("autotest", "Автовеб - 1");
        mobilePaymentPage.getInputParamFieldByName("param1").click();
        mobilePaymentPage.getInputParamFieldByName("param1").sendKeys("9104451325");
        mobilePaymentPage.getInputParamFieldByName("amount").sendKeys("10");
        assumeThat("К профилю не привязана банковская карта 'autotest-visa-non-3ds'", mobilePaymentPage.listRegisteredCardNames(), hasItem("autotest-visa-non-3ds"));
        mobilePaymentPage.getCardByName("autotest-visa-non-3ds").click();
        mobilePaymentPage.getCvvInputField().click();
        mobilePaymentPage.getCvvInputField().sendKeys("123");
        mobilePaymentPage.submit();
        assertThat("Платеж должен завершиться успешно", waitAndGetPaymentResult(), is("Выполнено"));
    }
}
