package com.intervale.qa.mts.autotest.webportal.tests.desktop;

import com.intervale.qa.mts.autotest.webportal.Profiles;
import com.intervale.qa.mts.autotest.webportal.selenium.ACSPage;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.PaymentPage;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.PaymentResultPage;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.PaymentsPage;
import com.intervale.qa.mts.autotest.webportal.tests.AbstractSeleniumTest;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.intervale.qa.mts.autotest.webportal.FeatureList.*;
import static com.intervale.qa.mts.autotest.webportal.StoriesList.REGISTERED_CARD_PAYMENT;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by dchahovsky.
 */
public class RegisteredCardPaymentTest extends AbstractSeleniumTest {

    @Features(DESKTOP_FEATURE)
    @Stories(REGISTERED_CARD_PAYMENT)
    @Description("Платеж с зарегистрированной карты без 3ds, проверка формы результата платежа")
    @Test
    public void paymentFromNon3dsCard() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        WebDriver driver = getDriver();
        driver.get(CONFIG.getMainPageUrl());
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        paymentsPage.openMenuItem("Автовеб - 1");
        PaymentPage paymentPage = new PaymentPage(driver);
        paymentPage.getInputParamFieldByName("param1").sendKeys("9104451325");
        paymentPage.getInputParamFieldByName("amount").sendKeys("10");
        paymentPage.selectCardByName("autotest-visa-non-3ds");
        paymentPage.getCvvInputField().click();
        paymentPage.getCvvInputField().sendKeys("123");
        paymentPage.submit();
        assertThat("Платеж должен завершиться успешно", waitAndGetPaymentResult(), is("Выполнено"));
        PaymentResultPage paymentResultPage = new PaymentResultPage(driver);
        assertThat("Неверно указан источник", paymentResultPage.getPaymentSource(), is("Карта"));
        assertThat("Неверно указана оплачиваемая услуга", paymentResultPage.getPaidService(), is("Автовеб - 1"));
        assertThat("Неверно указан получатель", paymentResultPage.getPaymentRecipient(), is("ЗАО «Получатель платежа»"));
        assertThat("Неверно указана сумма платежа", paymentResultPage.getParamById("amount"), is("10 руб."));
        assertThat("Неверно указан номер телефона", paymentResultPage.getParamById("param1"), is("9104451325"));
        assertThat("Неверно указана сумма с комиссией", paymentResultPage.getAmountWithCommission(), is("20 руб."));
    }

    @Features(DESKTOP_FEATURE)
    @Stories(REGISTERED_CARD_PAYMENT)
    @Description("Платеж с зарегистрированной 3ds карты, проверка формы результата платежа")
    @Test
    public void paymentFrom3dsCard() {
        setActiveProfile(Profiles.DEFAULT_PROFILE);
        WebDriver driver = getDriver();
        driver.get(CONFIG.getMainPageUrl());
        PaymentsPage paymentsPage = new PaymentsPage(driver);
        paymentsPage.openMenuItem("Автовеб - 1");
        PaymentPage paymentPage = new PaymentPage(driver);
        paymentPage.getInputParamFieldByName("param1").sendKeys("9104451325");
        paymentPage.getInputParamFieldByName("amount").sendKeys("10");
        paymentPage.selectCardByName("autotest-visa-3ds");
        paymentPage.getCvvInputField().click();
        paymentPage.getCvvInputField().sendKeys("123");
        paymentPage.submit();
        waitForDifferentCurrentUrlStartsWith(CONFIG.getBaseUrl());
        assertThat("Должен осуществиться переход на ACS", getDriver().getTitle(), is("Basic ACS"));
        ACSPage acsPage = new ACSPage(getDriver());
        acsPage.acsAccept();
        assertThat("Платеж должен завершиться успешно", waitAndGetPaymentResult(), is("Выполнено"));
        PaymentResultPage paymentResultPage = new PaymentResultPage(driver);
        assertThat("Неверно указан источник", paymentResultPage.getPaymentSource(), is("Карта"));
        assertThat("Неверно указана оплачиваемая услуга", paymentResultPage.getPaidService(), is("Автовеб - 1"));
        assertThat("Неверно указан получатель", paymentResultPage.getPaymentRecipient(), is("ЗАО «Получатель платежа»"));
        assertThat("Неверно указана сумма платежа", paymentResultPage.getParamById("amount"), is("10 руб."));
        assertThat("Неверно указан номер телефона", paymentResultPage.getParamById("param1"), is("9104451325"));
        assertThat("Неверно указана сумма с комиссией", paymentResultPage.getAmountWithCommission(), is("20 руб."));
    }
}
