package com.intervale.qa.mts.autotest.webportal.steps;

import com.intervale.qa.mts.autotest.webportal.WebConfig;
import com.intervale.qa.mts.autotest.webportal.selenium.desktop.favorite.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.List;

/**
 * Created by ashaporev
 */
public class FavouritesSteps {

    private static final WebConfig CONFIG = new WebConfig();

    private WebDriver driver;

    public static Logger logger = LoggerFactory.getLogger(FavouritesSteps.class);

    public FavouritesSteps(WebDriver driver){
        this.driver = driver;
    }

    @Step
    public void addPhonePaymentToFavourites(List<String> treePath, String paymentName, String phone, String amount){
        driver.get(CONFIG.getFavoritesUrl());
        FavoritesPage favoritesPage = new FavoritesPage(driver);
        WebElement addLink = favoritesPage.getAddToFavoriteLink();
        addLink.click();
        ChooseFavoritePaymentPage chooseFavoritePaymentPage = new ChooseFavoritePaymentPage(driver);
        PaymentsList paymentsList = chooseFavoritePaymentPage.getPaymentsList();
        paymentsList.selectByTree(treePath);
        AddFavoritePage addFavoritePage = new AddFavoritePage(driver);
        addFavoritePage.getInputFieldById("title").clear();
        if (paymentName!=null && !"".equals(paymentName)){
            addFavoritePage.getInputFieldById("title").sendKeys(paymentName);
        }
        if (phone!=null && !"".equals(phone)){
            addFavoritePage.getInputFieldById("param1").sendKeys(phone);
        }
        if (amount!=null && !"".equals(amount)){
            addFavoritePage.getInputFieldById("amount").sendKeys(amount);
        }
        addFavoritePage.submit();
    }

    @Step
    public void editFavPayment(String favTitle, String newFavTitle, String newPhone, String newAmount){
        driver.get(CONFIG.getFavoritesUrl());
        FavoritesPage favoritesPage = new FavoritesPage(driver);
        FavoritePaymentLine paymentLine = favoritesPage.getPaymentLine(favTitle);
        paymentLine.getEditLink().click();
        EditFavoritePage editFavoritePage = new EditFavoritePage(driver);
        if (newFavTitle!=null && !"".equals(newFavTitle)){
            // changing title
            editFavoritePage.getInputFieldById("title").click();
            editFavoritePage.getInputFieldById("title").clear();
            editFavoritePage.getInputFieldById("title").sendKeys(newFavTitle);
        }
        if (newPhone!=null && !"".equals(newPhone)){
            // changing phone number
//            String inputFieldId = "e_phone";  // if payment for mts
            String phoneInputFieldId = "param1";  // if payment for beeline
            editFavoritePage.getInputFieldById(phoneInputFieldId).click();
            editFavoritePage.getInputFieldById(phoneInputFieldId).clear();
            editFavoritePage.getInputFieldById(phoneInputFieldId).sendKeys(newPhone);
        }
        if (newAmount!=null && !"".equals(newAmount)){
            // changing amount
            editFavoritePage.getInputFieldById("amount").click();
            editFavoritePage.getInputFieldById("amount").clear();
            editFavoritePage.getInputFieldById("amount").sendKeys(newAmount);
        }
        logger.info("Renaming fav item '{}' to '{}'.", favTitle, newFavTitle);
        editFavoritePage.submit();
    }
}
